-- This file is part of Vervis.
--
-- Written in 2016, 2018, 2019, 2020, 2022, 2023
-- by fr33domlover <fr33domlover@riseup.net>.
--
-- ♡ Copying is an act of love. Please copy, reuse and share.
--
-- The author(s) have dedicated all copyright and related and neighboring
-- rights to this software to the public domain worldwide. This software is
-- distributed without any warranty.
--
-- You should have received a copy of the CC0 Public Domain Dedication along
-- with this software. If not, see
-- <http://creativecommons.org/publicdomain/zero/1.0/>.

-- ----------------------------------------------------------------------------
-- Yesod misc
-- ----------------------------------------------------------------------------

/highlight/#Text/style.css                   HighlightStyleR   GET

-- ----------------------------------------------------------------------------
-- Internal
-- ----------------------------------------------------------------------------


-- ----------------------------------------------------------------------------
-- Federation
-- ----------------------------------------------------------------------------


-- ----------------------------------------------------------------------------
-- Current user
-- ----------------------------------------------------------------------------

-- /cr                                          ClaimRequestsPersonR GET

-- ----------------------------------------------------------------------------
-- People
-- ----------------------------------------------------------------------------

-- /g/!new                                      GroupNewR            GET
-- /g/#ShrIdent/m                               GroupMembersR        GET POST
-- /g/#ShrIdent/m/!new                          GroupMemberNewR      GET
-- /g/#ShrIdent/m/#ShrIdent                     GroupMemberR         GET DELETE POST

-- /s/#ShrIdent/pr                              ProjectRolesR        GET POST
-- /s/#ShrIdent/pr/!new                         ProjectRoleNewR      GET
-- /s/#ShrIdent/pr/#RlIdent                     ProjectRoleR         GET DELETE POST
-- /s/#ShrIdent/pr/#RlIdent/a                   ProjectRoleOpsR      GET POST
-- /s/#ShrIdent/pr/#RlIdent/a/!new              ProjectRoleOpNewR    GET

-- ----------------------------------------------------------------------------
-- Projects
-- ----------------------------------------------------------------------------

-- /s/#ShrIdent/r                               ReposR                GET

-- /s/#ShrIdent/p/#PrjIdent/tcr                 ClaimRequestsProjectR GET

-- /w                                           GlobalWorkflowsR      GET POST
-- /w/!new                                      GlobalWorkflowNewR    GET
-- /w/#WflIdent                                 GlobalWorkflowR       GET DELETE POST

-- /s/#ShrIdent/w                               WorkflowsR            GET POST
-- /s/#ShrIdent/w/!new                          WorkflowNewR          GET
-- /s/#ShrIdent/w/#WflIdent                     WorkflowR             GET DELETE POST
-- /s/#ShrIdent/w/#WflIdent/f                   WorkflowFieldsR       GET POST
-- /s/#ShrIdent/w/#WflIdent/f/!new              WorkflowFieldNewR     GET
-- /s/#ShrIdent/w/#WflIdent/f/#FldIdent         WorkflowFieldR        GET DELETE POST
-- /s/#ShrIdent/w/#WflIdent/e                   WorkflowEnumsR        GET POST
-- /s/#ShrIdent/w/#WflIdent/e/!new              WorkflowEnumNewR      GET
-- /s/#ShrIdent/w/#WflIdent/e/#EnmIdent         WorkflowEnumR         GET DELETE POST
-- /s/#ShrIdent/w/#WflIdent/e/#EnmIdent/c       WorkflowEnumCtorsR    GET POST
-- /s/#ShrIdent/w/#WflIdent/e/#EnmIdent/c/!new  WorkflowEnumCtorNewR  GET
-- /s/#ShrIdent/w/#WflIdent/e/#EnmIdent/c/#Text WorkflowEnumCtorR     PUT DELETE POST

-- /s/#ShrIdent/p/#PrjIdent/w/+Texts            WikiPageR             GET























------------------------------------------------------------------------------
-- New route structure
------------------------------------------------------------------------------

---- Static Files ------------------------------------------------------------

/static      StaticR     Static appStatic
/favicon.svg FaviconSvgR GET
/favicon.png FaviconPngR GET
/robots.txt  RobotsR     GET

---- Authentication ----------------------------------------------------------

/auth/!resend ResendVerifyEmailR GET
/auth         AuthR              Auth  getAuth
/oauth        DvaraR             Dvara getDvara
/akey1        ActorKey1R         GET
/akey2        ActorKey2R         GET

---- Client ------------------------------------------------------------------

/              HomeR          GET
/browse        BrowseR        GET
/notifications NotificationsR GET POST
-- /publish       PublishR       GET POST
/inbox         InboxDebugR    GET

/ssh-keys                         KeysR      GET POST
/ssh-keys/#SshKeyKeyHashid/delete KeyDeleteR POST

--/publish/comment     PublishCommentR    GET POST
/publish/offer-merge PublishOfferMergeR GET POST
/publish/merge       PublishMergeR      GET POST
/publish/invite      PublishInviteR     GET POST
/publish/remove      PublishRemoveR     GET POST
/publish/resolve     PublishResolveR    GET POST

/accept-invite/#PermitFulfillsInviteKeyHashid AcceptInviteR POST

---- Person ------------------------------------------------------------------

/people/#PersonKeyHashid                                 PersonR           GET
/people/#PersonKeyHashid/inbox                           PersonInboxR      GET POST
/people/#PersonKeyHashid/outbox                          PersonOutboxR     GET POST
/people/#PersonKeyHashid/outbox/#OutboxItemKeyHashid     PersonOutboxItemR GET
/people/#PersonKeyHashid/followers                       PersonFollowersR  GET
/people/#PersonKeyHashid/following                       PersonFollowingR  GET

/people/#PersonKeyHashid/ssh-keys/#SshKeyKeyHashid       SshKeyR           GET

/people/#PersonKeyHashid/messages/#LocalMessageKeyHashid PersonMessageR    GET

/people/#PersonKeyHashid/follow                          PersonFollowR     POST
/people/#PersonKeyHashid/unfollow                        PersonUnfollowR   POST

/people/#PersonKeyHashid/stamps/#SigKeyKeyHashid         PersonStampR      GET

---- Group ------------------------------------------------------------------

/new-group                                              GroupNewR        GET POST

/groups/#GroupKeyHashid                                 GroupR           GET
/groups/#GroupKeyHashid/inbox                           GroupInboxR      GET POST
/groups/#GroupKeyHashid/outbox                          GroupOutboxR     GET
/groups/#GroupKeyHashid/outbox/#OutboxItemKeyHashid     GroupOutboxItemR GET
/groups/#GroupKeyHashid/followers                       GroupFollowersR  GET

/groups/#GroupKeyHashid/messages/#LocalMessageKeyHashid GroupMessageR    GET

/groups/#GroupKeyHashid/stamps/#SigKeyKeyHashid         GroupStampR      GET

/groups/#GroupKeyHashid/members                         GroupMembersR    GET
/groups/#GroupKeyHashid/invite                          GroupInviteR     GET POST
/groups/#GroupKeyHashid/remove/#CollabTopicGroupId      GroupRemoveR     POST

/groups/#GroupKeyHashid/children GroupChildrenR GET
/groups/#GroupKeyHashid/children/local/#DestThemSendDelegatorLocalKeyHashid/live GroupChildLocalLiveR GET
/groups/#GroupKeyHashid/children/remote/#DestThemSendDelegatorRemoteKeyHashid/live GroupChildRemoteLiveR GET
/groups/#GroupKeyHashid/parents  GroupParentsR  GET

---- Repo --------------------------------------------------------------------

/repos/#RepoKeyHashid                             RepoR              GET
/repos/#RepoKeyHashid/inbox                       RepoInboxR         GET POST
/repos/#RepoKeyHashid/outbox                      RepoOutboxR        GET
/repos/#RepoKeyHashid/outbox/#OutboxItemKeyHashid RepoOutboxItemR    GET
/repos/#RepoKeyHashid/followers                   RepoFollowersR     GET

/repos/#RepoKeyHashid/_darcs/+Texts               DarcsDownloadR     GET
/repos/#RepoKeyHashid/info/refs                   GitRefDiscoverR    GET
/repos/#RepoKeyHashid/git-upload-pack             GitUploadRequestR  POST

/repos/#RepoKeyHashid/source/+Texts               RepoSourceR        GET
/repos/#RepoKeyHashid/source-by/#Text/+Texts      RepoBranchSourceR  GET
/repos/#RepoKeyHashid/raw/#Text/+Texts            RepoBranchRawR     GET
/repos/#RepoKeyHashid/commits                     RepoCommitsR       GET
/repos/#RepoKeyHashid/commits-by/#Text            RepoBranchCommitsR GET
/repos/#RepoKeyHashid/commits/#Text               RepoCommitR        GET

/repos/#RepoKeyHashid/messages/#LocalMessageKeyHashid RepoMessageR GET

/new-repo                                         RepoNewR           GET POST
/repos/#RepoKeyHashid/delete                      RepoDeleteR        POST
/repos/#RepoKeyHashid/edit                        RepoEditR          GET POST
/repos/#RepoKeyHashid/follow                      RepoFollowR        POST
/repos/#RepoKeyHashid/unfollow                    RepoUnfollowR      POST

/post-receive                                     PostReceiveR       POST

/repos/#RepoKeyHashid/enable-loom/#LoomKeyHashid  RepoLinkR          POST

/repos/#RepoKeyHashid/stamps/#SigKeyKeyHashid     RepoStampR         GET

/repos/#RepoKeyHashid/collabs                     RepoCollabsR       GET

---- Deck --------------------------------------------------------------------

/decks/#DeckKeyHashid                             DeckR           GET
/decks/#DeckKeyHashid/inbox                       DeckInboxR      GET POST
/decks/#DeckKeyHashid/outbox                      DeckOutboxR     GET
/decks/#DeckKeyHashid/outbox/#OutboxItemKeyHashid DeckOutboxItemR GET
/decks/#DeckKeyHashid/followers                   DeckFollowersR  GET
/decks/#DeckKeyHashid/tickets                     DeckTicketsR    GET

/decks/#DeckKeyHashid/tree                        DeckTreeR       GET

/decks/#DeckKeyHashid/messages/#LocalMessageKeyHashid DeckMessageR GET

/new-deck                                         DeckNewR        GET POST
/decks/#DeckKeyHashid/delete                      DeckDeleteR     POST
/decks/#DeckKeyHashid/edit                        DeckEditR       GET POST
/decks/#DeckKeyHashid/follow                      DeckFollowR     POST
/decks/#DeckKeyHashid/unfollow                    DeckUnfollowR   POST

/decks/#DeckKeyHashid/stamps/#SigKeyKeyHashid     DeckStampR      GET

/decks/#DeckKeyHashid/collabs                     DeckCollabsR    GET
/decks/#DeckKeyHashid/invite                      DeckInviteR     GET POST
/decks/#DeckKeyHashid/remove/#CollabTopicDeckId   DeckRemoveR     POST
/decks/#DeckKeyHashid/projects                    DeckProjectsR   GET

/decks/#DeckKeyHashid/projects/approve/#StemKeyHashid DeckApproveCompR POST

---- Ticket ------------------------------------------------------------------

/decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid            TicketR            GET
/decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/discussion TicketDiscussionR  GET
/decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/events     TicketEventsR      GET
/decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/followers  TicketFollowersR   GET
/decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/deps       TicketDepsR        GET
/decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/rdeps      TicketReverseDepsR GET

/decks/#DeckKeyHashid/new-ticket                           TicketNewR         GET POST
-- /decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/edit       TicketEditR        GET POST
-- /decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/delete     TicketDeleteR      POST
/decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/close      TicketCloseR       POST
/decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/open       TicketOpenR        POST
-- /decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/claim      TicketClaimR       POST
-- /decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/unclaim    TicketUnclaimR     POST
-- /decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/assign     TicketAssignR      GET POST
-- /decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/unassign   TicketUnassignR    POST
/decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/follow                  TicketFollowR   POST
/decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/unfollow                TicketUnfollowR POST
/decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/reply                   TicketReplyR    GET POST
/decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/reply/#MessageKeyHashid TicketReplyOnR  GET POST

---- Ticket Dependency -------------------------------------------------------

/decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/deps/#TicketDepKeyHashid        TicketDepR       GET

-- /decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/new-dep                         TicketDepNewR    GET POST
-- /decks/#DeckKeyHashid/tickets/#TicketDeckKeyHashid/deps/#TicketDepKeyHashid/delete TicketDepDeleteR POST

---- Loom --------------------------------------------------------------------

/looms/#LoomKeyHashid                             LoomR           GET
/looms/#LoomKeyHashid/inbox                       LoomInboxR      GET POST
/looms/#LoomKeyHashid/outbox                      LoomOutboxR     GET
/looms/#LoomKeyHashid/outbox/#OutboxItemKeyHashid LoomOutboxItemR GET
/looms/#LoomKeyHashid/followers                   LoomFollowersR  GET
/looms/#LoomKeyHashid/cloths                      LoomClothsR     GET

/looms/#LoomKeyHashid/messages/#LocalMessageKeyHashid LoomMessageR GET

/new-loom                                         LoomNewR        GET POST
-- /looms/#LoomKeyHashid/delete                      LoomDeleteR     POST
-- /looms/#LoomKeyHashid/edit                        LoomEditR       GET POST
/looms/#LoomKeyHashid/follow                      LoomFollowR     POST
/looms/#LoomKeyHashid/unfollow                    LoomUnfollowR   POST

/looms/#LoomKeyHashid/stamps/#SigKeyKeyHashid     LoomStampR      GET

/looms/#LoomKeyHashid/collabs                     LoomCollabsR    GET

---- Cloth -------------------------------------------------------------------

/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid            ClothR            GET
/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/discussion ClothDiscussionR  GET
/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/events     ClothEventsR      GET
/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/followers  ClothFollowersR   GET
/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/deps       ClothDepsR        GET
/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/rdeps      ClothReverseDepsR GET

/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/bundles/#BundleKeyHashid                         BundleR GET
/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/bundles/#BundleKeyHashid/patches/#PatchKeyHashid PatchR  GET

/looms/#LoomKeyHashid/new-cloth                           ClothNewR         GET POST
-- /looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/edit       ClothEditR        GET POST
-- /looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/delete     ClothDeleteR      POST
-- /looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/close      ClothCloseR       POST
-- /looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/open       ClothOpenR        POST
-- /looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/claim      ClothClaimR       POST
-- /looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/unclaim    ClothUnclaimR     POST
-- /looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/assign     ClothAssignR      GET POST
-- /looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/unassign   ClothUnassignR    POST
/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/apply      ClothApplyR       POST
/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/follow     ClothFollowR      POST
/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/unfollow   ClothUnfollowR    POST
/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/reply      ClothReplyR       GET POST
/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/reply/#MessageKeyHashid ClothReplyOnR GET POST

---- Cloth Dependency --------------------------------------------------------

/looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/deps/#TicketDepKeyHashid        ClothDepR       GET

-- /looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/new-dep                         ClothDepNewR    GET POST
-- /looms/#LoomKeyHashid/cloths/#TicketLoomKeyHashid/deps/#TicketDepKeyHashid/delete ClothDepDeleteR POST

---- Project -----------------------------------------------------------------

/projects/#ProjectKeyHashid                              ProjectR           GET
/projects/#ProjectKeyHashid/inbox                        ProjectInboxR      GET POST
/projects/#ProjectKeyHashid/outbox                       ProjectOutboxR     GET
/projects/#ProjectKeyHashid/outbox/#OutboxItemKeyHashid  ProjectOutboxItemR GET
/projects/#ProjectKeyHashid/followers                    ProjectFollowersR  GET

/projects/#ProjectKeyHashid/messages/#LocalMessageKeyHashid ProjectMessageR GET

/new-project                                             ProjectNewR        GET POST

/projects/#ProjectKeyHashid/stamps/#SigKeyKeyHashid      ProjectStampR      GET

/projects/#ProjectKeyHashid/collabs                      ProjectCollabsR    GET
/projects/#ProjectKeyHashid/invite                       ProjectInviteR     GET POST
/projects/#ProjectKeyHashid/remove/#CollabTopicProjectId ProjectRemoveR     POST

/projects/#ProjectKeyHashid/components                   ProjectComponentsR GET
/projects/#ProjectKeyHashid/collabs/#CollabEnableKeyHashid/live ProjectCollabLiveR    GET

/projects/#ProjectKeyHashid/invite-component             ProjectInviteCompR     GET POST

/projects/#ProjectKeyHashid/children ProjectChildrenR GET
/projects/#ProjectKeyHashid/parents  ProjectParentsR  GET
/projects/#ProjectKeyHashid/parents/local/#DestThemSendDelegatorLocalKeyHashid/live ProjectParentLocalLiveR GET
/projects/#ProjectKeyHashid/parents/remote/#DestThemSendDelegatorRemoteKeyHashid/live ProjectParentRemoteLiveR GET
