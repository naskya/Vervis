{- This file is part of Vervis.
 -
 - Written 2019, 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

-- | Tools for integrating 'Web.Actor' with the Yesod web framework.
module Yesod.Actor
    ( decodeRouteLocal
    )
where

import Control.Monad.Trans.Except
import Data.Text (Text)
import Data.Text.Encoding
import Network.HTTP.Types.URI
import Yesod.Core

import Network.FedURI
import Web.Actor

import Control.Monad.Trans.Except.Local

instance ParseRoute site => DecodeRouteLocal (Route site) where
    decodeRouteLocal =
        parseRoute . (,[]) . decodePathSegments . encodeUtf8 . localUriPath
