{- This file is part of Vervis.
 -
 - Written in 2020 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Development.PatchMediaType.Persist () where

import Database.Persist
import Database.Persist.Sql

import Development.PatchMediaType

instance PersistField VersionControlSystem where
    toPersistValue = toPersistValue . versionControlSystemName
    fromPersistValue v = do
        t <- fromPersistValue v
        case parseVersionControlSystemName t of
            Nothing -> Left $ "Unknown version control system name: " <> t
            Just vcs -> Right vcs

instance PersistFieldSql VersionControlSystem where
    sqlType = sqlType . fmap versionControlSystemName

instance PersistField PatchMediaType where
    toPersistValue = toPersistValue . renderPatchMediaType
    fromPersistValue v = do
        t <- fromPersistValue v
        case parsePatchMediaType t of
            Nothing -> Left $ "Unknown patch media type: " <> t
            Just pmt -> Right pmt

instance PersistFieldSql PatchMediaType where
    sqlType = sqlType . fmap renderPatchMediaType
