{- This file is part of Vervis.
 -
 - Written in 2019, 2020 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Development.PatchMediaType.JSON () where

import Data.Aeson

import qualified Data.Text as T

import Development.PatchMediaType

instance FromJSON VersionControlSystem where
    parseJSON =
        withText "VersionControlSystem" $ \ t ->
            case parseVersionControlSystemURI t of
                Nothing ->
                    fail $ "Unknown version control system URI: " ++ T.unpack t
                Just vcs -> return vcs

instance ToJSON VersionControlSystem where
    toJSON = toJSON . versionControlSystemURI
    toEncoding = toEncoding . versionControlSystemURI

instance FromJSON PatchMediaType where
    parseJSON =
        withText "PatchMediaType" $ \ t ->
            case parsePatchMediaType t of
                Nothing -> fail $ "Unknown patch media type: " ++ T.unpack t
                Just pmt -> return pmt

instance ToJSON PatchMediaType where
    toJSON = toJSON . renderPatchMediaType
    toEncoding = toEncoding . renderPatchMediaType
