{- This file is part of Vervis.
 -
 - Written in 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Actor.Loom
    (
    )
where

import Control.Applicative
import Control.Exception.Base
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Align
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Foldable
import Data.List.NonEmpty (NonEmpty (..))
import Data.Maybe
import Data.Text (Text)
import Data.These
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Optics.Core
import Yesod.Persist.Core

import qualified Data.List.NonEmpty as NE
import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor
import Development.PatchMediaType
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Database.Persist.Local

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Actor
import Vervis.Actor.Common
import Vervis.Actor2
import Vervis.Cloth
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.Data.Ticket
import Vervis.FedURI
import Vervis.Federation.Util
import Vervis.Fetch
import Vervis.Foundation
import Vervis.Model hiding (deckCreate)
import Vervis.Recipient (makeRecipientSet, LocalStageBy (..), Aud (..), collectAudience, localActorFollowers)
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.RemoteActorStore
import Vervis.Ticket
import Vervis.Web.Repo

-- Meaning: An actor A is offering a ticket or a ticket dependency
-- Behavior:
--      * Verify I'm the target
--      * Insert the Offer to my inbox
--      * Create the new ticket in my DB
--      * Forward the Offer to my followers
--      * Publish an Accept to:
--          - My followers
--          - Offer sender+followers
loomOffer
    :: UTCTime
    -> LoomId
    -> Verse
    -> AP.Offer URIMode
    -> ActE (Text, Act (), Next)
loomOffer now loomID (Verse authorIdMsig body) (AP.Offer object uTarget) = do

    -- Check input
    (title, desc, source, originTipOrBundle, targetRepoID, maybeTargetBranch) <- do
        ticket <-
            case object of
                AP.OfferTicket t -> pure t
                _ -> throwE "Unsupported Offer.object type"
        ObjURI hAuthor _ <- lift $ getActorURI authorIdMsig
        let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
        WorkItemOffer {..} <- checkOfferTicket hAuthor ticket uTarget
        unless (bimap LocalActorPerson id wioAuthor == author) $
            throwE "Offering a Ticket attributed to someone else"
        Merge maybeOriginTip maybeBundle targetTip <- case wioRest of
            TAM_Task _ ->
                throwE
                    "Offer target is some local deck, so I have no use for \
                    \this Offer. Was I supposed to receive it?"
            TAM_Merge loomID' merge ->
                if loomID' == loomID
                    then return merge
                    else throwE
                            "Offer target is some other local loom, so I have \
                            \no use for this Offer. Was I supposed to receive \
                            \it?"
            TAM_Remote _ _ ->
                throwE
                    "Offer target is some remote tracker, so I have no use \
                    \for this Offer. Was I supposed to receive it?"
        originTipOrBundle <-
            fromMaybeE
                (align maybeOriginTip maybeBundle)
                "MR provides neither origin nor patches"
        (targetRepoID, maybeTargetBranch) <-
            case targetTip of
                TipLocalRepo repoID -> pure (repoID, Nothing)
                TipLocalBranch repoID branch -> pure (repoID, Just branch)
                _ -> throwE "MR target is a remote repo (this tracker serves only local repos)"
        return (wioTitle, wioDesc, wioSource, originTipOrBundle, targetRepoID, maybeTargetBranch)

    -- Verify the capability URI, if provided, is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCapability <-
        for (AP.activityCapability $ actbActivity body) $ \ uCap ->
            nameExceptT "Offer.capability" $
                first (\ (actor, _, item) -> (actor, item)) <$>
                    parseActivityURI' uCap

    -- If origin repo is remote, HTTP GET its AP representation and
    -- remember it in our DB
    --
    -- Why do we need to HTTP GET it? Because:
    --      * No support for providing a signed repo object directly in the
    --        Offer activity
    --      * It may be nice to make sure a remote origin repo's VCS type
    --        matches the target repo's VCS, even if patches are provided too
    --          + However there's no support for caching VCS type when
    --            remembering remote repo in our DB, so we'd have to check this
    --            every time
    --      * If origin is remote and no patches are provided, we'll need to
    --        know the clone URL to generate the patches ourselves
    --          + However the code here, for some simplicity, doesn't have a
    --            way to skip that and do the whole handler synchronously in
    --            case patches are provided or the origin is a local repo
    --          + And no support for caching the clone URI in DB when
    --            remembering the remote repo, so we'd need to do this every
    --            time
    let originTipOrBundle' =
            bimap
                (\case
                    TipLocalRepo repoID -> Left (repoID, Nothing)
                    TipLocalBranch repoID branch -> Left (repoID, Just branch)
                    TipRemote uOrigin -> Right (uOrigin, Nothing)
                    TipRemoteBranch uRepo branch -> Right (uRepo, Just branch)
                )
                id
                originTipOrBundle
    originTipOrBundle'' <-
        bitraverse
            (bitraverse
                pure
                (\ (uOrigin, maybeOriginBranch) -> do
                    case maybeOriginBranch of
                        Nothing -> do
                            (vcs, raid, uClone, mb) <- withExceptT (T.pack . show) $ httpGetRemoteTip' uOrigin
                            return (vcs, (raid, uClone, first Just <$> mb))
                        Just branch -> do
                            (vcs, raid, uClone) <- withExceptT (T.pack . show) $ httpGetRemoteRepo' uOrigin
                            return (vcs, (raid, uClone, Just (Nothing, branch)))
                )
            )
            pure
            originTipOrBundle'

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (loomRecip, actorRecip) <- lift $ do
            d <- getJust loomID
            (d,) <$> getJust (loomActor d)

        -- Grab loom's repo from DB and verify that it consents to be served by
        -- the loom, otherwise this loom doesn't accept tickets
        let recipLoomRepoID = loomRepo loomRecip
        unless (targetRepoID == recipLoomRepoID) $
            throwE "MR target repo isn't the one served by the Offer target loom"
        targetRepo <- lift $ getJust targetRepoID
        unless (repoLoom targetRepo == Just loomID) $
            throwE "Offer target loom doesn't have repo's consent to serve it"

        -- Verify VCS type match between patch bundle and target repo
        let targetRepoVCS = repoVcs targetRepo
        for_ (justThere originTipOrBundle) $ \ (Material typ diffs) -> do
            unless (targetRepoVCS == patchMediaTypeVCS typ) $
                throwE "Patch type and local target repo VCS mismatch"
            case (typ, diffs) of
                (PatchMediaTypeDarcs, _ :| _ : _) ->
                    throwE "More than one Darcs dpatch file provided"
                _ -> pure ()

        -- If origin repo is local, find it in our DB.
        --
        -- Verify the (local or remote) origin repo's VCS type matches the
        -- target repo.
        originOrBundle' <-
            bitraverse
                (bitraverse
                    (\ origin@(repoID, maybeBranch) -> do
                        repo <- getE repoID "MR origin local repo not found in DB"
                        unless (repoVcs repo == targetRepoVCS) $
                            throwE "Local origin repo VCS differs from target repo VCS"
                        return origin
                    )
                    (\ (vcs, origin) -> do
                        unless (vcs == targetRepoVCS) $
                            throwE "Remote origin repo VCS differs from target repo VCS"
                        return origin
                    )
                )
                pure
                originTipOrBundle''

        -- Verify that branches are specified for Git and aren't specified for
        -- Darcs
        -- Also, produce a data structure separating by VCS rather than by
        -- local/remote origin, which we'll need for generating patches
        tipInfo <- case targetRepoVCS of
            VCSGit -> do
                targetBranch <- fromMaybeE maybeTargetBranch "Local target repo is Git but no target branch specified"
                maybeOrigin <- for (justHere originOrBundle') $ \case
                    Left (originRepoID, maybeOriginBranch) -> do
                        originBranch <- fromMaybeE maybeOriginBranch "Local origin repo is Git but no origin branch specified"
                        return (Left originRepoID, originBranch)
                    Right (_remoteActorID, uClone, maybeOriginBranch) -> do
                        (_maybeURI, originBranch) <- fromMaybeE maybeOriginBranch "Remote origin repo is Git but no origin branch specified"
                        return (Right uClone, originBranch)
                return $ Left (targetBranch, maybeOrigin)
            VCSDarcs -> do
                verifyNothingE maybeTargetBranch "Local target repo is Darcs but target branch specified"
                maybeOriginRepo <- for (justHere originOrBundle') $ \case
                    Left (originRepoID, maybeOriginBranch) -> do
                        verifyNothingE maybeOriginBranch "Local origin repo is Darcs but origin branch specified"
                        return $ Left originRepoID
                    Right (_remoteActorID, uClone, maybeOriginBranch) -> do
                        verifyNothingE maybeOriginBranch "Remote origin repo is Darcs but origin branch specified"
                        return $ Right uClone
                return $ Right $ maybeOriginRepo

        -- Insert the Offer to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
        for mractid $ \ offerDB -> do

            -- If a capability is provided, check it
            for_ maybeCapability $ \ cap -> do
                lcap <-
                    case cap of
                        Left c -> pure c
                        Right _ -> throwE "Capability is a remote URI, i.e. not authored by me"
                verifyCapability'
                    lcap
                    authorIdMsig
                    (LocalActorLoom loomID)
                    AP.RoleReport

            -- Prepare forwarding the Offer to my followers
            let recipByID = LocalActorLoom loomID
            recipByHash <- hashLocalActor recipByID
            let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

            -- Insert the new ticket to our DB
            acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorRecip) now
            offerDB' <-
                bitraverse
                (traverseOf _1 $ \case
                    LocalActorPerson personID -> pure personID
                    _ -> throwE "Local non-Person ticket authors not allowed"
                )
                pure
                offerDB
            ticketID <- lift $ insertTask title desc source offerDB' acceptID
            clothID <- lift $ insertMerge loomID ticketID maybeTargetBranch originOrBundle'
            let maybePull =
                    let maybeTipInfo =
                            case tipInfo of
                                Left (b, mo) -> Left . (b,) <$> mo
                                Right mo -> Right <$> mo
                        hasBundle = isJust $ justThere originOrBundle'
                    in  (clothID, targetRepoID, hasBundle,) <$> maybeTipInfo

            -- Prepare an Accept activity and insert to my outbox
            accept@(actionAccept, _, _, _) <- lift $ prepareAccept clothID
            let recipByKey = LocalActorLoom loomID
            _luAccept <- lift $ updateOutboxItem' recipByKey acceptID actionAccept

            return (loomActor loomRecip, sieve, acceptID, accept, maybePull)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (loomActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), maybePull) -> do
            traverse_ generatePatches maybePull
            forwardActivity
                authorIdMsig body (LocalActorLoom loomID) loomActorID sieve
            lift $ sendActivity
                (LocalActorLoom loomID) loomActorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            done "Opened a MR and forwarded the Offer"

    where

    insertTask title desc source offerDB acceptID = do
        did <- insert Discussion
        fsid <- insert FollowerSet
        tid <- insert Ticket
            { ticketNumber      = Nothing
            , ticketCreated     = now
            , ticketTitle       = title
            , ticketSource      = source
            , ticketDescription = desc
            , ticketDiscuss     = did
            , ticketFollowers   = fsid
            , ticketAccept      = acceptID
            }
        case offerDB of
            Left (personID, _, offerID) ->
                insert_ TicketAuthorLocal
                    { ticketAuthorLocalTicket = tid
                    , ticketAuthorLocalAuthor = personID
                    , ticketAuthorLocalOpen   = offerID
                    }
            Right (author, _, offerID) ->
                insert_ TicketAuthorRemote
                    { ticketAuthorRemoteTicket = tid
                    , ticketAuthorRemoteAuthor = remoteAuthorId author
                    , ticketAuthorRemoteOpen   = offerID
                    }
        return tid

    insertMerge
        :: LoomId
        -> TicketId
        -> Maybe Text
        -> These
            (Either
                (RepoId, Maybe Text)
                (RemoteActorId, FedURI, Maybe (Maybe LocalURI, Text))
            )
            Material
        -> ActDB TicketLoomId
    insertMerge loomID ticketID maybeTargetBranch originOrBundle = do
        clothID <- insert $ TicketLoom ticketID loomID maybeTargetBranch
        for_ (justHere originOrBundle) $ \case
            Left (repoID, maybeOriginBranch) ->
                insert_ $ MergeOriginLocal clothID repoID maybeOriginBranch
            Right (remoteActorID, _uClone, maybeOriginBranch) -> do
                originID <- insert $ MergeOriginRemote clothID remoteActorID
                for_ maybeOriginBranch $ \ (mlu, b) ->
                    insert_ $ MergeOriginRemoteBranch originID mlu b
        for_ (justThere originOrBundle) $ \ (Material typ diffs) -> do
            bundleID <- insert $ Bundle clothID False
            insertMany_ $ NE.toList $ NE.reverse $
                NE.map (Patch bundleID now typ) diffs
        return clothID

    prepareAccept clothID = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audSender <- makeAudSenderWithFollowers authorIdMsig
        loomHash <- encodeKeyHashid loomID
        clothHash <- encodeKeyHashid clothID
        let audLoom = AudLocal [] [LocalStageLoomFollowers loomHash]
        uOffer <- lift $ getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audSender, audLoom]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uOffer]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uOffer
                    , AP.acceptResult   =
                        Just $ encodeRouteLocal $ ClothR loomHash clothHash
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor is asking to close a MR
-- Behavior:
--      * Verify it's my MR
--      * Verify the Resolve is authorized
--      * Insert the Resolve to my inbox
--      * Close the MR in my DB
--      * Forward the Resolve to my followers & MR followers
--      * Publish an Accept to:
--          - My followers
--          - MR's followers
--          - Resolve sender+followers
loomResolve
    :: UTCTime
    -> LoomId
    -> Verse
    -> AP.Resolve URIMode
    -> ActE (Text, Act (), Next)
loomResolve now loomID (Verse authorIdMsig body) (AP.Resolve uObject) = do

    -- Check input
    loomHash <- encodeKeyHashid loomID
    clothHash <- nameExceptT "Resolve.object" $ do
        route <- do
            routeOrRemote <- parseFedURI uObject
            case routeOrRemote of
                Left route -> pure route
                Right _ -> throwE "Remote, so definitely not mine"
        case route of
            ClothR loomHash' clothHash | loomHash' == loomHash ->
                return clothHash
            _ -> throwE "Local route but not a MR of mine"
    clothID <- decodeKeyHashidE clothHash "Invalid TicketLoom keyhashid"

    -- Check capability
    capability <- do

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the capability URI is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        cap <- nameExceptT "Resolve.capability" $ parseActivityURI' uCap

        -- Verify the capability is local
        case cap of
            Left (actorByKey, _, outboxItemID) ->
                return (actorByKey, outboxItemID)
            _ -> throwE "Capability is remote i.e. definitely not by me"

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (loomRecip, actorRecip) <- lift $ do
            d <- getJust loomID
            (d,) <$> getJust (loomActor d)

        -- Find ticket in DB, verify it's not resolved
        ticketID <- do
            maybeCloth <- lift $ getCloth loomID clothID
            (_loom, _cloth, Entity ticketID _, _author, maybeResolve, _merge) <-
                fromMaybeE maybeCloth "I don't have such a MR in DB"
            unless (isNothing maybeResolve) $
                throwE "MR is already resolved"
            return ticketID

        -- Insert the Resolve to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
        for mractid $ \ resolveDB -> do

            -- Verify the sender is authorized by the tracker to resolve a ticket
            verifyCapability'
                capability
                authorIdMsig
                (LocalActorLoom loomID)
                AP.RoleTriage

            -- Prepare forwarding the Resolve to my followers & ticket
            -- followers
            let sieve =
                    makeRecipientSet []
                        [ LocalStageLoomFollowers loomHash
                        , LocalStageClothFollowers loomHash clothHash
                        ]

            -- Mark ticket in DB as resolved by the Resolve
            acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorRecip) now
            lift $ insertResolve ticketID resolveDB acceptID

            -- Prepare an Accept activity and insert to my outbox
            accept@(actionAccept, _, _, _) <- lift $ prepareAccept clothID
            let recipByKey = LocalActorLoom loomID
            _luAccept <- lift $ updateOutboxItem' recipByKey acceptID actionAccept

            return (loomActor loomRecip, sieve, acceptID, accept)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (loomActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept)) -> do
            forwardActivity
                authorIdMsig body (LocalActorLoom loomID) loomActorID sieve
            lift $ sendActivity
                (LocalActorLoom loomID) loomActorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            done "Resolved ticket and forwarded the Resolve"

    where

    insertResolve ticketID resolveDB acceptID = do
        trid <- insert TicketResolve
            { ticketResolveTicket = ticketID
            , ticketResolveAccept = acceptID
            }
        case resolveDB of
            Left (_actorByKey, _, resolveID) ->
                insert_ TicketResolveLocal
                    { ticketResolveLocalTicket   = trid
                    , ticketResolveLocalActivity = resolveID
                    }
            Right (author, _, resolveID) ->
                insert_ TicketResolveRemote
                    { ticketResolveRemoteTicket   = trid
                    , ticketResolveRemoteActivity = resolveID
                    , ticketResolveRemoteActor    = remoteAuthorId author
                    }

    prepareAccept clothID = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audSender <- makeAudSenderWithFollowers authorIdMsig
        loomHash <- encodeKeyHashid loomID
        clothHash <- encodeKeyHashid clothID
        let audLoom =
                AudLocal
                    []
                    [ LocalStageLoomFollowers loomHash
                    , LocalStageClothFollowers loomHash clothHash
                    ]
        uResolve <- lift $ getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audSender, audLoom]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uResolve]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uResolve
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

loomBehavior :: UTCTime -> LoomId -> VerseExt -> ActE (Text, Act (), Next)
loomBehavior now loomID (Left verse@(Verse _authorIdMsig body)) =
    case AP.activitySpecific $ actbActivity body of
        AP.OfferActivity offer     -> loomOffer now loomID verse offer
        AP.ResolveActivity resolve -> loomResolve now loomID verse resolve
        _ -> throwE "Unsupported activity type for Loom"
loomBehavior _ _ (Right _) = throwE "ClientMsgs aren't supported for Loom"

instance VervisActor Loom where
    actorBehavior = loomBehavior
