{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2022, 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Vervis.Actor.Common
    ( actorFollow
    , topicAccept
    , topicReject
    , topicInvite
    , topicRemove
    , topicJoin
    , topicCreateMe
    , componentGrant
    )
where

import Control.Applicative
import Control.Exception.Base
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Barbie
import Data.Bifoldable
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Either
import Data.Foldable
import Data.List.NonEmpty (NonEmpty (..))
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Optics.Core
import Yesod.Persist.Core

import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Database.Persist.Local

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Actor
import Vervis.Actor2
import Vervis.Cloth
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.FedURI
import Vervis.Federation.Util
import Vervis.Foundation
import Vervis.Model
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.Recipient (makeRecipientSet, LocalStageBy (..), Aud (..), collectAudience, localActorFollowers, renderLocalActor)
import Vervis.RemoteActorStore
import Vervis.Ticket

actorFollow
    :: (PersistRecordBackend r SqlBackend, ToBackendKey SqlBackend r)
    => (Route App -> ActE a)
    -> (r -> ActorId)
    -> Bool
    -> (Actor -> a -> ActDBE FollowerSetId)
    -> (a -> ActDB RecipientRoutes)
    -> (forall f. f r -> LocalActorBy f)
    -> (a -> Act [Aud URIMode])
    -> UTCTime
    -> Key r
    -> Verse
    -> AP.Follow URIMode
    -> ActE (Text, Act (), Next)
actorFollow parseFollowee grabActor unread getFollowee getSieve makeLocalActor makeAudience now recipID (Verse authorIdMsig body) (AP.Follow uObject _ hide) = do

    -- Check input
    followee <- nameExceptT "Follow object" $ do
        route <- do
            routeOrRemote <- parseFedURI uObject
            case routeOrRemote of
                Left route -> pure route
                Right _ -> throwE "Remote, so definitely not me/mine"
        -- Verify the followee is me or a subobject of mine
        parseFollowee route
    verifyNothingE
        (AP.activityCapability $ actbActivity body)
        "Capability not needed"

    maybeFollow <- withDBExcept $ do

        -- Find me in DB
        recip <- lift $ getJust recipID
        let recipActorID = grabActor recip
        recipActor <- lift $ getJust recipActorID

        -- Insert the Follow to my inbox
        maybeFollowDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) unread
        for maybeFollowDB $ \ followDB -> do

            -- Find followee in DB
            followerSetID <- getFollowee recipActor followee

            -- Verify not already following me
            case followDB of
                Left (_, followerID, followID) -> do
                    maybeFollow <- lift $ getBy $ UniqueFollow followerID followerSetID
                    verifyNothingE maybeFollow "You're already following this object"
                Right (author, _, followID) -> do
                    let followerID = remoteAuthorId author
                    maybeFollow <- lift $ getBy $ UniqueRemoteFollow followerID followerSetID
                    verifyNothingE maybeFollow "You're already following this object"

            -- Record the new follow in DB
            acceptID <-
                lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
            lift $ case followDB of
                Left (_actorByKey, actorID, followID) ->
                    insert_ $ Follow actorID followerSetID (not hide) followID acceptID
                Right (author, _luFollow, followID) -> do
                    let authorID = remoteAuthorId author
                    insert_ $ RemoteFollow authorID followerSetID (not hide) followID acceptID

            -- Prepare an Accept activity and insert to actor's outbox
            accept@(actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept) <-
                lift $ prepareAccept followee
            _luAccept <- lift $ updateOutboxItem' (makeLocalActor recipID) acceptID actionAccept

            sieve <- lift $ getSieve followee
            return (recipActorID, acceptID, sieve, accept)

    case maybeFollow of
        Nothing -> done "I already have this activity in my inbox"
        Just (actorID, acceptID, sieve, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept)) -> do
            forwardActivity authorIdMsig body (makeLocalActor recipID) actorID sieve
            lift $ sendActivity
                (makeLocalActor recipID) actorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            done "Recorded Follow and published Accept"

    where

    prepareAccept followee = do
        encodeRouteHome <- getEncodeRouteHome

        audSender <- makeAudSenderWithFollowers authorIdMsig
        uFollow <- lift $ getActivityURI authorIdMsig

        audsRecip <- lift $ makeAudience followee

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience $ audSender : audsRecip

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = []
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uFollow
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor accepted something
-- Behavior:
--     * If it's on an Invite where I'm the resource:
--         * Verify the Accept is by the Invite target
--         * Forward the Accept to my followers
--         * Send a Grant:
--             * To: Accepter (i.e. Invite target)
--             * CC: Invite sender, Accepter's followers, my followers
--     * If it's on a Join where I'm the resource:
--         * Verify the Accept is authorized
--         * Forward the Accept to my followers
--         * Send a Grant:
--             * To: Join sender
--             * CC: Accept sender, Join sender's followers, my followers
--     * If it's an Invite (that I know about) where I'm invited to a project:
--          * If I haven't yet seen the project's approval:
--              * Verify the author is the project
--              * Record the approval in the Stem record in DB
--          * If I saw project's approval, but not my collaborators' approval:
--              * Verify the Accept is authorized
--              * Record the approval in the Stem record in DB
--              * Forward to my followers
--              * Publish and send an Accept:
--                  * To: Inviter, project, Accept author
--                  * CC: Project followers, my followers
--              * Record it in the Stem record in DB as well
--          * If I already saw both approvals, respond with error
--     * If it's an Add (that I know about and already Accepted) where I'm
--       invited to a project:
--          * If I've already seen the project's accept, respond with error
--          * Otherwise, just ignore the Accept
--     * Otherwise respond with error
topicAccept
    :: forall topic.
       (PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic)
    => (topic -> ActorId)
    -> (forall f. f topic -> ComponentBy f)
    -> UTCTime
    -> Key topic
    -> Verse
    -> AP.Accept URIMode
    -> ActE (Text, Act (), Next)
topicAccept topicActor topicComponent now recipKey (Verse authorIdMsig body) accept = do

    -- Check input
    acceptee <- parseAccept accept

    -- Verify the capability URI, if provided, is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCap <-
        traverse
            (nameExceptT "Accept capability" . parseActivityURI')
            (AP.activityCapability $ actbActivity body)

    -- Grab me from DB
    (recipActorID, recipActor) <- lift $ withDB $ do
        recip <- getJust recipKey
        let actorID = topicActor recip
        (actorID,) <$> getJust actorID

    collabOrStem <- withDBExcept $ do

        -- Find the accepted activity in our DB
        accepteeDB <- do
            a <- getActivity acceptee
            fromMaybeE a "Can't find acceptee in DB"

        -- See if the accepted activity is an Invite or Join to a local
        -- resource, grabbing the Collab record from our DB
        -- See if the accepted activity is an Invite or Add on a local
        -- component, grabbing the Stem record from our DB
        maybeCollabOrStem <-
            lift $ runMaybeT $
                Left . Left <$> tryInviteCollab accepteeDB <|>
                Left . Right <$> tryJoinCollab accepteeDB <|>
                Right . Left <$> tryInviteComp accepteeDB <|>
                Right . Right <$> tryAddComp accepteeDB
        fromMaybeE maybeCollabOrStem "Accepted activity isn't an Invite/Join/Add I'm aware of"

    case collabOrStem of
        Left collab ->
            topicAcceptCollab maybeCap recipActorID recipActor collab
        Right stem ->
            topicAcceptStem maybeCap recipActorID recipActor stem

    where

    topicResource :: forall f. f topic -> LocalActorBy f
    topicResource = componentActor . topicComponent

    tryInviteCollab (Left (actorByKey, _actorEntity, itemID)) =
        (,Left actorByKey) . collabInviterLocalCollab <$>
            MaybeT (getValBy $ UniqueCollabInviterLocalInvite itemID)
    tryInviteCollab (Right remoteActivityID) = do
        CollabInviterRemote collab actorID _ <-
            MaybeT $ getValBy $
                UniqueCollabInviterRemoteInvite remoteActivityID
        actor <- lift $ getJust actorID
        sender <-
            lift $ (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (collab, Right sender)

    tryJoinCollab (Left (actorByKey, _actorEntity, itemID)) =
        (,Left actorByKey) . collabRecipLocalJoinFulfills <$>
            MaybeT (getValBy $ UniqueCollabRecipLocalJoinJoin itemID)
    tryJoinCollab (Right remoteActivityID) = do
        CollabRecipRemoteJoin recipID fulfillsID _ <-
            MaybeT $ getValBy $
                UniqueCollabRecipRemoteJoinJoin remoteActivityID
        remoteActorID <- lift $ collabRecipRemoteActor <$> getJust recipID
        actor <- lift $ getJust remoteActorID
        joiner <-
            lift $ (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (fulfillsID, Right joiner)

    tryInviteComp (Left (actorByKey, _actorEntity, itemID)) =
        (,Left (actorByKey, itemID)) . stemProjectGestureLocalOrigin <$>
            MaybeT (getValBy $ UniqueStemProjectGestureLocalInvite itemID)
    tryInviteComp (Right remoteActivityID) = do
        StemProjectGestureRemote originID actorID _ <-
            MaybeT $ getValBy $
                UniqueStemProjectGestureRemoteInvite remoteActivityID
        actor <- lift $ getJust actorID
        inviter <-
            lift $ (,remoteActorFollowers actor, remoteActivityID) <$> getRemoteActorURI actor
        return (originID, Right inviter)

    tryAddComp (Left (actorByKey, _actorEntity, itemID)) = do
        StemComponentGestureLocal stemID _ <-
            MaybeT $ getValBy $ UniqueStemComponentGestureLocalActivity itemID
        originID <- MaybeT $ getKeyBy $ UniqueStemOriginAdd stemID
        return (stemID, originID, Left (actorByKey, itemID))
    tryAddComp (Right remoteActivityID) = do
        StemComponentGestureRemote stemID actorID _ <-
            MaybeT $ getValBy $
                UniqueStemComponentGestureRemoteActivity remoteActivityID
        originID <- MaybeT $ getKeyBy $ UniqueStemOriginAdd stemID
        actor <- lift $ getJust actorID
        adder <-
            lift $ (,remoteActorFollowers actor,remoteActivityID) <$> getRemoteActorURI actor
        return (stemID, originID, Right adder)

    prepareGrant isInvite sender role = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audAccepter <- makeAudSenderWithFollowers authorIdMsig
        audApprover <- lift $ makeAudSenderOnly authorIdMsig
        recipHash <- encodeKeyHashid recipKey
        let topicByHash = topicResource recipHash

        senderHash <- bitraverse hashLocalActor pure sender

        uAccepter <- lift $ getActorURI authorIdMsig

        let audience =
                if isInvite
                    then
                        let audInviter =
                                case senderHash of
                                    Left actor -> AudLocal [actor] []
                                    Right (ObjURI h lu, _followers) ->
                                        AudRemote h [lu] []
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audInviter, audAccepter, audTopic]
                    else
                        let audJoiner =
                                case senderHash of
                                    Left actor -> AudLocal [actor] [localActorFollowers actor]
                                    Right (ObjURI h lu, followers) ->
                                        AudRemote h [lu] (maybeToList followers)
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audJoiner, audApprover, audTopic]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [AP.acceptObject accept]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXRole role
                    , AP.grantContext   =
                        encodeRouteHome $ renderLocalActor topicByHash
                    , AP.grantTarget    =
                        if isInvite
                            then uAccepter
                            else case senderHash of
                                Left actor ->
                                    encodeRouteHome $ renderLocalActor actor
                                Right (ObjURI h lu, _) -> ObjURI h lu
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    topicAcceptCollab maybeCap recipActorID recipActor collab = do

        maybeNew <- withDBExcept $ do

            -- Find the local resource and verify it's me
            collabID <-
                lift $ case collab of
                    Left (fulfillsID, _) ->
                        collabFulfillsInviteCollab <$> getJust fulfillsID
                    Right (fulfillsID, _) ->
                        collabFulfillsJoinCollab <$> getJust fulfillsID
            topic <- lift $ getCollabTopic collabID
            unless (topicResource recipKey == topic) $
                throwE "Accept object is an Invite/Join for some other resource"

            idsForAccept <-
                case collab of

                    -- If accepting an Invite, find the Collab recipient and verify
                    -- it's the sender of the Accept
                    Left (fulfillsID, _) -> Left <$> do
                        recip <-
                            lift $
                            requireEitherAlt
                                (getBy $ UniqueCollabRecipLocal collabID)
                                (getBy $ UniqueCollabRecipRemote collabID)
                                "Found Collab with no recip"
                                "Found Collab with multiple recips"
                        case (recip, authorIdMsig) of
                            (Left (Entity crlid crl), Left (LocalActorPerson personID, _, _))
                                | collabRecipLocalPerson crl == personID ->
                                    return (fulfillsID, Left crlid)
                            (Right (Entity crrid crr), Right (author, _, _))
                                | collabRecipRemoteActor crr == remoteAuthorId author ->
                                    return (fulfillsID, Right crrid)
                            _ -> throwE "Accepting an Invite whose recipient is someone else"

                    -- If accepting a Join, verify accepter has permission
                    Right (fulfillsID, _) -> Right <$> do
                        capID <- fromMaybeE maybeCap "No capability provided"
                        capability <-
                            case capID of
                                Left (capActor, _, capItem) -> return (capActor, capItem)
                                Right _ -> throwE "Capability is a remote URI, i.e. not authored by the local resource"
                        verifyCapability'
                            capability
                            authorIdMsig
                            (topicResource recipKey)
                            AP.RoleAdmin
                        return fulfillsID

            -- Verify the Collab isn't already validated
            maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
            verifyNothingE maybeEnabled "I already sent a Grant for this Invite/Join"

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeAcceptDB $ \ acceptDB -> do

                -- Record the Accept on the Collab
                case (idsForAccept, acceptDB) of
                    (Left (fulfillsID, Left recipID), Left (_, _, acceptID)) -> do
                        maybeAccept <- lift $ insertUnique $ CollabRecipLocalAccept recipID fulfillsID acceptID
                        unless (isNothing maybeAccept) $
                            throwE "This Invite already has an Accept by recip"
                    (Left (fulfillsID, Right recipID), Right (_, _, acceptID)) -> do
                        maybeAccept <- lift $ insertUnique $ CollabRecipRemoteAccept recipID fulfillsID acceptID
                        unless (isJust maybeAccept) $
                            throwE "This Invite already has an Accept by recip"
                    (Right fulfillsID, Left (_, _, acceptID)) -> do
                        maybeAccept <- lift $ insertUnique $ CollabApproverLocal fulfillsID acceptID
                        unless (isJust maybeAccept) $
                            throwE "This Join already has an Accept"
                    (Right fulfillsID, Right (author, _, acceptID)) -> do
                        maybeAccept <- lift $ insertUnique $ CollabApproverRemote fulfillsID (remoteAuthorId author) acceptID
                        unless (isJust maybeAccept) $
                            throwE "This Join already has an Accept"
                    _ -> error "topicAccept impossible"

                -- Prepare forwarding of Accept to my followers
                let recipByID = topicResource recipKey
                recipByHash <- hashLocalActor recipByID
                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                grantInfo <- do

                    -- Enable the Collab in our DB
                    grantID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                    lift $ insert_ $ CollabEnable collabID grantID

                    -- Prepare a Grant activity and insert to my outbox
                    let inviterOrJoiner = either snd snd collab
                        isInvite = isLeft collab
                    grant@(actionGrant, _, _, _) <- do
                        Collab role <- lift $ getJust collabID
                        lift $ prepareGrant isInvite inviterOrJoiner role
                    let recipByKey = topicResource recipKey
                    _luGrant <- lift $ updateOutboxItem' recipByKey grantID actionGrant
                    return (grantID, grant)

                return (recipActorID, sieve, grantInfo)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, (grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant))) -> do
                let recipByID = topicResource recipKey
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ sendActivity
                    recipByID recipActorID localRecipsGrant
                    remoteRecipsGrant fwdHostsGrant grantID actionGrant
                done "Forwarded the Accept and published a Grant"

    prepareReact project inviter = do
        encodeRouteHome <- getEncodeRouteHome

        (audInviter, uInvite) <-
            case inviter of
                Left (byKey, itemID) -> do
                    byHash <- hashLocalActor byKey
                    itemHash <- encodeKeyHashid itemID
                    return
                        ( AudLocal [byHash] []
                        , encodeRouteHome $ activityRoute byHash itemHash
                        )
                Right (ObjURI h lu, _followers, activityID) -> do
                    objectID <- remoteActivityIdent <$> getJust activityID
                    luAct <- remoteObjectIdent <$> getJust objectID
                    return (AudRemote h [lu] [], ObjURI h luAct)
        audProject <-
            case project of
                Left (Entity _ (StemProjectLocal _ projectID)) -> do
                    projectHash <- encodeKeyHashid projectID
                    return $
                        AudLocal
                            [LocalActorProject projectHash]
                            [LocalStageProjectFollowers projectHash]
                Right (Entity _ (StemProjectRemote _ actorID)) -> do
                    actor <- getJust actorID
                    ObjURI h lu <- getRemoteActorURI actor
                    let followers = remoteActorFollowers actor
                    return $ AudRemote h [lu] (maybeToList followers)
        audAccepter <- lift $ makeAudSenderOnly authorIdMsig
        audMe <-
            AudLocal [] . pure . localActorFollowers .
            topicResource <$>
                encodeKeyHashid recipKey

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audInviter, audProject, audAccepter, audMe]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = []
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uInvite
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    topicAcceptStem maybeCap recipActorID recipActor stem = do

        maybeNew <- withDBExcept $ do

            -- Find the local component and verify it's me
            stemID <-
                lift $ case stem of
                    Left (originInviteID, _inviter) ->
                        stemOriginInviteStem <$> getJust originInviteID
                    Right (stemID, _originAddID, _adder) ->
                        return stemID
            ident <- lift $ getStemIdent stemID
            unless (topicComponent recipKey == ident) $
                throwE "Accept object is an Invite/Add for some other component"

            project <-
                lift $
                requireEitherAlt
                    (getBy $ UniqueStemProjectLocal stemID)
                    (getBy $ UniqueStemProjectRemote stemID)
                    "Found Stem with no project"
                    "Found Stem with multiple projects"

            idsForLater <- bitraverse

                -- Accepting an Invite
                -- If I haven't seen the project's approval, verify
                -- the author is the project
                -- Otherwise, verify the Accept is authorized
                (\ (originInviteID, inviter) -> do
                    scgl <- lift $ getBy $ UniqueStemComponentGestureLocal stemID
                    scgr <- lift $ getBy $ UniqueStemComponentGestureRemote stemID
                    unless (isNothing scgl && isNothing scgr) $
                        throwE "I've already recorded my collaborator's Accept on the Invite, no need for further Accepts from anyone"
                    seen <-
                        lift $ case project of
                            Left (Entity k _) -> isJust <$> getBy (UniqueStemProjectAcceptLocalProject k)
                            Right (Entity k _) -> isJust <$> getBy (UniqueStemProjectAcceptRemoteProject k)
                    if seen
                        then do
                            capID <- fromMaybeE maybeCap "No capability provided"
                            capability <-
                                case capID of
                                    Left (capActor, _, capItem) -> return (capActor, capItem)
                                    Right _ -> throwE "Capability is a remote URI, i.e. not authored by the local resource"
                            verifyCapability'
                                capability
                                authorIdMsig
                                (topicResource recipKey)
                                AP.RoleAdmin
                        else case (project, authorIdMsig) of
                            (Left (Entity _ sjl), Left (LocalActorProject projectID, _, _))
                                | stemProjectLocalProject sjl == projectID ->
                                    return ()
                            (Right (Entity _ sjr), Right (author, _, _))
                                | stemProjectRemoteProject sjr == remoteAuthorId author ->
                                    return ()
                            _ -> throwE "The Accept I'm waiting for is by the project"
                    return (originInviteID, seen, inviter)
                )

                (\ (_stemID, _originAddID, _adder) -> do
                    seen <-
                        lift $ case project of
                            Left (Entity k _) -> isJust <$> getBy (UniqueStemProjectGrantLocalProject k)
                            Right (Entity k _) -> isJust <$> getBy (UniqueStemProjectGrantRemoteProject k)
                    when seen $
                        throwE "Already saw project's Grant, no need for any Accepts"
                )

                stem

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeAcceptDB $ \ acceptDB ->

                case idsForLater of

                    Left (originInviteID, seen, inviter) -> do

                        if not seen
                            then do
                                lift $ case (project, acceptDB) of
                                    (Left (Entity j _), Left (_, _, acceptID)) ->
                                        insert_ $ StemProjectAcceptLocal originInviteID j acceptID
                                    (Right (Entity j _), Right (_, _, acceptID)) ->
                                        insert_ $ StemProjectAcceptRemote originInviteID j acceptID
                                    _ -> error "topicAccept Impossible"
                                return Nothing
                            else do
                                lift $ case acceptDB of
                                    Left (_, _, acceptID) ->
                                        insert_ $ StemComponentGestureLocal stemID acceptID
                                    Right (author, _, acceptID) ->
                                        insert_ $ StemComponentGestureRemote stemID (remoteAuthorId author) acceptID

                                -- Prepare forwarding of Accept to my followers
                                let recipByID = topicResource recipKey
                                recipByHash <- hashLocalActor recipByID
                                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                                reactInfo <- do

                                    -- Record the fresh Accept in our DB
                                    reactID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                                    lift $ insert_ $ StemComponentAccept stemID reactID

                                    -- Prepare an Accept activity and insert to my outbox
                                    react@(actionReact, _, _, _) <- lift $ prepareReact project inviter
                                    let recipByKey = topicResource recipKey
                                    _luReact <- lift $ updateOutboxItem' recipByKey reactID actionReact
                                    return (reactID, react)

                                return $ Just (sieve, reactInfo)

                    Right () -> return Nothing

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just Nothing -> done "Done"
            Just (Just (sieve, (reactID, (actionReact, localRecipsReact, remoteRecipsReact, fwdHostsReact)))) -> do
                let recipByID = topicResource recipKey
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ sendActivity
                    recipByID recipActorID localRecipsReact
                    remoteRecipsReact fwdHostsReact reactID actionReact
                done "Forwarded the Accept and published an Accept"

topicReject
    :: (PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic)
    => (topic -> ActorId)
    -> (forall f. f topic -> LocalActorBy f)
    -> UTCTime
    -> Key topic
    -> Verse
    -> AP.Reject URIMode
    -> ActE (Text, Act (), Next)
topicReject topicActor topicResource now recipKey (Verse authorIdMsig body) reject = do

    -- Check input
    rejectee <- parseReject reject

    -- Verify the capability URI is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCap <-
        traverse
            (nameExceptT "Accept capability" . parseActivityURI')
            (AP.activityCapability $ actbActivity body)

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (recipActorID, recipActor) <- lift $ do
            recip <- getJust recipKey
            let actorID = topicActor recip
            (actorID,) <$> getJust actorID

        -- Find the rejected activity in our DB
        rejecteeDB <- do
            a <- getActivity rejectee
            fromMaybeE a "Can't find rejectee in DB"

        -- See if the rejected activity is an Invite or Join to a local
        -- resource, grabbing the Collab record from our DB
        collab <- do
            maybeCollab <-
                lift $ runMaybeT $
                    Left <$> tryInvite rejecteeDB <|>
                    Right <$> tryJoin rejecteeDB
            fromMaybeE maybeCollab "Rejected activity isn't an Invite or Join I'm aware of"

        -- Find the local resource and verify it's me
        collabID <-
            lift $ case collab of
                Left (fulfillsID, _, _) ->
                    collabFulfillsInviteCollab <$> getJust fulfillsID
                Right (fulfillsID, _, _, _) ->
                    collabFulfillsJoinCollab <$> getJust fulfillsID
        (deleteTopic, topic) <- lift $ getCollabTopic' collabID
        unless (topicResource recipKey == topic) $
            throwE "Accept object is an Invite/Join for some other resource"

        idsForReject <-
            case collab of

                -- If rejecting an Invite, find the Collab recipient and verify
                -- it's the sender of the Reject
                Left (fulfillsID, _, deleteInviter) -> Left <$> do
                    recip <-
                        lift $
                        requireEitherAlt
                            (getBy $ UniqueCollabRecipLocal collabID)
                            (getBy $ UniqueCollabRecipRemote collabID)
                            "Found Collab with no recip"
                            "Found Collab with multiple recips"
                    case (recip, authorIdMsig) of
                        (Left (Entity crlid crl), Left (LocalActorPerson personID, _, _))
                            | collabRecipLocalPerson crl == personID ->
                                return (fulfillsID, Left crlid, deleteInviter)
                        (Right (Entity crrid crr), Right (author, _, _))
                            | collabRecipRemoteActor crr == remoteAuthorId author ->
                                return (fulfillsID, Right crrid, deleteInviter)
                        _ -> throwE "Rejecting an Invite whose recipient is someone else"

                -- If rejecting a Join, verify accepter has permission
                Right (fulfillsID, _, deleteRecipJoin, deleteRecip) -> Right <$> do
                    capID <- fromMaybeE maybeCap "No capability provided"
                    capability <-
                        case capID of
                            Left (capActor, _, capItem) -> return (capActor, capItem)
                            Right _ -> throwE "Capability is a remote URI, i.e. not authored by the local resource"
                    verifyCapability'
                        capability
                        authorIdMsig
                        (topicResource recipKey)
                        AP.RoleAdmin
                    return (fulfillsID, deleteRecipJoin, deleteRecip)

        -- Verify the Collab isn't already validated
        maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
        verifyNothingE maybeEnabled "I already sent a Grant for this Invite/Join"

        -- Verify the Collab isn't already accepted/approved
        case idsForReject of
            Left (_fulfillsID, Left recipID, _) -> do
                mval <-
                    lift $ getBy $ UniqueCollabRecipLocalAcceptCollab recipID
                verifyNothingE mval "Invite is already accepted"
            Left (_fulfillsID, Right recipID, _) -> do
                mval <-
                    lift $ getBy $ UniqueCollabRecipRemoteAcceptCollab recipID
                verifyNothingE mval "Invite is already accepted"
            Right (fulfillsID, _, _) -> do
                mval1 <- lift $ getBy $ UniqueCollabApproverLocal fulfillsID
                mval2 <- lift $ getBy $ UniqueCollabApproverRemote fulfillsID
                unless (isNothing mval1 && isNothing mval2) $
                    throwE "Join is already approved"

        maybeRejectDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
        for maybeRejectDB $ \ rejectDB -> do

            -- Delete the whole Collab record
            case idsForReject of
                Left (fulfillsID, recipID, deleteInviter) -> lift $ do
                    bitraverse_ delete delete recipID
                    deleteTopic
                    deleteInviter
                    delete fulfillsID
                Right (fulfillsID, deleteRecipJoin, deleteRecip) -> lift $ do
                    deleteRecipJoin
                    deleteRecip
                    deleteTopic
                    delete fulfillsID
            lift $ delete collabID

            -- Prepare forwarding of Reject to my followers
            let recipByID = topicResource recipKey
            recipByHash <- hashLocalActor recipByID
            let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

            newRejectInfo <- do

                -- Prepare a Reject activity and insert to my outbox
                newRejectID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                let inviterOrJoiner = either (view _2) (view _2) collab
                    isInvite = isLeft collab
                newReject@(actionReject, _, _, _) <-
                    lift $ prepareReject isInvite inviterOrJoiner
                let recipByKey = topicResource recipKey
                _luNewReject <- lift $ updateOutboxItem' recipByKey newRejectID actionReject
                return (newRejectID, newReject)

            return (recipActorID, sieve, newRejectInfo)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (recipActorID, sieve, (newRejectID, (action, localRecips, remoteRecips, fwdHosts))) -> do
            let recipByID = topicResource recipKey
            forwardActivity authorIdMsig body recipByID recipActorID sieve
            lift $ sendActivity
                recipByID recipActorID localRecips
                remoteRecips fwdHosts newRejectID action
            done "Forwarded the Reject and published my own Reject"

    where

    tryInvite (Left (actorByKey, _actorEntity, itemID)) = do
        Entity k (CollabInviterLocal f _) <-
            MaybeT $ getBy $ UniqueCollabInviterLocalInvite itemID
        return (f, Left actorByKey, delete k)
    tryInvite (Right remoteActivityID) = do
        Entity k (CollabInviterRemote collab actorID _) <-
            MaybeT $ getBy $
                UniqueCollabInviterRemoteInvite remoteActivityID
        actor <- lift $ getJust actorID
        sender <-
            lift $ (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (collab, Right sender, delete k)

    tryJoin (Left (actorByKey, _actorEntity, itemID)) = do
        Entity k (CollabRecipLocalJoin recipID fulfillsID _) <-
            MaybeT $ getBy $ UniqueCollabRecipLocalJoinJoin itemID
        return (fulfillsID, Left actorByKey, delete k, delete recipID)
    tryJoin (Right remoteActivityID) = do
        Entity k (CollabRecipRemoteJoin recipID fulfillsID _) <-
            MaybeT $ getBy $
                UniqueCollabRecipRemoteJoinJoin remoteActivityID
        remoteActorID <- lift $ collabRecipRemoteActor <$> getJust recipID
        actor <- lift $ getJust remoteActorID
        joiner <-
            lift $ (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (fulfillsID, Right joiner, delete k, delete recipID)

    prepareReject isInvite sender = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audRejecter <- makeAudSenderWithFollowers authorIdMsig
        audForbidder <- lift $ makeAudSenderOnly authorIdMsig
        recipHash <- encodeKeyHashid recipKey
        let topicByHash = topicResource recipHash

        senderHash <- bitraverse hashLocalActor pure sender

        uReject <- lift $ getActivityURI authorIdMsig

        let audience =
                if isInvite
                    then
                        let audInviter =
                                case senderHash of
                                    Left actor -> AudLocal [actor] []
                                    Right (ObjURI h lu, _followers) ->
                                        AudRemote h [lu] []
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audInviter, audRejecter, audTopic]
                    else
                        let audJoiner =
                                case senderHash of
                                    Left actor -> AudLocal [actor] [localActorFollowers actor]
                                    Right (ObjURI h lu, followers) ->
                                        AudRemote h [lu] (maybeToList followers)
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audJoiner, audForbidder, audTopic]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uReject]
                , AP.actionSpecific   = AP.RejectActivity AP.Reject
                    { AP.rejectObject = AP.rejectObject reject
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor A invited actor B to a resource
-- Behavior:
--      * If resource is my collaborators collection:
--          * Verify A isn't inviting themselves
--          * Verify A is authorized by me to invite actors to me
--          * Verify B doesn't already have an invite/join/grant for me
--          * Remember the invite in DB
--          * Forward the Invite to my followers
--          * Send Accept to A, B, my-followers
--      * If I'm B, i.e. I'm the one being invited:
--          * Verify the resource is some project's components collection URI
--          * For each Stem record I have for this project:
--              * Verify it's not enabled yet, i.e. I'm not already a component
--                of this project
--              * Verify it's not in Invite-Accept state, already got the
--                project's Accept and waiting for my approval
--              * Verify it's not in Add-Accept state, has my approval and
--                waiting for the project's side
--          * Create a Stem record in DB
--          * Insert the Invite to my inbox
--          * Forward the Invite to my followers
topicInvite
    :: forall topic ct si.
       ( PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic
       , PersistRecordBackend ct SqlBackend
       , PersistRecordBackend si SqlBackend
       )
    => (topic -> ActorId)
    -> (forall f. f topic -> ComponentBy f)
    -> EntityField ct (Key topic)
    -> EntityField ct CollabId
    -> (CollabId -> Key topic -> ct)
    -> (StemId -> Key topic -> si)
    -> UTCTime
    -> Key topic
    -> Verse
    -> AP.Invite URIMode
    -> ActE (Text, Act (), Next)
topicInvite grabActor topicComponent topicField topicCollabField collabTopicCtor stemIdentCtor now topicKey (Verse authorIdMsig body) invite = do

    -- Check invite
    recipOrProject <- do
        let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
        (role, resourceOrComps, recipientOrComp) <- parseInvite author invite
        let collabMode =
                Left (Left $ topicResource topicKey) == resourceOrComps
            compMode =
                Left (Right $ topicComponent topicKey) == recipientOrComp
        case (collabMode, compMode) of
            (False, False) -> throwE "Invite is unrelated to me"
            (True, True) -> throwE "I'm being invited as a collaborator in myself"
            (True, False) -> Left . (role,) <$>
                bitraverse
                    (\case
                        Left r -> pure r
                        Right _ -> throwE "Not accepting component actors as collabs"
                    )
                    pure
                    recipientOrComp
            (False, True) -> Right <$> do
                unless (role == AP.RoleAdmin) $
                    throwE "Invite-component role isn't admin"
                bitraverse
                    (\case
                        Left _ -> throwE "Inviting me to be a collaborator doesn't make sense to me"
                        Right j -> pure j
                    )
                    pure
                    resourceOrComps

    recipOrProjectDB <-
        bitraverse
            (\ (role, targetByKey) -> do

                -- Check capability
                capability <- do

                    -- Verify that a capability is provided
                    uCap <- do
                        let muCap = AP.activityCapability $ actbActivity body
                        fromMaybeE muCap "No capability provided"

                    -- Verify the capability URI is one of:
                    --   * Outbox item URI of a local actor, i.e. a local activity
                    --   * A remote URI
                    cap <- nameExceptT "Invite capability" $ parseActivityURI' uCap

                    -- Verify the capability is local
                    case cap of
                        Left (actorByKey, _, outboxItemID) ->
                            return (actorByKey, outboxItemID)
                        _ -> throwE "Capability is remote i.e. definitely not by me"

                -- If target is local, find it in our DB
                -- If target is remote, HTTP GET it, verify it's an actor, and store in
                -- our DB (if it's already there, no need for HTTP)
                --
                -- NOTE: This is a blocking HTTP GET done right here in the Invite handler,
                -- which is NOT a good idea. Ideally, it would be done async, and the
                -- handler result (approve/disapprove the Invite) would be sent later in a
                -- separate (e.g. Accept) activity. But for the PoC level, the current
                -- situation will hopefully do.
                targetDB <-
                    bitraverse
                        (withDBExcept . flip getGrantRecip "Invitee not found in DB")
                        (\ u@(ObjURI h lu) -> do
                            instanceID <-
                                lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                            result <-
                                ExceptT $ first (T.pack . displayException) <$>
                                    fetchRemoteActor' instanceID h lu
                            case result of
                                Left Nothing -> throwE "Target @id mismatch"
                                Left (Just err) -> throwE $ T.pack $ displayException err
                                Right Nothing -> throwE "Target isn't an actor"
                                Right (Just actor) -> return $ entityKey actor
                        )
                        targetByKey

                return (role, capability, targetByKey, targetDB)
            )

            -- If project is local, find it in our DB
            -- If project is remote, HTTP GET it and store in our DB (if it's already
            -- there, no need for HTTP)
            --
            -- NOTE: This is a blocking HTTP GET done right here in the handler,
            -- which is NOT a good idea. Ideally, it would be done async, and the
            -- handler result would be sent later in a separate (e.g. Accept) activity.
            -- But for the PoC level, the current situation will hopefully do.
            (bitraverse
                (withDBExcept . flip getEntityE "Project not found in DB")
                (\ u@(ObjURI h luComps) -> do
                    manager <- asksEnv envHttpManager
                    collection <-
                        ExceptT $ first T.pack <$>
                            AP.fetchAPID
                                manager
                                (AP.collectionId :: AP.Collection FedURI URIMode -> LocalURI)
                                h
                                luComps
                    luProject <- fromMaybeE (AP.collectionContext collection) "Collection has no context"
                    project <-
                        ExceptT $ first T.pack <$>
                            AP.fetchAPID manager (AP.actorId . AP.actorLocal . AP.projectActor) h luProject
                    unless (AP.projectComponents project == luComps) $
                        throwE "The collection isn't the project's components collection"

                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h luProject
                    case result of
                        Left Nothing -> throwE "Target @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Target isn't an actor"
                        Right (Just actor) -> do
                            unless (remoteActorType (entityVal actor) == AP.ActorTypeProject) $
                                throwE "Remote project type isn't Project"
                            return $ entityKey actor
                )
            )

            recipOrProject

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (topicActorID, topicActor) <- lift $ do
            recip <- getJust topicKey
            let actorID = grabActor recip
            (actorID,) <$> getJust actorID

        case recipOrProjectDB of
            Left (role, capability, _targetByKey, targetDB) -> do

                -- Verify the specified capability gives relevant access
                verifyCapability'
                    capability authorIdMsig (topicResource topicKey) AP.RoleAdmin

                -- Verify that target doesn't already have a Collab for me
                existingCollabIDs <-
                    lift $ case targetDB of
                        Left (GrantRecipPerson (Entity personID _)) ->
                            E.select $ E.from $ \ (topic `E.InnerJoin` recipl) -> do
                                E.on $
                                    topic E.^. topicCollabField E.==.
                                    recipl E.^. CollabRecipLocalCollab
                                E.where_ $
                                    topic E.^. topicField E.==. E.val topicKey E.&&.
                                    recipl E.^. CollabRecipLocalPerson E.==. E.val personID
                                return $ recipl E.^. CollabRecipLocalCollab
                        Right remoteActorID ->
                            E.select $ E.from $ \ (topic `E.InnerJoin` recipr) -> do
                                E.on $
                                    topic E.^. topicCollabField E.==.
                                    recipr E.^. CollabRecipRemoteCollab
                                E.where_ $
                                    topic E.^. topicField E.==. E.val topicKey E.&&.
                                    recipr E.^. CollabRecipRemoteActor E.==. E.val remoteActorID
                                return $ recipr E.^. CollabRecipRemoteCollab
                case existingCollabIDs of
                    [] -> pure ()
                    [_] -> throwE "I already have a Collab for the target"
                    _ -> error "Multiple collabs found for target"

            Right projectDB ->

                -- Find existing Stem records I have for this project
                -- Make sure none are enabled / in Add-Accept mode / in Invite-Accept
                -- mode
                checkExistingStems (topicComponent topicKey) projectDB

        maybeInviteDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
        lift $ for maybeInviteDB $ \ inviteDB -> do

            -- Prepare forwarding Invite to my followers
            sieve <- do
                topicHash <- encodeKeyHashid topicKey
                let topicByHash =
                        topicResource topicHash
                return $ makeRecipientSet [] [localActorFollowers topicByHash]

            -- Insert Collab or Stem record to DB
            -- In Collab mode: Prepare an Accept activity and insert to my
            -- outbox
            maybeAccept <- case recipOrProjectDB of
                Left (role, _capability, targetByKey, targetDB) -> Just <$> do
                    acceptID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
                    insertCollab role targetDB inviteDB acceptID
                    accept@(actionAccept, _, _, _) <- lift $ prepareAccept targetByKey
                    let topicByKey = topicResource topicKey
                    _luAccept <- updateOutboxItem' topicByKey acceptID actionAccept
                    return (acceptID, accept)
                Right projectDB -> do
                    insertStem projectDB inviteDB
                    return Nothing

            return (topicActorID, sieve, maybeAccept)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (topicActorID, sieve, maybeAccept) -> do
            let topicByID = topicResource topicKey
            forwardActivity authorIdMsig body topicByID topicActorID sieve
            lift $ for_ maybeAccept $ \ (acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept)) ->
                sendActivity
                    topicByID topicActorID localRecipsAccept remoteRecipsAccept
                    fwdHostsAccept acceptID actionAccept
            done "Recorded and forwarded the Invite, sent an Accept if collab"

    where

    topicResource :: forall f. f topic -> LocalActorBy f
    topicResource = componentActor . topicComponent

    insertCollab role recipient inviteDB acceptID = do
        collabID <- insert $ Collab role
        fulfillsID <- insert $ CollabFulfillsInvite collabID acceptID
        insert_ $ collabTopicCtor collabID topicKey
        case inviteDB of
            Left (_, _, inviteID) ->
                insert_ $ CollabInviterLocal fulfillsID inviteID
            Right (author, _, inviteID) -> do
                let authorID = remoteAuthorId author
                insert_ $ CollabInviterRemote fulfillsID authorID inviteID
        case recipient of
            Left (GrantRecipPerson (Entity personID _)) ->
                insert_ $ CollabRecipLocal collabID personID
            Right remoteActorID ->
                insert_ $ CollabRecipRemote collabID remoteActorID

    insertStem projectDB inviteDB = do
        stemID <- insert $ Stem AP.RoleAdmin
        insert_ $ stemIdentCtor stemID topicKey
        case projectDB of
            Left (Entity projectID _) ->
                insert_ $ StemProjectLocal stemID projectID
            Right remoteActorID ->
                insert_ $ StemProjectRemote stemID remoteActorID
        originID <- insert $ StemOriginInvite stemID
        case inviteDB of
            Left (_, _, inviteID) ->
                insert_ $ StemProjectGestureLocal originID inviteID
            Right (author, _, inviteID) ->
                insert_ $ StemProjectGestureRemote originID (remoteAuthorId author) inviteID

    prepareAccept invited = do
        encodeRouteHome <- getEncodeRouteHome

        audInviter <- makeAudSenderOnly authorIdMsig
        audInvited <-
            case invited of
                Left (GrantRecipPerson p) -> do
                    ph <- encodeKeyHashid p
                    return $ AudLocal [LocalActorPerson ph] []
                Right (ObjURI h lu) -> return $ AudRemote h [lu] []
        audTopic <-
            AudLocal [] . pure . localActorFollowers .
            topicResource <$>
                encodeKeyHashid topicKey
        uInvite <- getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audInviter, audInvited, audTopic]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uInvite]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uInvite
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

topicRemove
    :: ( PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic
       , PersistRecordBackend ct SqlBackend
       )
    => (topic -> ActorId)
    -> (forall f. f topic -> LocalActorBy f)
    -> EntityField ct (Key topic)
    -> EntityField ct CollabId
    -> UTCTime
    -> Key topic
    -> Verse
    -> AP.Remove URIMode
    -> ActE (Text, Act (), Next)
topicRemove grabActor topicResource topicField topicCollabField now topicKey (Verse authorIdMsig body) remove = do

    -- Check capability
    capability <- do

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the capability URI is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        cap <- nameExceptT "Remove.capability" $ parseActivityURI' uCap

        -- Verify the capability is local
        case cap of
            Left (actorByKey, _, outboxItemID) ->
                return (actorByKey, outboxItemID)
            _ -> throwE "Capability is remote i.e. definitely not by me"

    -- Check remove
    memberByKey <- do
        let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
        (resource, memberOrComp) <- parseRemove author remove
        unless (Left (Left $ topicResource topicKey) == resource) $
            throwE "Remove topic isn't my collabs URI"
        member <-
            bitraverse
                (\case
                    Left m -> pure m
                    Right _ -> throwE "Not accepting component actors as collabs"
                )
                pure
                memberOrComp
        return member

    maybeNew <- withDBExcept $ do

        -- Find member in our DB
        memberDB <-
            bitraverse
                (flip getGrantRecip "Member not found in DB")
                (\ u@(ObjURI h lu) -> (,u) <$> do
                    maybeActor <- lift $ runMaybeT $ do
                        iid <- MaybeT $ getKeyBy $ UniqueInstance h
                        roid <- MaybeT $ getKeyBy $ UniqueRemoteObject iid lu
                        MaybeT $ getBy $ UniqueRemoteActor roid
                    fromMaybeE maybeActor "Remote removee not found in DB"
                )
                memberByKey

        -- Grab me from DB
        (topicActorID, topicActor) <- lift $ do
            recip <- getJust topicKey
            let actorID = grabActor recip
            (actorID,) <$> getJust actorID

        -- Verify the specified capability gives relevant access
        verifyCapability'
            capability authorIdMsig (topicResource topicKey) AP.RoleAdmin

        -- Find the collab that the member already has for me
        existingCollabIDs <-
            lift $ case memberDB of
                Left (GrantRecipPerson (Entity personID _)) ->
                    fmap (map $ over _2 Left) $
                    E.select $ E.from $ \ (topic `E.InnerJoin` recipl) -> do
                        E.on $
                            topic E.^. topicCollabField E.==.
                            recipl E.^. CollabRecipLocalCollab
                        E.where_ $
                            topic E.^. topicField E.==. E.val topicKey E.&&.
                            recipl E.^. CollabRecipLocalPerson E.==. E.val personID
                        return
                            ( topic E.^. persistIdField
                            , recipl E.^. persistIdField
                            , recipl E.^. CollabRecipLocalCollab
                            )
                Right (Entity remoteActorID _, _) ->
                    fmap (map $ over _2 Right) $
                    E.select $ E.from $ \ (topic `E.InnerJoin` recipr) -> do
                        E.on $
                            topic E.^. topicCollabField E.==.
                            recipr E.^. CollabRecipRemoteCollab
                        E.where_ $
                            topic E.^. topicField E.==. E.val topicKey E.&&.
                            recipr E.^. CollabRecipRemoteActor E.==. E.val remoteActorID
                        return
                            ( topic E.^. persistIdField
                            , recipr E.^. persistIdField
                            , recipr E.^. CollabRecipRemoteCollab
                            )
        (E.Value topicID, recipID, E.Value collabID) <-
            case existingCollabIDs of
                [] -> throwE "Remove object isn't a member of me"
                [collab] -> return collab
                _ -> error "Multiple collabs found for removee"

        -- Verify the Collab is enabled
        maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
        Entity enableID (CollabEnable _ grantID) <-
            fromMaybeE maybeEnabled "Remove object isn't a member of me yet"

        -- Verify that at least 1 more enabled Admin collab for me exists
        otherCollabIDs <-
           lift $  E.select $ E.from $ \ (topic `E.InnerJoin` enable) -> do
                E.on $
                    topic E.^. topicCollabField E.==.
                    enable E.^. CollabEnableCollab
                E.where_ $
                    topic E.^. topicField E.==. E.val topicKey E.&&.
                    topic E.^. topicCollabField E.!=. E.val collabID
                return $ topic E.^. topicCollabField
        when (null otherCollabIDs) $
            throwE "No other admins exist, can't remove"

        maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
        lift $ for maybeRemoveDB $ \ _removeDB -> do

            -- Delete the whole Collab record
            deleteBy $ UniqueCollabDelegLocal enableID
            deleteBy $ UniqueCollabDelegRemote enableID
            delete enableID
            case recipID of
                Left (E.Value l) -> do
                    deleteBy $ UniqueCollabRecipLocalJoinCollab l
                    deleteBy $ UniqueCollabRecipLocalAcceptCollab l
                    delete l
                Right (E.Value r) -> do
                    deleteBy $ UniqueCollabRecipRemoteJoinCollab r
                    deleteBy $ UniqueCollabRecipRemoteAcceptCollab r
                    delete r
            delete topicID
            fulfills <- do
                mf <- runMaybeT $ asum
                    [ Left <$> MaybeT (getKeyBy $ UniqueCollabFulfillsLocalTopicCreation collabID)
                    , Right . Left <$> MaybeT (getKeyBy $ UniqueCollabFulfillsInvite collabID)
                    , Right . Right <$> MaybeT (getKeyBy $ UniqueCollabFulfillsJoin collabID)
                    ]
                maybe (error $ "No fulfills for collabID#" ++ show collabID) pure mf
            case fulfills of
                Left fc -> delete fc
                Right (Left fi) -> do
                    deleteBy $ UniqueCollabInviterLocal fi
                    deleteBy $ UniqueCollabInviterRemote fi
                    delete fi
                Right (Right fj) -> do
                    deleteBy $ UniqueCollabApproverLocal fj
                    deleteBy $ UniqueCollabApproverRemote fj
                    delete fj
            delete collabID

            -- Prepare forwarding Remove to my followers
            sieve <- lift $ do
                topicHash <- encodeKeyHashid topicKey
                let topicByHash =
                        topicResource topicHash
                return $ makeRecipientSet [] [localActorFollowers topicByHash]

            -- Prepare a Revoke activity and insert to my outbox
            revoke@(actionRevoke, _, _, _) <-
                lift $ prepareRevoke memberDB grantID
            let recipByKey = topicResource topicKey
            revokeID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
            _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke

            return (topicActorID, sieve, revokeID, revoke)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (topicActorID, sieve, revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke)) -> do
            let topicByID = topicResource topicKey
            forwardActivity authorIdMsig body topicByID topicActorID sieve
            lift $ sendActivity
                topicByID topicActorID localRecipsRevoke
                remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
            done "Deleted the Grant/Collab, forwarded Remove, sent Revoke"

    where

    prepareRevoke member grantID = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        recipHash <- encodeKeyHashid topicKey
        let topicByHash = topicResource recipHash

        memberHash <- bitraverse (hashGrantRecip . bmap entityKey) pure member

        audRemover <- makeAudSenderOnly authorIdMsig
        let audience =
                let audMember =
                        case memberHash of
                            Left (GrantRecipPerson p) ->
                                AudLocal [LocalActorPerson p] [LocalStagePersonFollowers p]
                            Right (Entity _ actor, ObjURI h lu) ->
                                AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)
                    audTopic = AudLocal [] [localActorFollowers topicByHash]
                in  [audRemover, audMember, audTopic]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
        uRemove <- getActivityURI authorIdMsig
        luGrant <- do
            grantHash <- encodeKeyHashid grantID
            return $ encodeRouteLocal $ activityRoute topicByHash grantHash
        let action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uRemove]
                , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                    { AP.revokeObject = luGrant :| []
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

topicJoin
    :: ( PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic
       , PersistRecordBackend ct SqlBackend
       )
    => (topic -> ActorId)
    -> (forall f. f topic -> LocalActorBy f)
    -> EntityField ct (Key topic)
    -> EntityField ct CollabId
    -> (CollabId -> Key topic -> ct)
    -> UTCTime
    -> Key topic
    -> Verse
    -> AP.Join URIMode
    -> ActE (Text, Act (), Next)
topicJoin grabActor topicResource topicField topicCollabField collabTopicCtor now topicKey (Verse authorIdMsig body) join = do

    -- Check input
    (role, resource) <- parseJoin join
    unless (resource == Left (topicResource topicKey)) $
        throwE "Join's object isn't my collabs URI, don't need this Join"

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (topicActorID, topicActor) <- lift $ do
            recip <- getJust topicKey
            let actorID = grabActor recip
            (actorID,) <$> getJust actorID

        -- Verify that target doesn't already have a Collab for me
        existingCollabIDs <- lift $
            case authorIdMsig of
                Left (LocalActorPerson personID, _, _) ->
                    E.select $ E.from $ \ (topic `E.InnerJoin` recipl) -> do
                        E.on $
                            topic E.^. topicCollabField E.==.
                            recipl E.^. CollabRecipLocalCollab
                        E.where_ $
                            topic E.^. topicField E.==. E.val topicKey E.&&.
                            recipl E.^. CollabRecipLocalPerson E.==. E.val personID
                        return $ recipl E.^. CollabRecipLocalCollab
                Left (_, _, _) -> pure []
                Right (author, _, _) -> do
                    let targetID = remoteAuthorId author
                    E.select $ E.from $ \ (topic `E.InnerJoin` recipr) -> do
                        E.on $
                            topic E.^. topicCollabField E.==.
                            recipr E.^. CollabRecipRemoteCollab
                        E.where_ $
                            topic E.^. topicField E.==. E.val topicKey E.&&.
                            recipr E.^. CollabRecipRemoteActor E.==. E.val targetID
                        return $ recipr E.^. CollabRecipRemoteCollab
        case existingCollabIDs of
            [] -> pure ()
            [_] -> throwE "I already have a Collab for the target"
            _ -> error "Multiple collabs found for target"

        maybeJoinDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
        for maybeJoinDB $ \ joinDB -> do

            -- Insert Collab record to DB
            joinDB' <-
                bitraverse
                    (\ (authorByKey, _, joinID) ->
                        case authorByKey of
                            LocalActorPerson personID -> pure (personID, joinID)
                            _ -> throwE "Non-person local actors can't get Grants currently"
                    )
                    pure
                    joinDB
            lift $ insertCollab role joinDB'

            -- Prepare forwarding Join to my followers
            sieve <- lift $ do
                topicHash <- encodeKeyHashid topicKey
                let topicByHash =
                        topicResource topicHash
                return $ makeRecipientSet [] [localActorFollowers topicByHash]
            return (topicActorID, sieve)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (topicActorID, sieve) -> do
            let topicByID = topicResource topicKey
            forwardActivity authorIdMsig body topicByID topicActorID sieve
            done "Recorded and forwarded the Join"

    where

    insertCollab role joinDB = do
        collabID <- insert $ Collab role
        fulfillsID <- insert $ CollabFulfillsJoin collabID
        insert_ $ collabTopicCtor collabID topicKey
        case joinDB of
            Left (personID, joinID) -> do
                recipID <- insert $ CollabRecipLocal collabID personID
                insert_ $ CollabRecipLocalJoin recipID fulfillsID joinID
            Right (author, _, joinID) -> do
                let authorID = remoteAuthorId author
                recipID <- insert $ CollabRecipRemote collabID authorID
                insert_ $ CollabRecipRemoteJoin recipID fulfillsID joinID

topicCreateMe
    :: ( PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic
       , PersistRecordBackend ct SqlBackend
       )
    => (topic -> ActorId)
    -> (forall f. f topic -> LocalActorBy f)
    -> EntityField ct (Key topic)
    -> (CollabId -> Key topic -> ct)
    -> UTCTime
    -> Key topic
    -> Verse
    -> ActE (Text, Act (), Next)
topicCreateMe topicActor topicResource collabTopicFieldTopic collabTopicCtor now recipKey (Verse authorIdMsig body) = do

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (recipActorID, recipActor) <- lift $ do
            recip <- getJust recipKey
            let actorID = topicActor recip
            (actorID,) <$> getJust actorID

        -- Verify I'm in the initial just-been-created state
        creatorActorID <-
            fromMaybeE
                (actorJustCreatedBy recipActor)
                "I already sent the initial Grant, why am I receiving this Create?"
        creatorPersonID <- do
            mp <- lift $ getKeyBy $ UniquePersonActor creatorActorID
            fromMaybeE mp "Granting access to local non-Person actors isn't suppported currently"
        existingCollabIDs <-
            lift $ selectList [collabTopicFieldTopic ==. recipKey] []
        unless (null existingCollabIDs) $
            error "Just-been-created but I somehow already have Collabs"

        -- Verify the Create author is my creator indeed
        case authorIdMsig of
            Left (_, actorID, _) | actorID == creatorActorID -> pure ()
            _ -> throwE "Create author isn't why I believe my creator is - is this Create fake?"

        maybeCreateDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
        lift $ for maybeCreateDB $ \ _createDB -> do

            -- Create a Collab record and exit just-been-created state
            grantID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
            insertCollab creatorPersonID grantID
            update creatorActorID [ActorJustCreatedBy =. Nothing]

            -- Prepare a Grant activity and insert to my outbox
            grant@(actionGrant, _, _, _) <- lift prepareGrant
            let recipByKey = topicResource recipKey
            _luGrant <- updateOutboxItem' recipByKey grantID actionGrant

            return (recipActorID, grantID, grant)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (recipActorID, grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant)) -> do
            let recipByID = topicResource recipKey
            lift $ sendActivity
                recipByID recipActorID localRecipsGrant
                remoteRecipsGrant fwdHostsGrant grantID actionGrant
            done "Created a Collab record and published a Grant"

    where

    insertCollab personID grantID = do
        collabID <- insert $ Collab AP.RoleAdmin
        insert_ $ collabTopicCtor collabID recipKey
        insert_ $ CollabEnable collabID grantID
        insert_ $ CollabRecipLocal collabID personID
        insert_ $ CollabFulfillsLocalTopicCreation collabID

    prepareGrant = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audCreator <- makeAudSenderOnly authorIdMsig
        recipHash <- encodeKeyHashid recipKey
        uCreator <- getActorURI authorIdMsig
        uCreate <- getActivityURI authorIdMsig
        let topicByHash = topicResource recipHash
            audience =
                let audTopic = AudLocal [] [localActorFollowers topicByHash]
                in  [audCreator, audTopic]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uCreate]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXRole AP.RoleAdmin
                    , AP.grantContext   =
                        encodeRouteHome $ renderLocalActor topicByHash
                    , AP.grantTarget    = uCreator
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor is granting access-to-some-resource to another actor
-- Behavior:
--      * If I approved an Add-to-project where I'm the component, and the
--        project is now giving me the delegator-Grant:
--          * Record this in the Stem record in DB
--          * Forward to my followers
--          * Start a delegation chain giving access-to-me, send this new Grant
--            to the project to distribute further, and use the delegator-Grant
--            as the capability
--              * To: Project
--              * CC: My followers, project followers
--      * If I approved an Invite-to-project where I'm the component, and the
--        project is now giving me the delegator-Grant:
--          * Record this in the Stem record in DB
--          * Forward to my followers
--          * Start a delegation chain giving access-to-me, send this new Grant
--            to the project to distribute further, and use the delegator-Grant
--            as the capability
--              * To: Project
--              * CC: My followers, project followers
--      * If the Grant is for an Add/Invite that hasn't had the full approval
--        chain, or I already got the delegator-Grant, raise an error
--      * Otherwise, if I've already seen this Grant or it's simply not related
--        to me, ignore it
componentGrant
    :: forall topic.
       (PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic)
    => (topic -> ActorId)
    -> (forall f. f topic -> ComponentBy f)
    -> UTCTime
    -> Key topic
    -> Verse
    -> AP.Grant URIMode
    -> ActE (Text, Act (), Next)
componentGrant grabActor topicComponent now recipKey (Verse authorIdMsig body) grant = do

    -- Check grant
    project <- checkDelegatorGrant grant

    -- Check the Add/Invite that it's related to
    fulfills <-
        case AP.activityFulfills $ actbActivity body of
            [u] ->
                first (\ (actor, _, item) -> (actor, item)) <$>
                    nameExceptT "Grant.fulfills" (parseActivityURI' u)
            _ -> throwE "Expecting a single 'fulfills' URI"

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (recipActorID, recipActor) <- lift $ do
            recip <- getJust recipKey
            let actorID = grabActor recip
            (actorID,) <$> getJust actorID

        -- Find the fulfilled activity in our DB
        fulfillsDB <- do
            a <- getActivity fulfills
            fromMaybeE a "Can't find fulfilled in DB"

        -- See if the fulfilled activity is an Invite or Add on a local
        -- component, grabbing the Stem record from our DB
        stem <- do
            maybeStem <-
                lift $ runMaybeT $
                    Left <$> tryInviteComp fulfillsDB <|>
                    Right <$> tryAddComp fulfillsDB
            fromMaybeE maybeStem "Fulfilled activity isn't an Invite/Add I'm aware of"

        -- Find the local component and verify it's me
        let stemID = either id id stem
        ident <- lift $ getStemIdent stemID
        unless (topicComponent recipKey == ident) $
            throwE "Fulfilled object is an Invite/Add for some other component"

        -- Find the project, verify it's identical to the Grant sender
        stemProject <-
            lift $
            requireEitherAlt
                (getBy $ UniqueStemProjectLocal stemID)
                (getBy $ UniqueStemProjectRemote stemID)
                "Found Stem with no project"
                "Found Stem with multiple projects"
        case (stemProject, authorIdMsig) of
            (Left (Entity _ sjl), Left (LocalActorProject projectID, _, _))
                | stemProjectLocalProject sjl == projectID ->
                    return ()
            (Right (Entity _ sjr), Right (author, _, _))
                | stemProjectRemoteProject sjr == remoteAuthorId author ->
                    return ()
            _ -> throwE "The Grant I'm waiting for is by the project"

        -- Verify I sent the Component's Accept but haven't already received
        -- the delegator-Grant
        compAccept <- do
            mk <- lift $ getKeyBy $ UniqueStemComponentAccept stemID
            fromMaybeE mk "Getting a delegator-Grant but never approved this Invite/Add"
        gl <- lift $ getBy $ UniqueStemProjectGrantLocal compAccept
        gr <- lift $ getBy $ UniqueStemProjectGrantRemote compAccept
        unless (isNothing gl && isNothing gr) $
            throwE "I already received a delegator-Grant for this Invite/Add"

        maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
        lift $ for maybeGrantDB $ \ grantDB -> do

            -- Prepare forwarding to my followers
            sieve <- do
                recipHash <- encodeKeyHashid recipKey
                let recipByHash =
                        topicResource recipHash
                return $ makeRecipientSet [] [localActorFollowers recipByHash]

            -- Update the Stem record in DB
            case (stemProject, grantDB) of
                (Left (Entity j _), Left (_, _, g)) -> insert_ $ StemProjectGrantLocal compAccept j g
                (Right (Entity j _), Right (_, _, g)) -> insert_ $ StemProjectGrantRemote compAccept j g
                _ -> error "componentGrant impossible"
            chainID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
            insert_ $ StemDelegateLocal compAccept chainID

            -- Prepare a Grant activity and insert to my outbox
            chain <- do
                Stem role <- getJust stemID
                chain@(actionChain, _, _, _) <- prepareChain role
                let recipByKey = topicResource recipKey
                _luChain <- updateOutboxItem' recipByKey chainID actionChain
                return chain

            return (recipActorID, sieve, chainID, chain)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (recipActorID, sieve, chainID, (actionChain, localRecipsChain, remoteRecipsChain, fwdHostsChain)) -> do
            let recipByID = topicResource recipKey
            forwardActivity authorIdMsig body recipByID recipActorID sieve
            lift $ sendActivity
                recipByID recipActorID localRecipsChain remoteRecipsChain
                fwdHostsChain chainID actionChain
            done "Recorded and forwarded the delegator-Grant, sent a delegation-starter Grant"

    where

    topicResource :: forall f. f topic -> LocalActorBy f
    topicResource = componentActor . topicComponent

    checkDelegatorGrant g = do
        (role, resource, recipient, _mresult, mstart, mend, usage, mdeleg) <-
            parseGrant' g
        case role of
            AP.RXRole _ -> throwE "Not a delegator Grant"
            AP.RXDelegator -> pure ()
        project <-
            bitraverse
                (\case
                    LocalActorProject j -> return j
                    _ -> throwE "Resource isn't a project"
                )
                pure
                resource
        case (project, authorIdMsig) of
            (Left j, Left (a, _, _)) | LocalActorProject j == a -> pure ()
            (Right u, Right (ra, _, _)) | remoteAuthorURI ra == u -> pure ()
            _ -> throwE "Author and resource aren't the same project actor"
        case recipient of
            Left la | topicResource recipKey == la -> pure ()
            _ -> throwE "Grant recipient isn't me"
        for_ mstart $ \ start ->
            unless (start < now) $ throwE "Start time is in the future"
        for_ mend $ \ _ ->
            throwE "End time is specified"
        unless (usage == AP.Invoke) $
            throwE "Usage isn't Invoke"
        for_ mdeleg $ \ _ ->
            throwE "'delegates' is specified"
        return project

    tryInviteComp (Left (_, _, itemID)) = do
        originInviteID <-
            stemProjectGestureLocalOrigin <$>
                MaybeT (getValBy $ UniqueStemProjectGestureLocalInvite itemID)
        lift $ stemOriginInviteStem <$> getJust originInviteID
    tryInviteComp (Right remoteActivityID) = do
        StemProjectGestureRemote originInviteID _ _ <-
            MaybeT $ getValBy $
                UniqueStemProjectGestureRemoteInvite remoteActivityID
        lift $ stemOriginInviteStem <$> getJust originInviteID

    tryAddComp (Left (_, __, itemID)) = do
        StemComponentGestureLocal stemID _ <-
            MaybeT $ getValBy $ UniqueStemComponentGestureLocalActivity itemID
        _originID <- MaybeT $ getKeyBy $ UniqueStemOriginAdd stemID
        return stemID
    tryAddComp (Right remoteActivityID) = do
        StemComponentGestureRemote stemID _ _ <-
            MaybeT $ getValBy $
                UniqueStemComponentGestureRemoteActivity remoteActivityID
        _originID <- MaybeT $ getKeyBy $ UniqueStemOriginAdd stemID
        return stemID

    prepareChain role = do
        encodeRouteHome <- getEncodeRouteHome

        audProject <- makeAudSenderWithFollowers authorIdMsig
        audMe <-
            AudLocal [] . pure . localActorFollowers .
            topicResource <$>
                encodeKeyHashid recipKey
        uProject <- lift $ getActorURI authorIdMsig
        uGrant <- lift $ getActivityURI authorIdMsig
        recipHash <- encodeKeyHashid recipKey
        let topicByHash = topicResource recipHash

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audProject, audMe]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Just uGrant
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uGrant]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXRole role
                    , AP.grantContext   =
                        encodeRouteHome $ renderLocalActor topicByHash
                    , AP.grantTarget    = uProject
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.GatherAndConvey
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)
