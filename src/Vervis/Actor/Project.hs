{- This file is part of Vervis.
 -
 - Written in 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Actor.Project
    (
    )
where

import Control.Applicative
import Control.Exception.Base
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Barbie
import Data.Bifoldable
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Either
import Data.Foldable
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Optics.Core
import Yesod.Persist.Core

import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Database.Persist.Local

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Actor
import Vervis.Actor.Common
import Vervis.Actor2
import Vervis.Cloth
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.FedURI
import Vervis.Federation.Util
import Vervis.Foundation
import Vervis.Model hiding (projectCreate)
import Vervis.Recipient (makeRecipientSet, LocalStageBy (..), Aud (..), collectAudience, localActorFollowers, renderLocalActor)
import Vervis.RemoteActorStore
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.Ticket

-- Meaning: An actor accepted something
-- Behavior:
--     * Check if I know the activity that's being Accepted:
--         * Is it an Invite to be a collaborator in me?
--             * Verify the Accept is by the Invite target
--         * Is it a Join to be a collaborator in me?
--             * Verify the Accept is authorized
--         * Is it an Invite to be a component of me?
--             * Nothing to check at this point
--         * Is it an Add to be a component of me?
--             * If the sender is the component:
--                 * Verify I haven't seen a component-Accept on this Add
--             * Otherwise, i.e. sender isn't the component:
--                 * Verify I've seen the component-Accept for this Add
--                 * Verify the new Accept is authorized
--         * If it's none of these, respond with error
--
--     * In collab mode, verify the Collab isn't enabled yet
--     * In component mode, verify the Component isn't enabled yet
--
--     * Insert the Accept to my inbox
--
--     * In collab mode, record the Accept and enable the Collab in DB
--     * In Invite-component mode,
--         * If sender is component, record the Accept and enable the Component
--           in DB
--         * Otherwise, nothing at this point
--     * In Add-component mode,
--         * If the sender is the component, record the Accept into the
--           Component record in DB
--         * Otherwise, i.e. sender isn't the component, record the Accept and
--           enable the Component in DB
--
--     * Forward the Accept to my followers
--
--     * Possibly send a Grant:
--         * For Invite-collab mode:
--             * Regular collaborator-Grant
--             * To: Accepter (i.e. Invite target)
--             * CC: Invite sender, Accepter's followers, my followers
--         * For Join-as-collab mode:
--             * Regular collaborator-Grant
--             * To: Join sender
--             * CC: Accept sender, Join sender's followers, my followers
--         * For Invite-component mode:
--             * Only if sender is the component
--             * delegator-Grant
--             * To: Component
--             * CC:
--                 - Component's followers
--                 - My followers
--         * For Add-component mode:
--             * Only if sender isn't the component
--             * delegator-Grant
--             * To: Component
--             * CC:
--                 - Component's followers
--                 - My followers
--                 - The Accept's sender
projectAccept
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Accept URIMode
    -> ActE (Text, Act (), Next)
projectAccept now projectID (Verse authorIdMsig body) accept = do

    -- Check input
    acceptee <- parseAccept accept

    -- Verify that the capability URI, if specified, is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCap <-
        traverse
            (nameExceptT "Accept capability" . parseActivityURI')
            (AP.activityCapability $ actbActivity body)

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (recipActorID, recipActor) <- lift $ do
            recip <- getJust projectID
            let actorID = projectActor recip
            (actorID,) <$> getJust actorID

        -- Find the accepted activity in our DB
        accepteeDB <- do
            a <- getActivity acceptee
            fromMaybeE a "Can't find acceptee in DB"

        -- See if the accepted activity is an Invite or Join where my collabs
        -- URI is the resource, grabbing the Collab record from our DB,
        -- Or if the accepted activity is an Invite or Add where my components
        -- URI is the resource, grabbing the Component record from our DB
        collabOrComp <- do
            let adapt = maybe (Right Nothing) (either Left (Right . Just))
            maybeCollab <-
                ExceptT $ fmap adapt $ runMaybeT $
                    runExceptT (Left <$> tryInviteCollab accepteeDB) <|>
                    runExceptT (Left <$> tryJoinCollab accepteeDB) <|>
                    runExceptT (Right <$> tryInviteComp accepteeDB) <|>
                    runExceptT (Right <$> tryAddComp accepteeDB)
            fromMaybeE
                maybeCollab
                "Accepted activity isn't an Invite/Join/Add I'm aware of"

        idsForAccept <- bitraverse

            (\ (collabID, fulfills, inviterOrJoiner) -> (collabID,inviterOrJoiner,) <$> bitraverse

                -- If accepting an Invite, find the Collab recipient and verify
                -- it's the sender of the Accept
                (\ fulfillsID -> do
                    recip <-
                        lift $
                        requireEitherAlt
                            (getBy $ UniqueCollabRecipLocal collabID)
                            (getBy $ UniqueCollabRecipRemote collabID)
                            "Found Collab with no recip"
                            "Found Collab with multiple recips"
                    case (recip, authorIdMsig) of
                        (Left (Entity crlid crl), Left (LocalActorPerson personID, _, _))
                            | collabRecipLocalPerson crl == personID ->
                                return (fulfillsID, Left crlid)
                        (Right (Entity crrid crr), Right (author, _, _))
                            | collabRecipRemoteActor crr == remoteAuthorId author ->
                                return (fulfillsID, Right crrid)
                        _ -> throwE "Accepting an Invite whose recipient is someone else"
                )

                -- If accepting a Join, verify accepter has permission
                (\ fulfillsID -> do
                    capID <- fromMaybeE maybeCap "No capability provided"
                    capability <-
                        case capID of
                            Left (capActor, _, capItem) -> return (capActor, capItem)
                            Right _ -> throwE "Capability is a remote URI, i.e. not authored by the local resource"
                    verifyCapability'
                        capability
                        authorIdMsig
                        (LocalActorProject projectID)
                        AP.RoleAdmin
                    return fulfillsID
                )

                fulfills
            )

            (\ (componentID, ident, inviteOrAdd) -> (componentID, ident,) <$> bitraverse

                -- If accepting an Invite-component, there's nothing to check
                -- at this point
                pure

                -- If accepting an Add-component:
                --     * If the sender is the component, verify I haven't seen
                --       a component-Accept on this Add
                --     * Otherwise, verify I've seen the component-Accept for
                --       this Add and that the new Accept is authorized
                (\ () -> do
                    maybeComponentAccept <-
                        lift $
                        case bimap fst fst ident of
                            Left localID -> (() <$) <$> getBy (UniqueComponentAcceptLocal localID)
                            Right remoteID -> (() <$) <$> getBy (UniqueComponentAcceptRemote remoteID)
                    if componentIsAuthor ident
                        then
                            verifyNothingE
                                maybeComponentAccept
                                    "I've already seen a ComponentAccept* on \
                                    \that Add"
                        else do
                            fromMaybeE
                                maybeComponentAccept
                                "I haven't yet seen the Component's Accept on \
                                \the Add"
                            capID <- fromMaybeE maybeCap "No capability provided"
                            capability <-
                                case capID of
                                    Left (capActor, _, capItem) -> return (capActor, capItem)
                                    Right _ -> throwE "Capability is a remote URI, i.e. not authored by me"
                            verifyCapability'
                                capability
                                authorIdMsig
                                (LocalActorProject projectID)
                                AP.RoleAdmin
                )

                inviteOrAdd
            )

            collabOrComp

        -- In collab mode, verify the Collab isn't already validated
        -- In component mode, verify the Component isn't already validated
        bitraverse_
            (\ (collabID, _, _) -> do
                maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
                verifyNothingE maybeEnabled "I already sent a Grant for this Invite/Join"
            )
            (\ (componentID, _, _) -> do
                maybeEnabled <- lift $ getBy $ UniqueComponentEnable componentID
                verifyNothingE maybeEnabled "I already sent a delegator-Grant for this Invite/Add"
            )
            collabOrComp

        maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
        for maybeAcceptDB $ \ acceptDB -> do

            idsForGrant <- case idsForAccept of

                -- In collab mode, record the Accept and enable the Collab
                Left (collabID, inviterOrJoiner, collab) -> Left <$> do
                    case (collab, acceptDB) of
                        (Left (fulfillsID, Left recipID), Left (_, _, acceptID)) -> do
                            maybeAccept <- lift $ insertUnique $ CollabRecipLocalAccept recipID fulfillsID acceptID
                            unless (isJust maybeAccept) $
                                throwE "This Invite already has an Accept by recip"
                        (Left (fulfillsID, Right recipID), Right (_, _, acceptID)) -> do
                            maybeAccept <- lift $ insertUnique $ CollabRecipRemoteAccept recipID fulfillsID acceptID
                            unless (isJust maybeAccept) $
                                throwE "This Invite already has an Accept by recip"
                        (Right fulfillsID, Left (_, _, acceptID)) -> do
                            maybeAccept <- lift $ insertUnique $ CollabApproverLocal fulfillsID acceptID
                            unless (isJust maybeAccept) $
                                throwE "This Join already has an Accept"
                        (Right fulfillsID, Right (author, _, acceptID)) -> do
                            maybeAccept <- lift $ insertUnique $ CollabApproverRemote fulfillsID (remoteAuthorId author) acceptID
                            unless (isJust maybeAccept) $
                                throwE "This Join already has an Accept"
                        _ -> error "projectAccept impossible"
                    grantID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                    enableID <- lift $ insert $ CollabEnable collabID grantID
                    return (collabID, inviterOrJoiner, collab, grantID, enableID)

                -- In Invite-component mode, only if the Accept author is the
                -- component, record the Accept and enable the Component
                Right (componentID, ident, Left ()) -> fmap Right $
                    lift $ if componentIsAuthor ident
                        then Just <$> do
                            case (ident, acceptDB) of
                                (Left (localID, _), Left (_, _, acceptID)) ->
                                    insert_ $ ComponentAcceptLocal localID acceptID
                                (Right (remoteID, _), Right (_, _, acceptID)) ->
                                    insert_ $ ComponentAcceptRemote remoteID acceptID
                                _ -> error "personAccept impossible ii"
                            grantID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                            enableID <- insert $ ComponentEnable componentID grantID
                            return (componentID, ident, grantID, enableID, False)
                        else pure Nothing

                -- In Add-component mode:
                -- * If the sender is the component, record the Accept
                -- * Otherwise, record the Accept and enable the Component
                Right (componentID, ident, Right ()) -> fmap Right $
                    lift $ if componentIsAuthor ident
                        then do
                            case (ident, acceptDB) of
                                (Left (localID, _), Left (_, _, acceptID)) ->
                                    insert_ $ ComponentAcceptLocal localID acceptID
                                (Right (remoteID, _), Right (_, _, acceptID)) ->
                                    insert_ $ ComponentAcceptRemote remoteID acceptID
                                _ -> error "personAccept impossible iii"
                            return Nothing
                        else Just <$> do
                            case acceptDB of
                                Left (_, _, acceptID) ->
                                    insert_ $ ComponentProjectGestureLocal componentID acceptID
                                Right (author, _, acceptID) ->
                                    insert_ $ ComponentProjectGestureRemote componentID (remoteAuthorId author) acceptID
                            grantID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                            enableID <- insert $ ComponentEnable componentID grantID
                            return (componentID, ident, grantID, enableID, True)

            -- Prepare forwarding of Accept to my followers
            let recipByID = LocalActorProject projectID
            recipByHash <- hashLocalActor recipByID
            let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

            maybeGrant <-
                case idsForGrant of

                    -- In collab mode, prepare a regular Grant
                    Left (collabID, inviterOrJoiner, collab, grantID, collabEnableID) -> lift $ do
                        let isInvite = isLeft collab
                        grant@(actionGrant, _, _, _) <- do
                            Collab role <- getJust collabID
                            prepareCollabGrant isInvite inviterOrJoiner role
                        let recipByKey = LocalActorProject projectID
                        _luGrant <- updateOutboxItem' recipByKey grantID actionGrant
                        return $ Just (grantID, grant)

                    -- In Invite-component mode, only if the Accept author is
                    -- the component, prepare a delegator-Grant
                    --
                    -- In Add-component mode, only if the Accept author isn't
                    -- the component, prepare a delegator-Grant
                    Right comp -> for comp $ \ (_componentID, ident, grantID, enableID, includeAuthor) -> lift $ do
                        grant@(actionGrant, _, _, _) <-
                            prepareDelegGrant (bimap snd snd ident) enableID includeAuthor
                        let recipByKey = LocalActorProject projectID
                        _luGrant <- updateOutboxItem' recipByKey grantID actionGrant
                        return (grantID, grant)

            return (recipActorID, sieve, maybeGrant)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (recipActorID, sieve, maybeGrant) -> do
            let recipByID = LocalActorProject projectID
            forwardActivity authorIdMsig body recipByID recipActorID sieve
            lift $ for_ maybeGrant $ \ (grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant)) ->
                sendActivity
                    recipByID recipActorID localRecipsGrant
                    remoteRecipsGrant fwdHostsGrant grantID actionGrant
            done "Forwarded the Accept and maybe published a Grant"

    where

    verifyCollabTopic collabID = do
        topic <- lift $ getCollabTopic collabID
        unless (LocalActorProject projectID == topic) $
            throwE "Accept object is an Invite/Join for some other resource"

    verifyInviteCollabTopic fulfillsID = do
        collabID <- lift $ collabFulfillsInviteCollab <$> getJust fulfillsID
        verifyCollabTopic collabID
        return collabID

    verifyJoinCollabTopic fulfillsID = do
        collabID <- lift $ collabFulfillsJoinCollab <$> getJust fulfillsID
        verifyCollabTopic collabID
        return collabID

    tryInviteCollab (Left (actorByKey, _actorEntity, itemID)) = do
        fulfillsID <-
            lift $ collabInviterLocalCollab <$>
                MaybeT (getValBy $ UniqueCollabInviterLocalInvite itemID)
        collabID <-
            ExceptT $ lift $ runExceptT $ verifyInviteCollabTopic fulfillsID
        return (collabID, Left fulfillsID, Left actorByKey)
    tryInviteCollab (Right remoteActivityID) = do
        CollabInviterRemote fulfillsID actorID _ <-
            lift $ MaybeT $ getValBy $
                UniqueCollabInviterRemoteInvite remoteActivityID
        collabID <-
            ExceptT $ lift $ runExceptT $ verifyInviteCollabTopic fulfillsID
        sender <- lift $ lift $ do
            actor <- getJust actorID
            (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (collabID, Left fulfillsID, Right sender)

    tryJoinCollab (Left (actorByKey, _actorEntity, itemID)) = do
        fulfillsID <-
            lift $ collabRecipLocalJoinFulfills <$>
                MaybeT (getValBy $ UniqueCollabRecipLocalJoinJoin itemID)
        collabID <-
            ExceptT $ lift $ runExceptT $ verifyJoinCollabTopic fulfillsID
        return (collabID, Right fulfillsID, Left actorByKey)
    tryJoinCollab (Right remoteActivityID) = do
        CollabRecipRemoteJoin recipID fulfillsID _ <-
            lift $ MaybeT $ getValBy $
                UniqueCollabRecipRemoteJoinJoin remoteActivityID
        collabID <-
            ExceptT $ lift $ runExceptT $ verifyJoinCollabTopic fulfillsID
        joiner <- lift $ lift $ do
            remoteActorID <- collabRecipRemoteActor <$> getJust recipID
            actor <- getJust remoteActorID
            (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (collabID, Right fulfillsID, Right joiner)

    verifyCompTopic :: ComponentId -> ActDBE ()
    verifyCompTopic componentID = do
        Component j _ <- lift $ getJust componentID
        unless (j == projectID) $
            throwE "Accept object is an Invite/Add for some other project"

    tryInviteComp (Left (actorByKey, _actorEntity, itemID)) = do
        ComponentProjectGestureLocal componentID _ <-
            lift $ MaybeT $ getValBy $
                UniqueComponentProjectGestureLocalActivity itemID
        _ <- lift $ MaybeT $ getBy $ UniqueComponentOriginInvite componentID
        ExceptT $ lift $ runExceptT $ verifyCompTopic componentID
        ident <- lift $ lift $ getComponentIdent componentID
        return (componentID, ident, Left ())
    tryInviteComp (Right remoteActivityID) = do
        ComponentProjectGestureRemote componentID _ _ <-
            lift $ MaybeT $ getValBy $
                UniqueComponentProjectGestureRemoteActivity remoteActivityID
        _ <- lift $ MaybeT $ getBy $ UniqueComponentOriginInvite componentID
        ExceptT $ lift $ runExceptT $ verifyCompTopic componentID
        ident <- lift $ lift $ getComponentIdent componentID
        return (componentID, ident, Left ())

    tryAddComp (Left (actorByKey, _actorEntity, itemID)) = do
        ComponentGestureLocal originID _ <-
            lift $ MaybeT $ getValBy $ UniqueComponentGestureLocalAdd itemID
        ComponentOriginAdd componentID <- lift $ lift $ getJust originID
        ExceptT $ lift $ runExceptT $ verifyCompTopic componentID
        ident <- lift $ lift $ getComponentIdent componentID
        return (componentID, ident, Right ())
    tryAddComp (Right remoteActivityID) = do
        ComponentGestureRemote originID _ _ <-
            lift $ MaybeT $ getValBy $
                UniqueComponentGestureRemoteAdd remoteActivityID
        ComponentOriginAdd componentID <- lift $ lift $ getJust originID
        ExceptT $ lift $ runExceptT $ verifyCompTopic componentID
        ident <- lift $ lift $ getComponentIdent componentID
        return (componentID, ident, Right ())

    componentIsAuthor ident =
        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        in  author == bimap (componentActor . snd) snd ident

    prepareCollabGrant isInvite sender role = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audAccepter <- makeAudSenderWithFollowers authorIdMsig
        audApprover <- lift $ makeAudSenderOnly authorIdMsig
        recipHash <- encodeKeyHashid projectID
        let topicByHash = LocalActorProject recipHash

        senderHash <- bitraverse hashLocalActor pure sender

        uAccepter <- lift $ getActorURI authorIdMsig

        let audience =
                if isInvite
                    then
                        let audInviter =
                                case senderHash of
                                    Left actor -> AudLocal [actor] []
                                    Right (ObjURI h lu, _followers) ->
                                        AudRemote h [lu] []
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audInviter, audAccepter, audTopic]
                    else
                        let audJoiner =
                                case senderHash of
                                    Left actor -> AudLocal [actor] [localActorFollowers actor]
                                    Right (ObjURI h lu, followers) ->
                                        AudRemote h [lu] (maybeToList followers)
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audJoiner, audApprover, audTopic]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [AP.acceptObject accept]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXRole role
                    , AP.grantContext   =
                        encodeRouteHome $ renderLocalActor topicByHash
                    , AP.grantTarget    =
                        if isInvite
                            then uAccepter
                            else case senderHash of
                                Left actor ->
                                    encodeRouteHome $ renderLocalActor actor
                                Right (ObjURI h lu, _) -> ObjURI h lu
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    prepareDelegGrant ident _enableID includeAuthor = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        (uComponent, audComponent) <-
            case ident of
                Left c -> do
                    a <- componentActor <$> hashComponent c
                    return
                        ( encodeRouteHome $ renderLocalActor a
                        , AudLocal [a] [localActorFollowers a]
                        )
                Right raID -> do
                    ra <- getJust raID
                    u@(ObjURI h lu) <- getRemoteActorURI ra
                    return
                        ( u
                        , AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
                        )
        audAuthor <- lift $ makeAudSenderOnly authorIdMsig
        projectHash <- encodeKeyHashid projectID
        let audProject = AudLocal [] [LocalStageProjectFollowers projectHash]

            audience =
                if includeAuthor
                    then [audComponent, audProject, audAuthor]
                    else [audComponent, audProject]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [AP.acceptObject accept]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXDelegator
                    , AP.grantContext   = encodeRouteHome $ ProjectR projectHash
                    , AP.grantTarget    = uComponent
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

checkExistingComponents
    :: ProjectId -> Either (ComponentBy Entity) RemoteActorId -> ActDBE ()
checkExistingComponents projectID componentDB = do

    -- Find existing Component records I have for this component
    componentIDs <- lift $ getExistingComponents componentDB

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    byEnabled <-
        lift $ for componentIDs $ \ (componentID, _) ->
            isJust <$> runMaybeT (tryComponentEnabled componentID)
    case length $ filter id byEnabled of
        0 -> return ()
        1 -> throwE "I already have a ComponentEnable for this component"
        _ -> error "Multiple ComponentEnable for a component"

    -- Verify none of the Component records are already in
    -- Add-waiting-for-project or Invite-waiting-for-component state
    anyStarted <-
        lift $ runMaybeT $ asum $
            map (\ (componentID, identID) ->
                    tryComponentAddAccept componentID identID <|>
                    tryComponentInviteAccept componentID
                )
                componentIDs
    unless (isNothing anyStarted) $
        throwE
            "One of the Component records is already in Add-Accept or \
            \Invite-Accept state"

    where

    getExistingComponents (Left (ComponentRepo (Entity repoID _))) =
        fmap (map $ bimap E.unValue (Left . E.unValue)) $
        E.select $ E.from $ \ (ident `E.InnerJoin` local `E.InnerJoin` comp) -> do
            E.on $ local E.^. ComponentLocalComponent E.==. comp E.^. ComponentId
            E.on $ ident E.^. ComponentLocalRepoComponent E.==. local E.^. ComponentLocalId
            E.where_ $
                ident E.^. ComponentLocalRepoRepo E.==. E.val repoID E.&&.
                comp E.^. ComponentProject E.==. E.val projectID
            return (comp E.^. ComponentId, local E.^. ComponentLocalId)
    getExistingComponents (Left (ComponentDeck (Entity deckID _))) =
        fmap (map $ bimap E.unValue (Left . E.unValue)) $
        E.select $ E.from $ \ (ident `E.InnerJoin` local `E.InnerJoin` comp) -> do
            E.on $ local E.^. ComponentLocalComponent E.==. comp E.^. ComponentId
            E.on $ ident E.^. ComponentLocalDeckComponent E.==. local E.^. ComponentLocalId
            E.where_ $
                ident E.^. ComponentLocalDeckDeck E.==. E.val deckID E.&&.
                comp E.^. ComponentProject E.==. E.val projectID
            return (comp E.^. ComponentId, local E.^. ComponentLocalId)
    getExistingComponents (Left (ComponentLoom (Entity loomID _))) =
        fmap (map $ bimap E.unValue (Left . E.unValue)) $
        E.select $ E.from $ \ (ident `E.InnerJoin` local `E.InnerJoin` comp) -> do
            E.on $ local E.^. ComponentLocalComponent E.==. comp E.^. ComponentId
            E.on $ ident E.^. ComponentLocalLoomComponent E.==. local E.^. ComponentLocalId
            E.where_ $
                ident E.^. ComponentLocalLoomLoom E.==. E.val loomID E.&&.
                comp E.^. ComponentProject E.==. E.val projectID
            return (comp E.^. ComponentId, local E.^. ComponentLocalId)
    getExistingComponents (Right remoteActorID) =
        fmap (map $ bimap E.unValue (Right . E.unValue)) $
        E.select $ E.from $ \ (ident `E.InnerJoin` comp) -> do
            E.on $ ident E.^. ComponentRemoteComponent E.==. comp E.^. ComponentId
            E.where_ $
                ident E.^. ComponentRemoteActor E.==. E.val remoteActorID E.&&.
                comp E.^. ComponentProject E.==. E.val projectID
            return (comp E.^. ComponentId, ident E.^. ComponentRemoteId)

    tryComponentEnabled componentID =
        const () <$> MaybeT (getBy $ UniqueComponentEnable componentID)

    tryComponentAddAccept componentID identID = do
        _ <- MaybeT $ getBy $ UniqueComponentOriginAdd componentID
        case identID of
            Left localID ->
                const () <$>
                    MaybeT (getBy $ UniqueComponentAcceptLocal localID)
            Right remoteID ->
                const () <$>
                    MaybeT (getBy $ UniqueComponentAcceptRemote remoteID)

    tryComponentInviteAccept componentID = do
        originID <- MaybeT $ getKeyBy $ UniqueComponentOriginInvite componentID
        const () <$> MaybeT (getBy $ UniqueComponentProjectAccept originID)

-- Meaning: An actor is adding some object to some target
-- Behavior:
--     * Verify my components list is the target
--     * Verify the object is a component, find in DB/HTTP
--     * Verify it's not already an active component of mine
--     * Verify it's not already in a Add-Accept process waiting for project
--       collab to accept too
--     * Verify it's not already in an Invite-Accept process waiting for
--       component (or its collaborator) to accept too
--     * Insert the Add to my inbox
--     * Create a Component record in DB
--     * Forward the Add to my followers
projectAdd
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Add URIMode
    -> ActE (Text, Act (), Next)
projectAdd now projectID (Verse authorIdMsig body) add = do

    -- Check input
    component <- do
        let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
        (component, projectComps, role) <- parseAdd author add
        unless (projectComps == Left projectID) $
            throwE "Add target isn't my components collection"
        unless (role == AP.RoleAdmin) $
            throwE "Add role isn't admin"
        return component

    -- If component is local, find it in our DB
    -- If component is remote, HTTP GET it, verify it's an actor of a component
    -- type, and store in our DB (if it's already there, no need for HTTP)
    --
    -- NOTE: This is a blocking HTTP GET done right here in the handler,
    -- which is NOT a good idea. Ideally, it would be done async, and the
    -- handler result would be sent later in a separate (e.g. Accept) activity.
    -- But for the PoC level, the current situation will hopefully do.
    componentDB <-
        bitraverse
            (withDBExcept . flip getComponentE "Component not found in DB")
            (\ u@(ObjURI h lu) -> do
                instanceID <-
                    lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                result <-
                    ExceptT $ first (T.pack . displayException) <$>
                        fetchRemoteActor' instanceID h lu
                case result of
                    Left Nothing -> throwE "Target @id mismatch"
                    Left (Just err) -> throwE $ T.pack $ displayException err
                    Right Nothing -> throwE "Target isn't an actor"
                    Right (Just actor) -> do
                        case remoteActorType $ entityVal actor of
                            AP.ActorTypeRepo -> pure ()
                            AP.ActorTypeTicketTracker -> pure ()
                            AP.ActorTypePatchTracker -> pure ()
                            _ -> throwE "Remote component type isn't repo/tt/pt"
                        return $ entityKey actor
            )
            component

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (project, actorRecip) <- lift $ do
            p <- getJust projectID
            (p,) <$> getJust (projectActor p)

        -- Find existing Component records I have for this component
        -- Make sure none are enabled / in Add-Accept mode / in Invite-Accept
        -- mode
        checkExistingComponents projectID componentDB

        -- Insert the Add to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
        lift $ for mractid $ \ addDB -> do

            -- Create a Component record in DB
            insertComponent componentDB addDB

            return $ projectActor project

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just actorID -> do
            projectHash <- encodeKeyHashid projectID
            let sieve =
                    makeRecipientSet
                        []
                        [LocalStageProjectFollowers projectHash]
            forwardActivity
                authorIdMsig body (LocalActorProject projectID) actorID sieve
            done
                "Recorded a Component record; Inserted the Add to inbox; \
                \Forwarded to followers if addressed"

    where

    insertComponent componentDB addDB = do
        componentID <- insert $ Component projectID AP.RoleAdmin
        originID <- insert $ ComponentOriginAdd componentID
        case addDB of
            Left (_, _, addID) ->
                insert_ $ ComponentGestureLocal originID addID
            Right (author, _, addID) ->
                insert_ $ ComponentGestureRemote originID (remoteAuthorId author) addID
        case componentDB of
            Left l -> do
                identID <- insert $ ComponentLocal componentID
                case l of
                    ComponentRepo (Entity repoID _) ->
                        insert_ $ ComponentLocalRepo identID repoID
                    ComponentDeck (Entity deckID _) ->
                        insert_ $ ComponentLocalDeck identID deckID
                    ComponentLoom (Entity loomID _) ->
                        insert_ $ ComponentLocalLoom identID loomID
            Right remoteActorID ->
                insert_ $ ComponentRemote componentID remoteActorID

-- Meaning: Someone has created a project with my ID URI
-- Behavior:
--     * Verify I'm in a just-been-created state
--     * Verify my creator and the Create sender are the same actor
--     * Create an admin Collab record in DB
--     * Send an admin Grant to the creator
--     * Get out of the just-been-created state
projectCreateMe
    :: UTCTime
    -> ProjectId
    -> Verse
    -> ActE (Text, Act (), Next)
projectCreateMe =
    topicCreateMe
        projectActor LocalActorProject
        CollabTopicProjectProject CollabTopicProject

projectCreate
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Create URIMode
    -> ActE (Text, Act (), Next)
projectCreate now projectID verse (AP.Create obj _muTarget) =
    case obj of

        AP.CreateProject _ mlocal -> do
            (h, local) <- fromMaybeE mlocal "No project id provided"
            let luProject = AP.actorId local
            uMe <- do
                projectHash <- encodeKeyHashid projectID
                encodeRouteHome <- getEncodeRouteHome
                return $ encodeRouteHome $ ProjectR projectHash
            unless (uMe == ObjURI h luProject) $
                throwE "The created project id isn't me"
            projectCreateMe now projectID verse

        _ -> throwE "Unsupported Create object for Project"

-- Meaning: An actor is following someone/something
-- Behavior:
--      * Verify the target is me
--      * Record the follow in DB
--      * Publish and send an Accept to the sender and its followers
projectFollow
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Follow URIMode
    -> ActE (Text, Act (), Next)
projectFollow now recipProjectID verse follow = do
    recipProjectHash <- encodeKeyHashid recipProjectID
    actorFollow
        (\case
            ProjectR d | d == recipProjectHash -> pure ()
            _ -> throwE "Asking to follow someone else"
        )
        projectActor
        False
        (\ recipProjectActor () -> pure $ actorFollowers recipProjectActor)
        (\ _ -> pure $ makeRecipientSet [] [])
        LocalActorProject
        (\ _ -> pure [])
        now recipProjectID verse follow

-- Meaning: An actor is granting access-to-some-resource to another actor
-- Behavior:
--      * Option 1 - Component sending me a delegation-start - Verify that:
--          * The sender is a component of mine, C
--          * The Grant's context is C
--          * The Grant's target is me
--          * The Grant's usage is gatherAndConvey
--          * The Grant doesn't specify 'delegates'
--          * The activity is authorized via a valid delegator-Grant I had sent
--            to C
--      * Verify the Grant's role is the same one specified in the Invite/Add
--        that added the Component
--      * Verify I don't yet have a delegation from C
--      * Insert the Grant to my inbox
--      * Record the delegation in the Component record in DB
--      * Forward the Grant to my followers
--      * For each person (non-team) collaborator of mine, prepare and send an
--        extension-Grant, and store it in the Componet record in DB:
--          * Role: The lower among (1) admin (2) the collaborator's role in me
--          * Resource: C
--          * Target: The collaborator
--          * Delegates: The Grant I just got from C
--          * Result: ProjectCollabLiveR for this collaborator
--          * Usage: invoke
--
--      * Option 2 - Collaborator sending me a delegator-Grant - Verify that:
--          * The sender is a collaborator of mine, A
--          * The Grant's context is A
--          * The Grant's target is me
--          * The Grant's usage is invoke & role is delegate
--          * The Grant doesn't specify 'delegates'
--          * The activity is authorized via a valid direct-Grant I had sent
--            to A
--      * Verify I don't yet have a delegator-Grant from A
--      * Insert the Grant to my inbox
--      * Record the delegator-Grant in the Collab record in DB
--      * Forward the Grant to my followers
--      * For each component of mine C, prepare and send an
--        extension-Grant to A, and store it in the Componet record in DB:
--          * Role: The lower among (1) admin (2) the collaborator's role in me
--          * Resource: C
--          * Target: A
--          * Delegates: The start-Grant I have from C
--          * Result: ProjectCollabLiveR for this collaborator, A
--          * Usage: invoke
--
--      * If neither 1 nor 2, raise an error
projectGrant
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Grant URIMode
    -> ActE (Text, Act (), Next)
projectGrant now projectID (Verse authorIdMsig body) grant = do

    -- Check capability
    capability <- do

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the capability URI is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        cap <- nameExceptT "Invite capability" $ parseActivityURI' uCap

        -- Verify the capability is local
        case cap of
            Left (actorByKey, _, outboxItemID) ->
                return (actorByKey, outboxItemID)
            _ -> throwE "Capability is remote i.e. definitely not by me"

    -- Check grant
    grant' <-
        Left <$> checkDelegationStart grant <|>
        Right <$> checkDelegator grant

    case grant' of
        Left (role, component) -> handleComp capability role component
        Right collab -> handleCollab capability collab

    where

    checkDelegationStart g = do
        (role, resource, recipient, _mresult, mstart, mend, usage, mdeleg) <-
            parseGrant' g
        role' <-
            case role of
                AP.RXRole r -> pure r
                AP.RXDelegator -> throwE "Role is delegator"
        component <-
            fromMaybeE
                (bitraverse actorToComponent Just resource)
                "Resource is a local project, therefore not a component of mine"
        case (component, authorIdMsig) of
            (Left c, Left (a, _, _)) | componentActor c == a -> pure ()
            (Right u, Right (ra, _, _)) | remoteAuthorURI ra == u -> pure ()
            _ -> throwE "Author and context aren't the same actor"
        case recipient of
            Left (LocalActorProject j) | j == projectID -> pure ()
            _ -> throwE "Target isn't me"
        for_ mstart $ \ start ->
            unless (start < now) $ throwE "Start time is in the future"
        for_ mend $ \ _ ->
            throwE "End time is specified"
        unless (usage == AP.GatherAndConvey) $
            throwE "Usage isn't GatherAndConvey"
        for_ mdeleg $ \ _ ->
            throwE "'delegates' is specified"
        return (role', component)

    checkDelegator g = do
        (role, resource, recipient, _mresult, mstart, mend, usage, mdeleg) <-
            parseGrant' g
        case role of
            AP.RXRole _ -> throwE "Role isn't delegator"
            AP.RXDelegator -> pure ()
        collab <-
            bitraverse
                (\case
                    LocalActorPerson p -> pure p
                    _ -> throwE "Local resource isn't a Person, therefore not a collaborator of mine"
                )
                pure
                resource
        case (collab, authorIdMsig) of
            (Left c, Left (a, _, _)) | LocalActorPerson c == a -> pure ()
            (Right u, Right (ra, _, _)) | remoteAuthorURI ra == u -> pure ()
            _ -> throwE "Author and context aren't the same actor"
        case recipient of
            Left (LocalActorProject j) | j == projectID -> pure ()
            _ -> throwE "Target isn't me"
        for_ mstart $ \ start ->
            unless (start < now) $ throwE "Start time is in the future"
        for_ mend $ \ _ ->
            throwE "End time is specified"
        unless (usage == AP.Invoke) $
            throwE "Usage isn't Invoke"
        for_ mdeleg $ \ _ ->
            throwE "'delegates' is specified"
        return collab

    handleComp capability role component = do

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust projectID
                let actorID = projectActor recip
                (actorID,) <$> getJust actorID

            -- Find the Component record from the capability
            Entity enableID (ComponentEnable componentID _) <- do
                unless (fst capability == LocalActorProject projectID) $
                    throwE "Capability isn't mine"
                m <- lift $ getBy $ UniqueComponentEnableGrant $ snd capability
                fromMaybeE m "I don't have a Component with this capability"
            Component j role' <- lift $ getJust componentID
            unless (j == projectID) $
                throwE "Found a Component for this delegator-Grant but it's not mine"
            unless (role' == role) $
                throwE "Grant role isn't the same as in the Invite/Add"
            ident <- lift $ getComponentIdent componentID
            identForCheck <-
                lift $
                bitraverse
                    (pure . snd)
                    (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                    ident
            unless (identForCheck == component) $
                throwE "Capability's component and Grant author aren't the same actor"

            -- Verify I don't yet have a delegation from the component
            maybeDeleg <-
                lift $ case bimap fst fst ident of
                    Left localID -> (() <$) <$> getBy (UniqueComponentDelegateLocal localID)
                    Right remoteID -> (() <$) <$> getBy (UniqueComponentDelegateRemote remoteID)
            verifyNothingE maybeDeleg "I already have a delegation-start Grant from this component"

            maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeGrantDB $ \ grantDB -> do

                -- Record the delegation in DB
                lift $ case (grantDB, bimap fst fst ident) of
                    (Left (_, _, grantID), Left localID) -> insert_ $ ComponentDelegateLocal localID grantID
                    (Right (_, _, grantID), Right remoteID) -> insert_ $ ComponentDelegateRemote remoteID grantID
                    _ -> error "projectGrant impossible"

                -- Prepare forwarding of Accept to my followers
                projectHash <- encodeKeyHashid projectID
                let sieve = makeRecipientSet [] [LocalStageProjectFollowers projectHash]

                -- For each Collab in me, prepare a delegation-extension Grant
                localCollabs <-
                    lift $
                    E.select $ E.from $ \ (topic `E.InnerJoin` collab `E.InnerJoin` enable `E.InnerJoin` recipL `E.InnerJoin` deleg) -> do
                        E.on $ enable E.^. CollabEnableId E.==. deleg E.^. CollabDelegLocalEnable
                        E.on $ enable E.^. CollabEnableCollab E.==. recipL E.^. CollabRecipLocalCollab
                        E.on $ topic E.^. CollabTopicProjectCollab E.==. enable E.^. CollabEnableCollab
                        E.on $ topic E.^. CollabTopicProjectCollab E.==. collab E.^. CollabId
                        E.where_ $ topic E.^. CollabTopicProjectProject E.==. E.val projectID
                        return
                            ( collab E.^. CollabRole
                            , recipL E.^. CollabRecipLocalPerson
                            , deleg
                            )
                localExtensions <- lift $ for localCollabs $ \ (E.Value role', E.Value personID, Entity delegID (CollabDelegLocal enableID' recipID grantID)) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    insert_ $ ComponentFurtherLocal enableID delegID extID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrant identForCheck (Left (personID, grantID)) (min role role') enableID'
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                remoteCollabs <-
                    lift $
                    E.select $ E.from $ \ (topic `E.InnerJoin` collab `E.InnerJoin` enable `E.InnerJoin` recipR `E.InnerJoin` deleg) -> do
                        E.on $ enable E.^. CollabEnableId E.==. deleg E.^. CollabDelegRemoteEnable
                        E.on $ enable E.^. CollabEnableCollab E.==. recipR E.^. CollabRecipRemoteCollab
                        E.on $ topic E.^. CollabTopicProjectCollab E.==. enable E.^. CollabEnableCollab
                        E.on $ topic E.^. CollabTopicProjectCollab E.==. collab E.^. CollabId
                        E.where_ $ topic E.^. CollabTopicProjectProject E.==. E.val projectID
                        return
                            ( collab E.^. CollabRole
                            , recipR E.^. CollabRecipRemoteActor
                            , deleg
                            )
                remoteExtensions <- lift $ for remoteCollabs $ \ (E.Value role', E.Value raID, Entity delegID (CollabDelegRemote enableID' recipID grantID)) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    insert_ $ ComponentFurtherRemote enableID delegID extID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrant identForCheck (Right (raID, grantID)) (min role role') enableID'
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return (recipActorID, sieve, localExtensions, remoteExtensions)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, localExts, remoteExts) -> do
                let recipByID = LocalActorProject projectID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ for_ (localExts ++ remoteExts) $
                    \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            recipByID recipActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                done "Forwarded the start-Grant and published delegation extensions"

        where

        prepareExtensionGrant component collab role enableID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            uStart <- lift $ getActivityURI authorIdMsig

            (uCollab, audCollab, uDeleg) <-
                case collab of
                    Left (personID, itemID) -> do
                        personHash <- encodeKeyHashid personID
                        itemHash <- encodeKeyHashid itemID
                        return
                            ( encodeRouteHome $ PersonR personHash
                            , AudLocal [LocalActorPerson personHash] []
                            , encodeRouteHome $
                                PersonOutboxItemR personHash itemHash
                            )
                    Right (raID, ractID) -> do
                        ra <- getJust raID
                        u@(ObjURI h lu) <- getRemoteActorURI ra
                        uAct <- do
                            ract <- getJust ractID
                            getRemoteActivityURI ract
                        return (u, AudRemote h [lu] [], uAct)

            uComponent <-
                case component of
                    Left c -> do
                        a <- componentActor <$> hashComponent c
                        return $ encodeRouteHome $ renderLocalActor a
                    Right u -> pure u

            enableHash <- encodeKeyHashid enableID

            let audience = [audCollab]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience audience

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = uComponent
                        , AP.grantTarget    = uCollab
                        , AP.grantResult    =
                            Just
                                (encodeRouteLocal $
                                    ProjectCollabLiveR projectHash enableHash
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Invoke
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    handleCollab capability collab = do

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust projectID
                let actorID = projectActor recip
                (actorID,) <$> getJust actorID

            -- Find the Collab record from the capability
            Entity enableID (CollabEnable collabID _) <- do
                unless (fst capability == LocalActorProject projectID) $
                    throwE "Capability isn't mine"
                m <- lift $ getBy $ UniqueCollabEnableGrant $ snd capability
                fromMaybeE m "I don't have a Collab with this capability"
            Collab role <- lift $ getJust collabID
            topic <- lift $ getCollabTopic collabID
            unless (topic == LocalActorProject projectID) $
                throwE "Found a Collab for this direct-Grant but it's not mine"
            recip <- lift $ getCollabRecip collabID
            recipForCheck <-
                lift $
                bitraverse
                    (pure . collabRecipLocalPerson . entityVal)
                    (getRemoteActorURI <=< getJust . collabRecipRemoteActor . entityVal)
                    recip
            unless (recipForCheck == collab) $
                throwE "Capability's collaborator and Grant author aren't the same actor"

            -- Verify I don't yet have a delegator-Grant from the collaborator
            maybeDeleg <-
                lift $ case bimap entityKey entityKey recip of
                    Left localID -> (() <$) <$> getBy (UniqueCollabDelegLocalRecip localID)
                    Right remoteID -> (() <$) <$> getBy (UniqueCollabDelegRemoteRecip remoteID)
            verifyNothingE maybeDeleg "I already have a delegator-Grant from this collaborator"

            maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeGrantDB $ \ grantDB -> do

                -- Record the delegator-Grant in the Collab record
                (insertExt, uDeleg) <-
                    lift $ case (grantDB, bimap entityKey entityKey recip) of
                        (Left (grantActor, _, grantID), Left localID) -> do
                            delegID <- insert $ CollabDelegLocal enableID localID grantID
                            encodeRouteHome <- getEncodeRouteHome
                            delegR <-
                                activityRoute
                                    <$> hashLocalActor grantActor
                                    <*> encodeKeyHashid grantID
                            return
                                (\ enableID furtherID ->
                                    insert_ $ ComponentFurtherLocal enableID delegID furtherID
                                , encodeRouteHome delegR
                                )
                        (Right (_, _, grantID), Right remoteID) -> do
                            delegID <- insert $ CollabDelegRemote enableID remoteID grantID
                            u <- getRemoteActivityURI =<< getJust grantID
                            return
                                (\ enableID furtherID ->
                                    insert_ $ ComponentFurtherRemote enableID delegID furtherID
                                , u
                                )
                        _ -> error "projectGrant impossible 2"

                -- Prepare forwarding of Accept to my followers
                projectHash <- encodeKeyHashid projectID
                let sieve = makeRecipientSet [] [LocalStageProjectFollowers projectHash]

                -- For each Component of mine, prepare a delegation-extension
                -- Grant
                extensions <- lift $ do
                    locals <-
                        fmap (map $ over _1 Left) $
                        E.select $ E.from $ \ (deleg `E.InnerJoin` local `E.InnerJoin` comp `E.InnerJoin` enable) -> do
                            E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                            E.on $ local E.^. ComponentLocalComponent E.==. comp E.^. ComponentId
                            E.on $ deleg E.^. ComponentDelegateLocalComponent E.==.local E.^. ComponentLocalId
                            E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
                            return (deleg E.^. ComponentDelegateLocalGrant, comp, enable)
                    remotes <-
                        fmap (map $ over _1 Right) $
                        E.select $ E.from $ \ (deleg `E.InnerJoin` remote `E.InnerJoin` comp `E.InnerJoin` enable) -> do
                            E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                            E.on $ remote E.^. ComponentRemoteComponent E.==. comp E.^. ComponentId
                            E.on $ deleg E.^. ComponentDelegateRemoteComponent E.==.remote E.^. ComponentRemoteId
                            E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
                            return (deleg E.^. ComponentDelegateRemoteGrant, comp, enable)
                    (uCollab, audCollab) <-
                        case recip of
                            Left (Entity _ (CollabRecipLocal _ personID)) -> do
                                personHash <- encodeKeyHashid personID
                                encodeRouteHome <- getEncodeRouteHome
                                return
                                    ( encodeRouteHome $ PersonR personHash
                                    , AudLocal [LocalActorPerson personHash] []
                                    )
                            Right (Entity _ (CollabRecipRemote _ raID)) -> do
                                ra <- getJust raID
                                u@(ObjURI h lu) <- getRemoteActorURI ra
                                return (u, AudRemote h [lu] [])
                    for (locals ++ remotes) $ \ (start, Entity componentID component, Entity enableID' _) -> do
                        extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                        insertExt enableID' extID
                        componentIdent <- do
                            i <- getComponentIdent componentID
                            bitraverse
                                (pure . snd)
                                (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                                i
                        uStart <-
                            case start of
                                Left (E.Value startID) -> do
                                    encodeRouteHome <- getEncodeRouteHome
                                    c <-
                                        case componentIdent of
                                            Left ci -> hashComponent ci
                                            Right _ -> error "Delegation-start Grant URI is local, but component found to be remote, impossible"
                                    s <- encodeKeyHashid startID
                                    return $ encodeRouteHome $ activityRoute (componentActor c) s
                                Right (E.Value remoteActivityID) -> do
                                    ra <- getJust remoteActivityID
                                    getRemoteActivityURI ra
                        ext@(actionExt, _, _, _) <-
                            prepareExtensionGrant uCollab audCollab uDeleg componentIdent uStart (min role (componentRole component)) enableID
                        let recipByKey = LocalActorProject projectID
                        _luExt <- updateOutboxItem' recipByKey extID actionExt
                        return (extID, ext)

                return (recipActorID, sieve, extensions)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, extensions) -> do
                let recipByID = LocalActorProject projectID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ for_ extensions $
                    \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            recipByID recipActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                done "Forwarded the delegator-Grant, updated DB and published delegation extensions"

        where

        prepareExtensionGrant uCollab audCollab uDeleg component uStart role enableID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID

            uComponent <-
                case component of
                    Left c -> do
                        a <- componentActor <$> hashComponent c
                        return $ encodeRouteHome $ renderLocalActor a
                    Right u -> pure u

            enableHash <- encodeKeyHashid enableID

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audCollab]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = uComponent
                        , AP.grantTarget    = uCollab
                        , AP.grantResult    =
                            Just
                                (encodeRouteLocal $
                                    ProjectCollabLiveR projectHash enableHash
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Invoke
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor A invited actor B to a resource
-- Behavior:
--      * Verify the resource is my collabs or components list
--      * If resource is collabs and B is local, verify it's a Person
--      * If resource is components and B is local, verify it's a Component
--      * Verify A isn't inviting themselves
--      * Verify A is authorized by me to invite collabs/components to me
--
--      * In collab mode,
--          * Verify B doesn't already have an invite/join/grant for me
--      * In component mode,
--          * Verify B isn't already an active component of mine
--          * Verify B isn't already in a Add-Accept process waiting for
--            project collab to accept too
--          * Verify B isn't already in an Invite-Accept process waiting for
--            component (or its collaborator) to accept too
--
--      * Insert the Invite to my inbox
--
--      * In collab mode, Insert a Collab record to DB
--      * In component mode, Create a Component record in DB
--
--      * Forward the Invite to my followers
--      * Send Accept to A, B (and followers if it's a component), my-followers
projectInvite
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Invite URIMode
    -> ActE (Text, Act (), Next)
projectInvite now projectID (Verse authorIdMsig body) invite = do

    -- Check capability
    capability <- do

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the capability URI is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        cap <- nameExceptT "Invite capability" $ parseActivityURI' uCap

        -- Verify the capability is local
        case cap of
            Left (actorByKey, _, outboxItemID) ->
                return (actorByKey, outboxItemID)
            _ -> throwE "Capability is remote i.e. definitely not by me"

    -- Check invite
    (role, invited) <- do
        let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
        (role, resourceOrComps, recipientOrComp) <- parseInvite author invite
        mode <-
            case resourceOrComps of
                Left (Left (LocalActorProject j)) | j == projectID ->
                    Left <$>
                        bitraverse
                            (\case
                                Left r -> pure r
                                Right _ -> throwE "Not accepting local component actors as collabs"
                            )
                            pure
                            recipientOrComp
                Left (Right j) | j == projectID ->
                    Right <$>
                        bitraverse
                            (\case
                                Left _ -> throwE "Not accepting local Persons as components"
                                Right r -> pure r
                            )
                            pure
                            recipientOrComp
                _ -> throwE "Invite topic isn't my collabs or components URI"
        return (role, mode)

    -- If target is local, find it in our DB
    -- If target is remote, HTTP GET it, verify it's an actor, and store in
    -- our DB (if it's already there, no need for HTTP)
    --
    -- NOTE: This is a blocking HTTP GET done right here in the Invite handler,
    -- which is NOT a good idea. Ideally, it would be done async, and the
    -- handler result (approve/disapprove the Invite) would be sent later in a
    -- separate (e.g. Accept) activity. But for the PoC level, the current
    -- situation will hopefully do.
    invitedDB <-
        bitraverse
            (bitraverse
                (withDBExcept . flip getGrantRecip "Invitee not found in DB")
                getRemoteActorFromURI
            )
            (bitraverse
                (withDBExcept . flip getComponentE "Invitee not found in DB")
                getRemoteActorFromURI
            )
        invited

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (topicActorID, topicActor) <- lift $ do
            recip <- getJust projectID
            let actorID = projectActor recip
            (actorID,) <$> getJust actorID

        -- Verify the specified capability gives relevant access
        verifyCapability'
            capability authorIdMsig (LocalActorProject projectID) AP.RoleAdmin

        case invitedDB of

            -- Verify that target doesn't already have a Collab for me
            Left collab -> do
                existingCollabIDs <- lift $ getExistingCollabs collab
                case existingCollabIDs of
                    [] -> pure ()
                    [_] -> throwE "I already have a Collab for the target"
                    _ -> error "Multiple collabs found for target"

            -- Find existing Component records I have for this component
            -- Make sure none are enabled / in Add-Accept mode / in
            -- Invite-Accept mode
            Right component -> checkExistingComponents projectID component

        maybeInviteDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
        lift $ for maybeInviteDB $ \ inviteDB -> do

            -- Insert Collab or Component record to DB
            acceptID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
            case invitedDB of
                Left collab -> insertCollab role collab inviteDB acceptID
                Right component -> insertComponent component inviteDB acceptID

            -- Prepare forwarding Invite to my followers
            sieve <- do
                projectHash <- encodeKeyHashid projectID
                return $ makeRecipientSet [] [LocalStageProjectFollowers projectHash]

            -- Prepare an Accept activity and insert to my outbox
            accept@(actionAccept, _, _, _) <- prepareAccept invitedDB
            _luAccept <- updateOutboxItem' (LocalActorProject projectID) acceptID actionAccept

            return (topicActorID, sieve, acceptID, accept)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (projectActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept)) -> do
            forwardActivity
                authorIdMsig body (LocalActorProject projectID) projectActorID sieve
            lift $ sendActivity
                (LocalActorProject projectID) projectActorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            done "Recorded and forwarded the Invite, sent an Accept"

    where

    getRemoteActorFromURI (ObjURI h lu) = do
        instanceID <-
            lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
        result <-
            ExceptT $ first (T.pack . displayException) <$>
                fetchRemoteActor' instanceID h lu
        case result of
            Left Nothing -> throwE "Target @id mismatch"
            Left (Just err) -> throwE $ T.pack $ displayException err
            Right Nothing -> throwE "Target isn't an actor"
            Right (Just actor) -> return $ entityKey actor

    getExistingCollabs (Left (GrantRecipPerson (Entity personID _))) =
        E.select $ E.from $ \ (topic `E.InnerJoin` recipl) -> do
            E.on $
                topic E.^. CollabTopicProjectCollab E.==.
                recipl E.^. CollabRecipLocalCollab
            E.where_ $
                topic E.^. CollabTopicProjectProject E.==. E.val projectID E.&&.
                recipl E.^. CollabRecipLocalPerson E.==. E.val personID
            return $ recipl E.^. CollabRecipLocalCollab
    getExistingCollabs (Right remoteActorID) =
        E.select $ E.from $ \ (topic `E.InnerJoin` recipr) -> do
            E.on $
                topic E.^. CollabTopicProjectCollab E.==.
                recipr E.^. CollabRecipRemoteCollab
            E.where_ $
                topic E.^. CollabTopicProjectProject E.==. E.val projectID E.&&.
                recipr E.^. CollabRecipRemoteActor E.==. E.val remoteActorID
            return $ recipr E.^. CollabRecipRemoteCollab

    insertCollab role recipient inviteDB acceptID = do
        collabID <- insert $ Collab role
        fulfillsID <- insert $ CollabFulfillsInvite collabID acceptID
        insert_ $ CollabTopicProject collabID projectID
        case inviteDB of
            Left (_, _, inviteID) ->
                insert_ $ CollabInviterLocal fulfillsID inviteID
            Right (author, _, inviteID) -> do
                let authorID = remoteAuthorId author
                insert_ $ CollabInviterRemote fulfillsID authorID inviteID
        case recipient of
            Left (GrantRecipPerson (Entity personID _)) ->
                insert_ $ CollabRecipLocal collabID personID
            Right remoteActorID ->
                insert_ $ CollabRecipRemote collabID remoteActorID

    insertComponent componentDB inviteDB acceptID = do
        componentID <- insert $ Component projectID AP.RoleAdmin
        originID <- insert $ ComponentOriginInvite componentID
        case inviteDB of
            Left (_, _, inviteID) ->
                insert_ $ ComponentProjectGestureLocal componentID inviteID
            Right (author, _, inviteID) ->
                insert_ $ ComponentProjectGestureRemote componentID (remoteAuthorId author) inviteID
        case componentDB of
            Left l -> do
                identID <- insert $ ComponentLocal componentID
                case l of
                    ComponentRepo (Entity repoID _) ->
                        insert_ $ ComponentLocalRepo identID repoID
                    ComponentDeck (Entity deckID _) ->
                        insert_ $ ComponentLocalDeck identID deckID
                    ComponentLoom (Entity loomID _) ->
                        insert_ $ ComponentLocalLoom identID loomID
            Right remoteActorID ->
                insert_ $ ComponentRemote componentID remoteActorID
        insert_ $ ComponentProjectAccept originID acceptID

    prepareAccept invitedDB = do
        encodeRouteHome <- getEncodeRouteHome

        audInviter <- lift $ makeAudSenderOnly authorIdMsig
        audInvited <-
            case invitedDB of
                Left (Left (GrantRecipPerson (Entity p _))) -> do
                    ph <- encodeKeyHashid p
                    return $ AudLocal [LocalActorPerson ph] []
                Left (Right remoteActorID) -> do
                    ra <- getJust remoteActorID
                    ObjURI h lu <- getRemoteActorURI ra
                    return $ AudRemote h [lu] []
                Right (Left componentByEnt) -> do
                    componentByHash <- hashComponent $ bmap entityKey componentByEnt
                    let actor = componentActor componentByHash
                    return $ AudLocal [actor] [localActorFollowers actor]
                Right (Right remoteActorID) -> do
                    ra <- getJust remoteActorID
                    ObjURI h lu <- getRemoteActorURI ra
                    return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
        audTopic <-
            AudLocal [] . pure . LocalStageProjectFollowers <$>
                encodeKeyHashid projectID
        uInvite <- lift $ getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audInviter, audInvited, audTopic]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uInvite]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uInvite
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor A asked to join a resource
-- Behavior:
--      * Verify the resource is me
--      * Verify A doesn't already have an invite/join/grant for me
--      * Remember the join in DB
--      * Forward the Join to my followers
projectJoin
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Join URIMode
    -> ActE (Text, Act (), Next)
projectJoin =
    topicJoin
        projectActor LocalActorProject
        CollabTopicProjectProject CollabTopicProjectCollab CollabTopicProject

-- Meaning: An actor rejected something
-- Behavior:
--     * If it's on an Invite where I'm the resource:
--         * Verify the Reject is by the Invite target
--         * Remove the relevant Collab record from DB
--         * Forward the Reject to my followers
--         * Send a Reject on the Invite:
--             * To: Rejecter (i.e. Invite target)
--             * CC: Invite sender, Rejecter's followers, my followers
--     * If it's on a Join where I'm the resource:
--         * Verify the Reject is authorized
--         * Remove the relevant Collab record from DB
--         * Forward the Reject to my followers
--         * Send a Reject:
--             * To: Join sender
--             * CC: Reject sender, Join sender's followers, my followers
--     * Otherwise respond with error
projectReject
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Reject URIMode
    -> ActE (Text, Act (), Next)
projectReject = topicReject projectActor LocalActorProject

-- Meaning: An actor A is removing actor B from a resource
-- Behavior:
--      * Verify the resource is me
--      * Verify A isn't removing themselves
--      * Verify A is authorized by me to remove actors from me
--      * Verify B already has a Grant for me
--      * Remove the whole Collab record from DB
--      * Forward the Remove to my followers
--      * Send a Revoke:
--          * To: Actor B
--          * CC: Actor A, B's followers, my followers
projectRemove
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Remove URIMode
    -> ActE (Text, Act (), Next)
projectRemove =
    topicRemove
        projectActor LocalActorProject
        CollabTopicProjectProject CollabTopicProjectCollab

-- Meaning: An actor is undoing some previous action
-- Behavior:
--      * If they're undoing their Following of me:
--          * Record it in my DB
--          * Publish and send an Accept only to the sender
--      * Otherwise respond with an error
projectUndo
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Undo URIMode
    -> ActE (Text, Act (), Next)
projectUndo now recipProjectID (Verse authorIdMsig body) (AP.Undo uObject) = do

    -- Check input
    undone <-
        first (\ (actor, _, item) -> (actor, item)) <$>
            parseActivityURI' uObject

    -- Verify the capability URI, if provided, is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCapability <-
        for (AP.activityCapability $ actbActivity body) $ \ uCap ->
            nameExceptT "Undo capability" $
                first (\ (actor, _, item) -> (actor, item)) <$>
                    parseActivityURI' uCap

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (projectRecip, actorRecip) <- lift $ do
            p <- getJust recipProjectID
            (p,) <$> getJust (projectActor p)

        -- Insert the Undo to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
        for mractid $ \ _undoDB -> do

            maybeUndo <- runMaybeT $ do

                -- Find the undone activity in our DB
                undoneDB <- MaybeT $ getActivity undone

                let followers = actorFollowers actorRecip
                asum
                    [ tryUnfollow followers undoneDB authorIdMsig
                    ]

            (sieve, audience) <-
                fromMaybeE
                    maybeUndo
                    "Undone activity isn't a Follow related to me"

            -- Prepare an Accept activity and insert to project's outbox
            acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorRecip) now
            accept@(actionAccept, _, _, _) <- lift $ lift $ prepareAccept audience
            _luAccept <- lift $ updateOutboxItem' (LocalActorProject recipProjectID) acceptID actionAccept

            return (projectActor projectRecip, sieve, acceptID, accept)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (actorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept)) -> do
            forwardActivity
                authorIdMsig body (LocalActorProject recipProjectID) actorID sieve
            lift $ sendActivity
                (LocalActorProject recipProjectID) actorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            done
                "Undid the Follow, forwarded the Undo and published Accept"

    where

    tryUnfollow projectFollowersID (Left (_actorByKey, _actorE, outboxItemID)) (Left (_, actorID, _)) = do
        Entity followID follow <-
            MaybeT $ lift $ getBy $ UniqueFollowFollow outboxItemID
        let followerID = followActor follow
            followerSetID = followTarget follow
        verifyTargetMe followerSetID
        unless (followerID == actorID) $
            lift $ throwE "You're trying to Undo someone else's Follow"
        lift $ lift $ delete followID
        audSenderOnly <- lift $ lift $ lift $ makeAudSenderOnly authorIdMsig
        return (makeRecipientSet [] [], [audSenderOnly])
        where
        verifyTargetMe followerSetID = guard $ followerSetID == projectFollowersID
    tryUnfollow projectFollowersID (Right remoteActivityID) (Right (author, _, _)) = do
        Entity remoteFollowID remoteFollow <-
            MaybeT $ lift $ getBy $ UniqueRemoteFollowFollow remoteActivityID
        let followerID = remoteFollowActor remoteFollow
            followerSetID = remoteFollowTarget remoteFollow
        verifyTargetMe followerSetID
        unless (followerID == remoteAuthorId author) $
            lift $ throwE "You're trying to Undo someone else's Follow"
        lift $ lift $ delete remoteFollowID
        audSenderOnly <- lift $ lift $ lift $ makeAudSenderOnly authorIdMsig
        return (makeRecipientSet [] [], [audSenderOnly])
        where
        verifyTargetMe followerSetID = guard $ followerSetID == projectFollowersID
    tryUnfollow _ _ _ = mzero

    prepareAccept audience = do
        encodeRouteHome <- getEncodeRouteHome

        uUndo <- getActivityURI authorIdMsig
        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = []
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uUndo
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

projectBehavior :: UTCTime -> ProjectId -> VerseExt -> ActE (Text, Act (), Next)
projectBehavior now projectID (Left verse@(Verse _authorIdMsig body)) =
    case AP.activitySpecific $ actbActivity body of
        AP.AcceptActivity accept -> projectAccept now projectID verse accept
        AP.AddActivity add       -> projectAdd now projectID verse add
        AP.CreateActivity create -> projectCreate now projectID verse create
        AP.FollowActivity follow -> projectFollow now projectID verse follow
        AP.GrantActivity grant   -> projectGrant now projectID verse grant
        AP.InviteActivity invite -> projectInvite now projectID verse invite
        AP.JoinActivity join     -> projectJoin now projectID verse join
        AP.RejectActivity reject -> projectReject now projectID verse reject
        AP.RemoveActivity remove -> projectRemove now projectID verse remove
        AP.UndoActivity undo     -> projectUndo now projectID verse undo
        _ -> throwE "Unsupported activity type for Project"
projectBehavior _ _ (Right _) = throwE "ClientMsgs aren't supported for Project"

instance VervisActor Project where
    actorBehavior = projectBehavior
