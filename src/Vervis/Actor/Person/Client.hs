{- This file is part of Vervis.
 -
 - Written in 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Actor.Person.Client
    ( clientBehavior
    )
where

import Control.Applicative
import Control.Exception.Base
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Barbie
import Data.Bifoldable
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Foldable
import Data.List.NonEmpty (NonEmpty)
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Optics.Core
import Yesod.Persist.Core

import qualified Data.Text as T

import Control.Concurrent.Actor
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Database.Persist.Local

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Actor
import Vervis.Actor2
import Vervis.Actor.Deck
import Vervis.Actor.Group
import Vervis.Actor.Project
import Vervis.Cloth
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.Data.Follow
import Vervis.Data.Ticket
import Vervis.FedURI
import Vervis.Fetch
import Vervis.Foundation
import Vervis.Model
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.Persist.Follow
import Vervis.Recipient (makeRecipientSet, LocalStageBy (..), Aud (..), collectAudience, localRecipSieve, localActorFollowers)
import Vervis.RemoteActorStore
import Vervis.Ticket

verifyActorAddressed :: RecipientRoutes -> LocalActorBy Key -> ActE ()
verifyActorAddressed localRecips resource = do
    resourceHash <- hashLocalActor resource
    unless (actorIsAddressed localRecips resourceHash) $
        throwE "Local resource not addressed"

verifyProjectAddressed localRecips projectID = do
    projectHash <- encodeKeyHashid projectID
    fromMaybeE (verify projectHash) "Project not addressed"
    where
    verify j = do
        routes <- lookup j $ recipProjects localRecips
        guard $ routeProject routes

verifyRecipientAddressed localRecips recipient = do
    recipientHash <- hashGrantRecip recipient
    fromMaybeE (verify recipientHash) "Recipient not addressed"
    where
    verify (GrantRecipPerson p) = do
        routes <- lookup p $ recipPeople localRecips
        guard $ routePerson routes

verifyComponentAddressed :: RecipientRoutes -> ComponentBy Key -> ActE ()
verifyComponentAddressed localRecips component = do
    componentHash <- hashComponent component
    fromMaybeE (verify componentHash) "Local component not addressed"
    where
    verify (ComponentRepo r) = do
        routes <- lookup r $ recipRepos localRecips
        guard $ routeRepo routes
    verify (ComponentDeck d) = do
        routes <- lookup d $ recipDecks localRecips
        guard $ routeDeck $ familyDeck routes
    verify (ComponentLoom l) = do
        routes <- lookup l $ recipLooms localRecips
        guard $ routeLoom $ familyLoom routes

verifyRemoteAddressed :: [(Host, NonEmpty LocalURI)] -> FedURI -> ActE ()
verifyRemoteAddressed remoteRecips u =
    fromMaybeE (verify u) "Given remote entity not addressed"
    where
    verify (ObjURI h lu) = do
        lus <- lookup h remoteRecips
        guard $ lu `elem` lus

-- Meaning: The human is approving or accepting something
-- Behavior:
--      * Insert to my inbox
--      * Deliver without filtering
--      * If it's an Invite (that I know about) where I'm invited to a project/team/component:
--          * If I haven't yet seen the topic's approval:
--              * Respond with error, we want to wait for the approval
--          * If I saw topic's approval, but not its direct-Grant:
--              * If I already accepted, raise error
--              * Otherwise, record the approval in the Permit record in DB
--          * If I already saw both, respond with error, as Permit is already enabled
clientAccept
    :: UTCTime
    -> PersonId
    -> ClientMsg
    -> AP.Accept URIMode
    -> ActE OutboxItemId
clientAccept now personMeID (ClientMsg maybeCap localRecips remoteRecips fwdHosts action) accept = do

    -- Check input
    acceptee <- parseAccept accept

    (actorMeID, localRecipsFinal, acceptID) <- withDBExcept $ do

        -- Grab me from DB
        (personMe, actorMe) <- lift $ do
            p <- getJust personMeID
            (p,) <$> getJust (personActor p)

        -- Find the accepted activity in our DB
        accepteeDB <- do
            a <- getActivity acceptee
            fromMaybeE a "Can't find acceptee in DB"

        -- Insert the Accept activity to my outbox
        acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorMe) now
        _luAccept <- lift $ updateOutboxItem' (LocalActorPerson personMeID) acceptID action

        -- See if the accepted activity is an Invite to a resource, grabbing
        -- the Permit record from our DB
        maybePermit <- lift $ runMaybeT $ tryInvite accepteeDB

        for_ maybePermit $ \ (permitID, _fulfillsID) -> do

            -- Find the local person and verify it's me
            Permit p _role <- lift $ getJust permitID
            when (p == personMeID) $ do

                -- Find the topic
                topic <-
                    lift $
                    requireEitherAlt
                        (getKeyBy $ UniquePermitTopicLocal permitID)
                        (getKeyBy $ UniquePermitTopicRemote permitID)
                        "Permit without topic"
                        "Permit with both local and remote topic"

                -- If I haven't seen topic's Accept, raise error
                maybeTopicAccept <-
                    lift $ case topic of
                        Left localID -> void <$> getBy (UniquePermitTopicAcceptLocalTopic localID)
                        Right remoteID -> void <$> getBy (UniquePermitTopicAcceptRemoteTopic remoteID)
                when (isNothing maybeTopicAccept) $
                    throwE "Haven't seen topic's Accept yet, please wait for it"

                -- If I haven't seen the direct-Grant, and haven't already
                -- accepted, record my accept
                -- If I've already accepted or seen the direct-Grant, raise an error
                maybeTopicEnable <-
                    lift $ case topic of
                        Left localID -> void <$> getBy (UniquePermitTopicEnableLocalTopic localID)
                        Right remoteID -> void <$> getBy (UniquePermitTopicEnableRemoteTopic remoteID)
                if isNothing maybeTopicEnable
                    then do
                        maybeInserted <- lift $ insertUnique $ PermitPersonGesture permitID acceptID
                        when (isNothing maybeInserted) $
                            throwE "I already Accepted this Invite"
                    else throwE "I already have a direct-Grant for this Invite"

        return
            ( personActor personMe
            , localRecips
            , acceptID
            )

    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFinal remoteRecips
        fwdHosts acceptID action
    return acceptID

    where

    tryInvite (Left (actorByKey, _actorEntity, itemID)) = do
        PermitTopicGestureLocal fulfillsID _ <-
            MaybeT $ getValBy $ UniquePermitTopicGestureLocalInvite itemID
        PermitFulfillsInvite permitID <- lift $ getJust fulfillsID
        return (permitID, fulfillsID)
    tryInvite (Right remoteActivityID) = do
        PermitTopicGestureRemote fulfillsID _ _ <-
            MaybeT $ getValBy $ UniquePermitTopicGestureRemoteInvite remoteActivityID
        PermitFulfillsInvite permitID <- lift $ getJust fulfillsID
        return (permitID, fulfillsID)

-- Meaning: The human wants to add component C to project P
-- Behavior:
--      * Some basic sanity checks
--          * Parse the Add
--          * Make sure not inviting myself
--          * Verify that a capability is specified
--          * If component is local, verify it exists in DB
--          * If project is local, verify it exists in DB
--      * Verify C and P are addressed in the Invite
--      * Insert Add to my inbox
--      * Asynchrnously deliver to:
--          * C+followers
--          * P+followers
--          * My followers
clientAdd
    :: UTCTime
    -> PersonId
    -> ClientMsg
    -> AP.Add URIMode
    -> ActE OutboxItemId
clientAdd now personMeID (ClientMsg maybeCap localRecips remoteRecips fwdHosts action) add = do

    -- Check input
    (component, project, _role) <- parseAdd (Left $ LocalActorPerson personMeID) add
    _capID <- fromMaybeE maybeCap "No capability provided"

    -- If project components URI is remote, HTTP GET it and its resource and its
    -- managing actor, and insert to our DB. If project is local, find it in
    -- our DB.
    projectDB <-
        bitraverse
            (withDBExcept . flip getEntityE "Project not found in DB")
            (\ u@(ObjURI h luComps) -> do
                manager <- asksEnv envHttpManager
                coll <- ExceptT $ liftIO $ first T.pack <$> AP.fetchAPID manager AP.collectionId h luComps
                lu <- fromMaybeE (AP.collectionContext (coll :: AP.Collection FedURI URIMode)) "Remote topic collabs has no 'context'"
                AP.ResourceWithCollections _ _ mluComps _ <- ExceptT $ liftIO $ first (T.pack . show) <$> AP.fetchRWC manager h lu
                unless (mluComps == Just luComps) $
                    throwE "Add target isn't a components list"

                instanceID <-
                    lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                result <-
                    ExceptT $ first (T.pack . show) <$>
                        fetchRemoteResource instanceID h lu
                case result of
                    Left (Entity actorID actor) ->
                        return (remoteActorIdent actor, actorID, u)
                    Right (objectID, luManager, (Entity actorID _)) ->
                        return (objectID, actorID, ObjURI h luManager)
            )
            project

    -- If component is remote, HTTP GET it, make sure it's an actor, and insert
    -- it to our DB. If recipient is local, find it in our DB.
    componentDB <-
        bitraverse
            (withDBExcept . flip getComponentE "Component not found in DB")
            (\ u@(ObjURI h lu) -> do
                instanceID <-
                    lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                result <-
                    ExceptT $ first (T.pack . displayException) <$>
                        fetchRemoteActor' instanceID h lu
                case result of
                    Left Nothing -> throwE "Recipient @id mismatch"
                    Left (Just err) -> throwE $ T.pack $ displayException err
                    Right Nothing -> throwE "Recipient isn't an actor"
                    Right (Just actor) -> return (entityKey actor, u)
            )
            component

    -- Verify that project and component are addressed by the Add
    bitraverse_
        (verifyProjectAddressed localRecips . entityKey)
        (\ (_, _, u) -> verifyRemoteAddressed remoteRecips u)
        projectDB
    bitraverse_
        (verifyComponentAddressed localRecips . bmap entityKey)
        (verifyRemoteAddressed remoteRecips . snd)
        componentDB

    (actorMeID, localRecipsFinal, addID) <- withDBExcept $ do

        -- Grab me from DB
        (personMe, actorMe) <- lift $ do
            p <- getJust personMeID
            (p,) <$> getJust (personActor p)

        -- Insert the Add activity to my outbox
        addID <- lift $ insertEmptyOutboxItem' (actorOutbox actorMe) now
        _luAdd <- lift $ updateOutboxItem' (LocalActorPerson personMeID) addID action

        -- Prepare local recipients for Add delivery
        sieve <- lift $ do
            projectHash <- bitraverse encodeKeyHashid pure project
            componentHash <- bitraverse hashComponent pure component
            senderHash <- encodeKeyHashid personMeID
            let sieveActors = catMaybes
                    [ case projectHash of
                        Left j -> Just $ LocalActorProject j
                        Right _ -> Nothing
                    , case componentHash of
                        Left c -> Just $ componentActor c
                        Right _ -> Nothing
                    ]
                sieveStages = catMaybes
                    [ Just $ LocalStagePersonFollowers senderHash
                    , case projectHash of
                        Left j -> Just $ LocalStageProjectFollowers j
                        Right _ -> Nothing
                    , case componentHash of
                        Left c -> Just $ localActorFollowers $ componentActor c
                        Right _ -> Nothing
                    ]
            return $ makeRecipientSet sieveActors sieveStages
        return
            ( personActor personMe
            , localRecipSieve sieve False localRecips
            , addID
            )

    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFinal remoteRecips
        fwdHosts addID action
    return addID

-- Meaning: The human wants to create a ticket tracker
-- Behavior:
--      * Create a deck on DB
--      * Create a Permit record in DB
--      * Launch a deck actor
--      * Record a FollowRequest in DB
--      * Create and send Create and Follow to it
clientCreateDeck
    :: UTCTime
    -> PersonId
    -> ClientMsg
    -> AP.ActorDetail
    -> ActE OutboxItemId
clientCreateDeck now personMeID (ClientMsg maybeCap localRecips remoteRecips fwdHosts action) tracker = do

    -- Check input
    verifyNothingE maybeCap "Capability not needed"
    (name, msummary) <- parseTracker tracker

    (actorMeID, localRecipsFinal, createID, actionCreate, followID, follow, deckID) <- withDBExcept $ do

        -- Grab me from DB
        (personMe, actorMe) <- lift $ do
            p <- getJust personMeID
            (p,) <$> getJust (personActor p)
        let actorMeID = personActor personMe

        -- Insert new deck to DB
        createID <- lift $ insertEmptyOutboxItem' (actorOutbox actorMe) now
        wid <- findWorkflow
        (deckID, deckFollowerSetID) <-
            lift $ insertDeck now name msummary createID wid actorMeID

        -- Insert a Permit record
        lift $ do
            permitID <- insert $ Permit personMeID AP.RoleAdmin
            topicID <- insert $ PermitTopicLocal permitID
            insert_ $ PermitTopicDeck topicID deckID
            insert_ $ PermitFulfillsTopicCreation permitID
            insert_ $ PermitPersonGesture permitID createID

        -- Insert the Create activity to my outbox
        deckHash <- encodeKeyHashid deckID
        actionCreate <- prepareCreate name msummary deckHash
        luCreate <- lift $ updateOutboxItem' (LocalActorPerson personMeID) createID actionCreate

        -- Prepare recipient sieve for sending the Create
        personMeHash <- lift $ encodeKeyHashid personMeID
        let sieve =
                makeRecipientSet
                    [LocalActorDeck deckHash]
                    [LocalStagePersonFollowers personMeHash]
            onlyDeck = DeckFamilyRoutes (DeckRoutes True False) []
            addMe' decks = (deckHash, onlyDeck) : decks
            addMe rs = rs { recipDecks = addMe' $ recipDecks rs }

        -- Insert a follow request, since I'm about to send a Follow
        followID <- lift $ insertEmptyOutboxItem' (actorOutbox actorMe) now
        lift $ insert_ $ FollowRequest actorMeID deckFollowerSetID True followID

        -- Insert a Follow to my outbox
        follow@(actionFollow, _, _, _) <- lift $ lift $ prepareFollow deckID luCreate
        _luFollow <- lift $ updateOutboxItem' (LocalActorPerson personMeID) followID actionFollow

        return
            ( personActor personMe
            , localRecipSieve sieve False $ addMe localRecips
            , createID
            , actionCreate
            , followID
            , follow
            , deckID
            )

    -- Spawn new Deck actor
    success <- lift $ launchActor LocalActorDeck deckID
    unless success $
        error "Failed to spawn new Deck, somehow ID already in Theater"

    -- Send the Create
    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFinal remoteRecips
        fwdHosts createID actionCreate

    -- Send the Follow
    let (actionFollow, localRecipsFollow, remoteRecipsFollow, fwdHostsFollow) = follow
    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFollow
        remoteRecipsFollow fwdHostsFollow followID actionFollow

    return createID

    where

    parseTracker (AP.ActorDetail typ muser mname msummary) = do
        unless (typ == AP.ActorTypeTicketTracker) $
            error "createTicketTrackerC: Create object isn't a TicketTracker"
        verifyNothingE muser "TicketTracker can't have a username"
        name <- fromMaybeE mname "TicketTracker doesn't specify name"
        return (name, msummary)

    findWorkflow = do
        mw <- lift $ selectFirst ([] :: [Filter Workflow]) []
        entityKey <$> fromMaybeE mw "Can't find a workflow"

    insertDeck now name msummary obiidCreate wid actorMeID = do
        ibid <- insert Inbox
        obid <- insert Outbox
        fsid <- insert FollowerSet
        aid <- insert Actor
            { actorName      = name
            , actorDesc      = fromMaybe "" msummary
            , actorCreatedAt = now
            , actorInbox     = ibid
            , actorOutbox    = obid
            , actorFollowers = fsid
            , actorJustCreatedBy = Just actorMeID
            }
        did <- insert Deck
            { deckActor      = aid
            , deckWorkflow   = wid
            , deckNextTicket = 1
            , deckWiki       = Nothing
            , deckCreate     = obiidCreate
            }
        return (did, fsid)

    prepareCreate name msummary deckHash = do
        encodeRouteLocal <- getEncodeRouteLocal
        hLocal <- asksEnv stageInstanceHost
        let ttdetail = AP.ActorDetail
                { AP.actorType     = AP.ActorTypeTicketTracker
                , AP.actorUsername = Nothing
                , AP.actorName     = Just name
                , AP.actorSummary  = msummary
                }
            ttlocal = AP.ActorLocal
                { AP.actorId         = encodeRouteLocal $ DeckR deckHash
                , AP.actorInbox      = encodeRouteLocal $ DeckInboxR deckHash
                , AP.actorOutbox     = Nothing
                , AP.actorFollowers  = Nothing
                , AP.actorFollowing  = Nothing
                , AP.actorPublicKeys = []
                , AP.actorSshKeys    = []
                }
            specific = AP.CreateActivity AP.Create
                { AP.createObject = AP.CreateTicketTracker ttdetail (Just (hLocal, ttlocal))
                , AP.createTarget = Nothing
                }
        return action { AP.actionSpecific = specific }

    prepareFollow deckID luCreate = do
        encodeRouteHome <- getEncodeRouteHome
        h <- asksEnv stageInstanceHost
        deckHash <- encodeKeyHashid deckID

        let audTopic = AudLocal [LocalActorDeck deckHash] []
            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audTopic]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [ObjURI h luCreate]
                , AP.actionSpecific   = AP.FollowActivity AP.Follow
                    { AP.followObject  = encodeRouteHome $ DeckR deckHash
                    , AP.followContext = Nothing
                    , AP.followHide    = False
                    }
                }
        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: The human wants to create a project
-- Behavior:
--      * Create a project on DB
--      * Create a Permit record in DB
--      * Launch a project actor
--      * Record a FollowRequest in DB
--      * Create and send Create and Follow to it
clientCreateProject
    :: UTCTime
    -> PersonId
    -> ClientMsg
    -> AP.ActorDetail
    -> ActE OutboxItemId
clientCreateProject now personMeID (ClientMsg maybeCap localRecips remoteRecips fwdHosts action) tracker = do

    -- Check input
    verifyNothingE maybeCap "Capability not needed"
    (name, msummary) <- parseTracker tracker

    (actorMeID, localRecipsFinal, createID, actionCreate, followID, follow, projectID) <- lift $ withDB $ do

        -- Grab me from DB
        (personMe, actorMe) <- do
            p <- getJust personMeID
            (p,) <$> getJust (personActor p)
        let actorMeID = personActor personMe

        -- Insert new project to DB
        createID <- insertEmptyOutboxItem' (actorOutbox actorMe) now
        (projectID, projectFollowerSetID) <-
            insertProject now name msummary createID actorMeID

        -- Insert a Permit record
        permitID <- insert $ Permit personMeID AP.RoleAdmin
        topicID <- insert $ PermitTopicLocal permitID
        insert_ $ PermitTopicProject topicID projectID
        insert_ $ PermitFulfillsTopicCreation permitID
        insert_ $ PermitPersonGesture permitID createID

        -- Insert the Create activity to my outbox
        projectHash <- lift $ encodeKeyHashid projectID
        actionCreate <- lift $ prepareCreate name msummary projectHash
        luCreate <- updateOutboxItem' (LocalActorPerson personMeID) createID actionCreate

        -- Prepare recipient sieve for sending the Create
        personMeHash <- lift $ encodeKeyHashid personMeID
        let sieve =
                makeRecipientSet
                    [LocalActorProject projectHash]
                    [LocalStagePersonFollowers personMeHash]
            onlyProject = ProjectRoutes True False
            addMe' projects = (projectHash, onlyProject) : projects
            addMe rs = rs { recipProjects = addMe' $ recipProjects rs }

        -- Insert a follow request, since I'm about to send a Follow
        followID <- insertEmptyOutboxItem' (actorOutbox actorMe) now
        insert_ $ FollowRequest actorMeID projectFollowerSetID True followID

        -- Insert a Follow to my outbox
        follow@(actionFollow, _, _, _) <- lift $ prepareFollow projectID luCreate
        _luFollow <- updateOutboxItem' (LocalActorPerson personMeID) followID actionFollow

        return
            ( personActor personMe
            , localRecipSieve sieve False $ addMe localRecips
            , createID
            , actionCreate
            , followID
            , follow
            , projectID
            )

    -- Spawn new Project actor
    success <- lift $ launchActor LocalActorProject projectID
    unless success $
        error "Failed to spawn new Project, somehow ID already in Theater"

    -- Send the Create
    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFinal remoteRecips
        fwdHosts createID actionCreate

    -- Send the Follow
    let (actionFollow, localRecipsFollow, remoteRecipsFollow, fwdHostsFollow) = follow
    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFollow
        remoteRecipsFollow fwdHostsFollow followID actionFollow

    return createID

    where

    parseTracker (AP.ActorDetail typ muser mname msummary) = do
        unless (typ == AP.ActorTypeProject) $
            error "clientCreateProject: Create object isn't a Project"
        verifyNothingE muser "Project can't have a username"
        name <- fromMaybeE mname "Project doesn't specify name"
        return (name, msummary)

    insertProject now name msummary obiidCreate actorMeID = do
        ibid <- insert Inbox
        obid <- insert Outbox
        fsid <- insert FollowerSet
        aid <- insert Actor
            { actorName      = name
            , actorDesc      = fromMaybe "" msummary
            , actorCreatedAt = now
            , actorInbox     = ibid
            , actorOutbox    = obid
            , actorFollowers = fsid
            , actorJustCreatedBy = Just actorMeID
            }
        did <- insert Project
            { projectActor      = aid
            , projectCreate     = obiidCreate
            }
        return (did, fsid)

    prepareCreate name msummary projectHash = do
        encodeRouteLocal <- getEncodeRouteLocal
        hLocal <- asksEnv stageInstanceHost
        let ttdetail = AP.ActorDetail
                { AP.actorType     = AP.ActorTypeProject
                , AP.actorUsername = Nothing
                , AP.actorName     = Just name
                , AP.actorSummary  = msummary
                }
            ttlocal = AP.ActorLocal
                { AP.actorId         = encodeRouteLocal $ ProjectR projectHash
                , AP.actorInbox      = encodeRouteLocal $ ProjectInboxR projectHash
                , AP.actorOutbox     = Nothing
                , AP.actorFollowers  = Nothing
                , AP.actorFollowing  = Nothing
                , AP.actorPublicKeys = []
                , AP.actorSshKeys    = []
                }
            specific = AP.CreateActivity AP.Create
                { AP.createObject = AP.CreateProject ttdetail (Just (hLocal, ttlocal))
                , AP.createTarget = Nothing
                }
        return action { AP.actionSpecific = specific }

    prepareFollow projectID luCreate = do
        encodeRouteHome <- getEncodeRouteHome
        h <- asksEnv stageInstanceHost
        projectHash <- encodeKeyHashid projectID

        let audTopic = AudLocal [LocalActorProject projectHash] []
            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audTopic]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [ObjURI h luCreate]
                , AP.actionSpecific   = AP.FollowActivity AP.Follow
                    { AP.followObject  = encodeRouteHome $ ProjectR projectHash
                    , AP.followContext = Nothing
                    , AP.followHide    = False
                    }
                }
        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: The human wants to create a team
-- Behavior:
--      * Create a team on DB
--      * Create a Permit record in DB
--      * Launch a team actor
--      * Record a FollowRequest in DB
--      * Create and send Create and Follow to it
clientCreateTeam
    :: UTCTime
    -> PersonId
    -> ClientMsg
    -> AP.ActorDetail
    -> ActE OutboxItemId
clientCreateTeam now personMeID (ClientMsg maybeCap localRecips remoteRecips fwdHosts action) tracker = do

    -- Check input
    verifyNothingE maybeCap "Capability not needed"
    (name, msummary) <- parseTracker tracker

    (actorMeID, localRecipsFinal, createID, actionCreate, followID, follow, groupID) <- lift $ withDB $ do

        -- Grab me from DB
        (personMe, actorMe) <- do
            p <- getJust personMeID
            (p,) <$> getJust (personActor p)
        let actorMeID = personActor personMe

        -- Insert new team to DB
        createID <- insertEmptyOutboxItem' (actorOutbox actorMe) now
        (groupID, projectFollowerSetID) <-
            insertTeam now name msummary createID actorMeID

        -- Insert a Permit record
        permitID <- insert $ Permit personMeID AP.RoleAdmin
        topicID <- insert $ PermitTopicLocal permitID
        insert_ $ PermitTopicGroup topicID groupID
        insert_ $ PermitFulfillsTopicCreation permitID
        insert_ $ PermitPersonGesture permitID createID

        -- Insert the Create activity to my outbox
        groupHash <- lift $ encodeKeyHashid groupID
        actionCreate <- lift $ prepareCreate name msummary groupHash
        luCreate <- updateOutboxItem' (LocalActorPerson personMeID) createID actionCreate

        -- Prepare recipient sieve for sending the Create
        personMeHash <- lift $ encodeKeyHashid personMeID
        let sieve =
                makeRecipientSet
                    [LocalActorGroup groupHash]
                    [LocalStagePersonFollowers personMeHash]
            onlyGroup = GroupRoutes True False
            addMe' groups = (groupHash, onlyGroup) : groups
            addMe rs = rs { recipGroups = addMe' $ recipGroups rs }

        -- Insert a follow request, since I'm about to send a Follow
        followID <- insertEmptyOutboxItem' (actorOutbox actorMe) now
        insert_ $ FollowRequest actorMeID projectFollowerSetID True followID

        -- Insert a Follow to my outbox
        follow@(actionFollow, _, _, _) <- lift $ prepareFollow groupID luCreate
        _luFollow <- updateOutboxItem' (LocalActorPerson personMeID) followID actionFollow

        return
            ( personActor personMe
            , localRecipSieve sieve False $ addMe localRecips
            , createID
            , actionCreate
            , followID
            , follow
            , groupID
            )

    -- Spawn new Group actor
    success <- lift $ launchActor LocalActorGroup groupID
    unless success $
        error "Failed to spawn new Group, somehow ID already in Theater"

    -- Send the Create
    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFinal remoteRecips
        fwdHosts createID actionCreate

    -- Send the Follow
    let (actionFollow, localRecipsFollow, remoteRecipsFollow, fwdHostsFollow) = follow
    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFollow
        remoteRecipsFollow fwdHostsFollow followID actionFollow

    return createID

    where

    parseTracker (AP.ActorDetail typ muser mname msummary) = do
        unless (typ == AP.ActorTypeTeam) $
            error "clientCreateTeam: Create object isn't a Team"
        verifyNothingE muser "Team can't have a username"
        name <- fromMaybeE mname "Team doesn't specify name"
        return (name, msummary)

    insertTeam now name msummary obiidCreate actorMeID = do
        ibid <- insert Inbox
        obid <- insert Outbox
        fsid <- insert FollowerSet
        aid <- insert Actor
            { actorName      = name
            , actorDesc      = fromMaybe "" msummary
            , actorCreatedAt = now
            , actorInbox     = ibid
            , actorOutbox    = obid
            , actorFollowers = fsid
            , actorJustCreatedBy = Just actorMeID
            }
        gid <- insert Group
            { groupActor      = aid
            , groupCreate     = obiidCreate
            }
        return (gid, fsid)

    prepareCreate name msummary groupHash = do
        encodeRouteLocal <- getEncodeRouteLocal
        hLocal <- asksEnv stageInstanceHost
        let ttdetail = AP.ActorDetail
                { AP.actorType     = AP.ActorTypeTeam
                , AP.actorUsername = Nothing
                , AP.actorName     = Just name
                , AP.actorSummary  = msummary
                }
            ttlocal = AP.ActorLocal
                { AP.actorId         = encodeRouteLocal $ GroupR groupHash
                , AP.actorInbox      = encodeRouteLocal $ GroupInboxR groupHash
                , AP.actorOutbox     = Nothing
                , AP.actorFollowers  = Nothing
                , AP.actorFollowing  = Nothing
                , AP.actorPublicKeys = []
                , AP.actorSshKeys    = []
                }
            specific = AP.CreateActivity AP.Create
                { AP.createObject = AP.CreateTeam ttdetail (Just (hLocal, ttlocal))
                , AP.createTarget = Nothing
                }
        return action { AP.actionSpecific = specific }

    prepareFollow groupID luCreate = do
        encodeRouteHome <- getEncodeRouteHome
        h <- asksEnv stageInstanceHost
        groupHash <- encodeKeyHashid groupID

        let audTopic = AudLocal [LocalActorGroup groupHash] []
            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audTopic]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [ObjURI h luCreate]
                , AP.actionSpecific   = AP.FollowActivity AP.Follow
                    { AP.followObject  = encodeRouteHome $ GroupR groupHash
                    , AP.followContext = Nothing
                    , AP.followHide    = False
                    }
                }
        return (action, recipientSet, remoteActors, fwdHosts)

clientCreate
    :: UTCTime
    -> PersonId
    -> ClientMsg
    -> AP.Create URIMode
    -> ActE OutboxItemId
clientCreate now personMeID msg (AP.Create object muTarget) =
    case object of

        AP.CreateTicketTracker detail mlocal -> do
            verifyNothingE mlocal "Tracker id must not be provided"
            verifyNothingE muTarget "'target' not supported in Create TicketTracker"
            clientCreateDeck now personMeID msg detail

        AP.CreateProject detail mlocal -> do
            verifyNothingE mlocal "Project id must not be provided"
            verifyNothingE muTarget "'target' not supported in Create Project"
            clientCreateProject now personMeID msg detail

        AP.CreateTeam detail mlocal -> do
            verifyNothingE mlocal "Team id must not be provided"
            verifyNothingE muTarget "'target' not supported in Create Team"
            clientCreateTeam now personMeID msg detail

        _ -> throwE "Unsupported Create object for C2S"

-- Meaning: The human wants to invite someone A to a resource R
-- Behavior:
--      * Some basic sanity checks
--          * Parse the Invite
--          * Make sure not inviting myself
--          * Verify that a capability is specified
--          * If resource is local, verify it exists in DB
--      * Verify the target A and resource R are addressed in the Invite
--      * Insert Invite to my inbox
--      * Asynchrnously deliver to:
--          * Resource+followers
--          * Target+followers
--          * My followers
clientInvite
    :: UTCTime
    -> PersonId
    -> ClientMsg
    -> AP.Invite URIMode
    -> ActE OutboxItemId
clientInvite now personMeID (ClientMsg maybeCap localRecips remoteRecips fwdHosts action) invite = do

    -- Check input
    (_role, resource, recipient) <- parseInvite (Left $ LocalActorPerson personMeID) invite
    _capID <- fromMaybeE maybeCap "No capability provided"

    -- If resource collabs URI is remote, HTTP GET it and its resource and its
    -- managing actor, and insert to our DB. If resource is local, find it in
    -- our DB.
    resourceDB <-
        bitraverse
            (bitraverse
                (withDBExcept . flip getLocalActorEntityE "Grant resource not found in DB")
                (withDBExcept . flip getEntityE "Grant context project not found in DB")
            )
            (\ u@(ObjURI h luColl) -> do
                manager <- asksEnv envHttpManager
                coll <- ExceptT $ liftIO $ first T.pack <$> AP.fetchAPID manager AP.collectionId h luColl
                lu <- fromMaybeE (AP.collectionContext (coll :: AP.Collection FedURI URIMode)) "Remote topic collabs has no 'context'"
                AP.ResourceWithCollections _ mluCollabs mluComps mluMembers <- ExceptT $ liftIO $ first (T.pack . show) <$> AP.fetchRWC manager h lu
                unless (mluCollabs == Just luColl || mluComps == Just luColl || mluMembers == Just luColl) $
                    throwE "Invite target isn't a collabs/components list"

                instanceID <-
                    lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                result <-
                    ExceptT $ first (T.pack . show) <$>
                        fetchRemoteResource instanceID h lu
                case result of
                    Left (Entity actorID actor) ->
                        return (remoteActorIdent actor, actorID, u)
                    Right (objectID, luManager, (Entity actorID _)) ->
                        return (objectID, actorID, ObjURI h luManager)
            )
            resource

    -- If recipient is remote, HTTP GET it, make sure it's an actor, and insert
    -- it to our DB. If recipient is local, find it in our DB.
    recipientDB <-
        bitraverse
            (bitraverse
                (withDBExcept . flip getGrantRecip "Grant recipient person not found in DB")
                (withDBExcept . flip getComponentE "Grant recipient component not found in DB")
            )
            (\ u@(ObjURI h lu) -> do
                instanceID <-
                    lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                result <-
                    ExceptT $ first (T.pack . displayException) <$>
                        fetchRemoteActor' instanceID h lu
                case result of
                    Left Nothing -> throwE "Recipient @id mismatch"
                    Left (Just err) -> throwE $ T.pack $ displayException err
                    Right Nothing -> throwE "Recipient isn't an actor"
                    Right (Just actor) -> return (entityKey actor, u)
            )
            recipient

    -- Verify that resource and recipient are addressed by the Invite
    bitraverse_
        (bitraverse_
            (verifyActorAddressed localRecips . bmap entityKey)
            (verifyProjectAddressed localRecips . entityKey)
        )
        (\ (_, _, u) -> verifyRemoteAddressed remoteRecips u)
        resourceDB
    bitraverse_
        (bitraverse_
            (verifyRecipientAddressed localRecips . bmap entityKey)
            (verifyComponentAddressed localRecips . bmap entityKey)
        )
        (verifyRemoteAddressed remoteRecips . snd)
        recipientDB

    (actorMeID, localRecipsFinal, inviteID) <- withDBExcept $ do

        -- Grab me from DB
        (personMe, actorMe) <- lift $ do
            p <- getJust personMeID
            (p,) <$> getJust (personActor p)

        -- Insert the Invite activity to my outbox
        inviteID <- lift $ insertEmptyOutboxItem' (actorOutbox actorMe) now
        _luInvite <- lift $ updateOutboxItem' (LocalActorPerson personMeID) inviteID action

        -- Prepare local recipients for Invite delivery
        sieve <- lift $ do
            resourceHash <- bitraverse (bitraverse hashLocalActor encodeKeyHashid) pure resource
            recipientHash <- bitraverse (bitraverse hashGrantRecip hashComponent) pure recipient
            senderHash <- encodeKeyHashid personMeID
            let sieveActors = catMaybes
                    [ case resourceHash of
                        Left (Left a)  -> Just a
                        Left (Right j) -> Just $ LocalActorProject j
                        Right _        -> Nothing
                    , case recipientHash of
                        Left (Left (GrantRecipPerson p)) -> Just $ LocalActorPerson p
                        Left (Right c)                   -> Just $ componentActor c
                        Right _                          -> Nothing
                    ]
                sieveStages = catMaybes
                    [ Just $ LocalStagePersonFollowers senderHash
                    , case resourceHash of
                        Left (Left a)  -> Just $ localActorFollowers a
                        Left (Right j) -> Just $ LocalStageProjectFollowers j
                        Right _        -> Nothing
                    , case recipientHash of
                        Left (Left (GrantRecipPerson p)) -> Just $ LocalStagePersonFollowers p
                        Left (Right c)                   -> Just $ localActorFollowers $ componentActor c
                        Right _                          -> Nothing
                    ]
            return $ makeRecipientSet sieveActors sieveStages
        return
            ( personActor personMe
            , localRecipSieve sieve False localRecips
            , inviteID
            )

    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFinal remoteRecips
        fwdHosts inviteID action
    return inviteID

-- Meaning: The human wants to join a resource R
-- Behavior:
--      * Some basic sanity checks
--          * Parse the Join
--          * Make sure not joining myself
--          * Verify that a capability isn't specified
--          * If resource is local, verify it exists in DB
--      * Verify the resource R is addressed in the Join
--      * Insert Join to my outbox
--
--      * If R is referred by a collabs/members collection URI:
--          * For each Permit record I have for this resource:
--              * Verify it's not enabled yet, i.e. I'm not already a
--                collaborator, haven't received a direct-Grant
--              * Verify it's not in Invite-Accept state, already got the
--                resource's Accept and waiting for my approval or for the
--                topic's Grant
--              * Verify it's not a Join
--          * Create a Permit record in DB
--
--      * Asynchrnously deliver to:
--          * Resource+followers
--          * My followers
clientJoin
    :: UTCTime
    -> PersonId
    -> ClientMsg
    -> AP.Join URIMode
    -> ActE OutboxItemId
clientJoin now personMeID (ClientMsg maybeCap localRecips remoteRecips fwdHosts action) join = do

    -- Check input
    (role, resource) <- parseJoin join
    verifyNothingE maybeCap "Capability provided"

    -- If resource collabs URI is remote, HTTP GET it and its resource and its
    -- managing actor, and insert to our DB. If resource is local, find it in
    -- our DB.
    resourceDB <-
        bitraverse
            (withDBExcept . flip getLocalActorEntityE "Join resource not found in DB")
            (\ u@(ObjURI h luColl) -> do
                manager <- asksEnv envHttpManager
                coll <- ExceptT $ liftIO $ first T.pack <$> AP.fetchAPID manager AP.collectionId h luColl
                lu <- fromMaybeE (AP.collectionContext (coll :: AP.Collection FedURI URIMode)) "Remote topic collabs has no 'context'"
                AP.ResourceWithCollections _ mluCollabs mluComps mluMembers <- ExceptT $ liftIO $ first (T.pack . show) <$> AP.fetchRWC manager h lu
                let isCollabs = mluCollabs == Just luColl || mluMembers == Just luColl
                unless (isCollabs || mluComps == Just luColl) $
                    throwE "Join resource isn't a collabs/components list"

                instanceID <-
                    lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                result <-
                    ExceptT $ first (T.pack . show) <$>
                        fetchRemoteResource instanceID h lu
                case result of
                    Left (Entity actorID actor) ->
                        return (remoteActorIdent actor, actorID, u, isCollabs)
                    Right (objectID, luManager, (Entity actorID _)) ->
                        return (objectID, actorID, ObjURI h luManager, isCollabs)
            )
            resource

    -- Verify that resource is addressed by the Join
    bitraverse_
        (verifyActorAddressed localRecips . bmap entityKey)
        (\ (_, _, u, _) -> verifyRemoteAddressed remoteRecips u)
        resourceDB

    let maybePermit =
            case resourceDB of
                Left la -> Just $ Left la
                Right (_, _, _, False) -> Nothing
                Right (objectID, actorID, uActor, True) -> Just $ Right (objectID, actorID, uActor)

    (actorMeID, localRecipsFinal, joinID) <- withDBExcept $ do

        -- Grab me from DB
        (personMe, actorMe) <- lift $ do
            p <- getJust personMeID
            (p,) <$> getJust (personActor p)

        -- Insert the Join activity to my outbox
        joinID <- lift $ insertEmptyOutboxItem' (actorOutbox actorMe) now
        _luJoin <- lift $ updateOutboxItem' (LocalActorPerson personMeID) joinID action

        for_ maybePermit $ \ topicDB -> do

            -- Find existing Permit records I have for this topic
            -- Make sure none are enabled / in Join mode / in Invite-Accept
            -- mode
            checkExistingPermits
                personMeID
                (bimap (bmap entityKey) (view _2) topicDB)

            -- Insert Permit record to DB
            insertPermit topicDB joinID role

        -- Prepare local recipients for Join delivery
        sieve <- lift $ do
            resourceHash <- bitraverse hashLocalActor pure resource
            senderHash <- encodeKeyHashid personMeID
            let sieveActors = catMaybes
                    [ case resourceHash of
                        Left a  -> Just a
                        Right _ -> Nothing
                    ]
                sieveStages = catMaybes
                    [ Just $ LocalStagePersonFollowers senderHash
                    , case resourceHash of
                        Left a  -> Just $ localActorFollowers a
                        Right _ -> Nothing
                    ]
            return $ makeRecipientSet sieveActors sieveStages
        return
            ( personActor personMe
            , localRecipSieve sieve False localRecips
            , joinID
            )

    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFinal remoteRecips
        fwdHosts joinID action
    return joinID

    where

    insertPermit resourceDB joinID role = do
        permitID <- lift $ insert $ Permit personMeID role
        case resourceDB of
            Left la -> do
                localID <- lift $ insert $ PermitTopicLocal permitID
                case bmap entityKey la of
                    LocalActorPerson _ -> throwE "insertPermit: Person not supported as a PermitTopicLocal type (you can't become a \"collaborator in a person\""
                    LocalActorRepo r -> lift $ insert_ $ PermitTopicRepo localID r
                    LocalActorDeck d -> lift $ insert_ $ PermitTopicDeck localID d
                    LocalActorLoom l -> lift $ insert_ $ PermitTopicLoom localID l
                    LocalActorProject j -> lift $ insert_ $ PermitTopicProject localID j
                    LocalActorGroup g -> lift $ insert_ $ PermitTopicGroup localID g
            Right (_, actorID, _) -> lift $ insert_ $ PermitTopicRemote permitID actorID
        lift $ do
            insert_ $ PermitFulfillsJoin permitID
            insert_ $ PermitPersonGesture permitID joinID

-- Meaning: The human wants to open a ticket/MR/dependency
-- Behavior:
--      * Basics checks on the provided ticket/MR (dependency not allowed)
--      * Verify the Offer target is addressed in the Offer
--      * Insert Invite to my inbox
--      * Asynchrnously deliver to:
--          * Target tracker + followers
--          * My followers
clientOffer
    :: UTCTime
    -> PersonId
    -> ClientMsg
    -> AP.Offer URIMode
    -> ActE OutboxItemId
clientOffer now personMeID (ClientMsg maybeCap localRecips remoteRecips fwdHosts action) (AP.Offer object uTarget) = do

    -- Check input
    ticket <-
        case object of
            AP.OfferTicket t -> pure t
            _ -> throwE "Unsupported Offer.object type"
    h <- asksEnv stageInstanceHost
    WorkItemOffer {..} <- checkOfferTicket h ticket uTarget
    unless (wioAuthor == Left personMeID) $
        throwE "Offering a Ticket attributed to someone else"

    -- Verify the tracker is addressed by the Offer
    -- Verify it exists in DB if local
    case wioRest of
        TAM_Task deckID -> do
            _ <- withDBExcept $ getE deckID "No such local deck"
            verifyComponentAddressed localRecips $ ComponentDeck deckID
        TAM_Merge loomID _ -> do
            _ <- withDBExcept $ getE loomID "No such local loom"
            verifyComponentAddressed localRecips $ ComponentLoom loomID
        TAM_Remote u _ -> verifyRemoteAddressed remoteRecips u

    (actorMeID, localRecipsFinal, offerID) <- withDBExcept $ do

        -- Grab me from DB
        (personMe, actorMe) <- lift $ do
            p <- getJust personMeID
            (p,) <$> getJust (personActor p)

        -- Insert the Offer activity to my outbox
        offerID <- lift $ insertEmptyOutboxItem' (actorOutbox actorMe) now
        _luOffer <- lift $ updateOutboxItem' (LocalActorPerson personMeID) offerID action

        -- Prepare local recipients for Invite delivery
        sieve <- lift $ do
            tracker <-
                case wioRest of
                    TAM_Task deckID -> Left . Left <$> encodeKeyHashid deckID
                    TAM_Merge loomID _ -> Left . Right <$> encodeKeyHashid loomID
                    TAM_Remote u _ -> pure $ Right u
            senderHash <- encodeKeyHashid personMeID
            let sieveActors = catMaybes
                    [ case tracker of
                        Left (Left d)  -> Just $ LocalActorDeck d
                        Left (Right l) -> Just $ LocalActorLoom l
                        Right _        -> Nothing
                    ]
                sieveStages = catMaybes
                    [ Just $ LocalStagePersonFollowers senderHash
                    , case tracker of
                        Left (Left d)  -> Just $ LocalStageDeckFollowers d
                        Left (Right l) -> Just $ LocalStageLoomFollowers l
                        Right _        -> Nothing
                    ]
            return $ makeRecipientSet sieveActors sieveStages
        return
            ( personActor personMe
            , localRecipSieve sieve False localRecips
            , offerID
            )

    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFinal remoteRecips
        fwdHosts offerID action
    return offerID

-- Meaning: The human wants to remove someone A from a resource R
-- Behavior:
--      * Some basic sanity checks
--          * Parse the Remove
--          * Make sure not removing myself
--          * Verify that a capability is specified
--          * If resource is local, verify it exists in DB
--      * Verify the target A and resource R are addressed in the Remove
--      * Insert the Remove to my inbox
--      * Asynchrnously deliver to:
--          * Resource+followers
--          * Member+followers
--          * My followers
clientRemove
    :: UTCTime
    -> PersonId
    -> ClientMsg
    -> AP.Remove URIMode
    -> ActE OutboxItemId
clientRemove now personMeID (ClientMsg maybeCap localRecips remoteRecips fwdHosts action) remove = do

    -- Check input
    (resourceOrComps, memberOrComp) <- parseRemove (Left $ LocalActorPerson personMeID) remove
    resource <-
        bitraverse
            (\case
                Left r -> pure r
                Right _ -> throwE "Not accepting project components as target"
            )
            pure
            resourceOrComps
    member <-
        bitraverse
            (\case
                Left r -> pure r
                Right _ -> throwE "Not accepting component actors as collabs"
            )
            pure
            memberOrComp
    _capID <- fromMaybeE maybeCap "No capability provided"

    -- If resource collabs is remote, HTTP GET it to determine resource
    resource' <-
        bitraverse
            pure
            (\ (ObjURI h luColl) -> do
                manager <- asksEnv envHttpManager
                coll <- ExceptT $ liftIO $ first T.pack <$> AP.fetchAPID manager AP.collectionId h luColl
                lu <- fromMaybeE (AP.collectionContext (coll :: AP.Collection FedURI URIMode)) "Remote topic collabs has no 'context'"
                AP.ResourceWithCollections _ mluCollabs _ mluMembers <- ExceptT $ liftIO $ first (T.pack . show) <$> AP.fetchRWC manager h lu
                unless (mluCollabs == Just luColl || mluMembers == Just luColl) $
                    throwE "Remove origin isn't a collabs list"
                return $ ObjURI h lu
            )
            resource

    -- Verify that resource is addressed by the Remove
    bitraverse_
        (verifyActorAddressed localRecips)
        (verifyRemoteAddressed remoteRecips)
        resource'

    -- Verify that member is addressed by the Remove
    bitraverse_
        (verifyRecipientAddressed localRecips)
        (verifyRemoteAddressed remoteRecips)
        member

    (actorMeID, localRecipsFinal, removeID) <- withDBExcept $ do

        -- If resource is local, find it in our DB
        _resourceDB <-
            bitraverse
                (flip getLocalActorEntityE "Resource not found in DB")
                pure
                resource'

        -- If member is local, find it in our DB
        _memberDB <-
            bitraverse
                (flip getGrantRecip "Member not found in DB")
                pure
                member

        -- Grab me from DB
        (personMe, actorMe) <- lift $ do
            p <- getJust personMeID
            (p,) <$> getJust (personActor p)

        -- Insert the Remove activity to my outbox
        removeID <- lift $ insertEmptyOutboxItem' (actorOutbox actorMe) now
        _luRemove <- lift $ updateOutboxItem' (LocalActorPerson personMeID) removeID action

        -- Prepare local recipients for Remove delivery
        sieve <- lift $ do
            resourceHash <- bitraverse hashLocalActor pure resource'
            recipientHash <- bitraverse hashGrantRecip pure member
            senderHash <- encodeKeyHashid personMeID
            let sieveActors = catMaybes
                    [ case resourceHash of
                        Left a -> Just a
                        Right _                    -> Nothing
                    , case recipientHash of
                        Left (GrantRecipPerson p) -> Just $ LocalActorPerson p
                        Right _                   -> Nothing
                    ]
                sieveStages = catMaybes
                    [ Just $ LocalStagePersonFollowers senderHash
                    , case resourceHash of
                        Left a -> Just $ localActorFollowers a
                        Right _                    -> Nothing
                    , case recipientHash of
                        Left (GrantRecipPerson p) -> Just $ LocalStagePersonFollowers p
                        Right _                   -> Nothing
                    ]
            return $ makeRecipientSet sieveActors sieveStages
        return
            ( personActor personMe
            , localRecipSieve sieve False localRecips
            , removeID
            )

    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFinal remoteRecips
        fwdHosts removeID action
    return removeID

-- Meaning: The human wants to close a ticket/MR/dependency
-- Behavior:
--      * Insert Resolve to my inbox
--      * Asynchrnously deliver without filter
clientResolve
    :: UTCTime
    -> PersonId
    -> ClientMsg
    -> AP.Resolve URIMode
    -> ActE OutboxItemId
clientResolve now personMeID (ClientMsg maybeCap localRecips remoteRecips fwdHosts action) (AP.Resolve uObject) = do

    (actorMeID, localRecipsFinal, resolveID) <- withDBExcept $ do

        -- Grab me from DB
        (personMe, actorMe) <- lift $ do
            p <- getJust personMeID
            (p,) <$> getJust (personActor p)

        -- Insert the Resolve activity to my outbox
        acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorMe) now
        _luAccept <- lift $ updateOutboxItem' (LocalActorPerson personMeID) acceptID action

        return
            ( personActor personMe
            , localRecips
            , acceptID
            )

    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFinal remoteRecips
        fwdHosts resolveID action
    return resolveID

-- Meaning: The human wants to unfollow or unresolve
-- Behavior:
--      * Insert the Undo to my inbox
--      * Asynchrnously deliver without filter
clientUndo
    :: UTCTime
    -> PersonId
    -> ClientMsg
    -> AP.Undo URIMode
    -> ActE OutboxItemId
clientUndo now personMeID (ClientMsg maybeCap localRecips remoteRecips fwdHosts action) (AP.Undo uObject) = do

    (actorMeID, localRecipsFinal, undoID) <- withDBExcept $ do

        -- Grab me from DB
        (personMe, actorMe) <- lift $ do
            p <- getJust personMeID
            (p,) <$> getJust (personActor p)

        -- Insert the Undo activity to my outbox
        acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorMe) now
        _luAccept <- lift $ updateOutboxItem' (LocalActorPerson personMeID) acceptID action

        return
            ( personActor personMe
            , localRecips
            , acceptID
            )

    lift $ sendActivity
        (LocalActorPerson personMeID) actorMeID localRecipsFinal remoteRecips
        fwdHosts undoID action
    return undoID

clientBehavior :: UTCTime -> PersonId -> ClientMsg -> ActE (Text, Act (), Next)
clientBehavior now personID msg =
    done . T.pack . show =<<
    case AP.actionSpecific $ cmAction msg of
        AP.AcceptActivity accept -> clientAccept now personID msg accept
        AP.AddActivity add       -> clientAdd now personID msg add
        AP.CreateActivity create -> clientCreate now personID msg create
        AP.InviteActivity invite -> clientInvite now personID msg invite
        AP.JoinActivity join     -> clientJoin now personID msg join
        AP.OfferActivity offer   -> clientOffer now personID msg offer
        AP.RemoveActivity remove -> clientRemove now personID msg remove
        AP.ResolveActivity resolve -> clientResolve now personID msg resolve
        AP.UndoActivity undo     -> clientUndo now personID msg undo
        _ -> throwE "Unsupported activity type for C2S"
