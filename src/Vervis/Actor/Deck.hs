{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2022, 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Actor.Deck
    (
    )
where

import Control.Applicative
import Control.Exception.Base
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Foldable
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Optics.Core
import Yesod.Persist.Core

import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Database.Persist.Local

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Actor
import Vervis.Actor.Common
import Vervis.Actor2
import Vervis.Cloth
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.Data.Ticket
import Vervis.FedURI
import Vervis.Federation.Util
import Vervis.Foundation
import Vervis.Model hiding (deckCreate)
import Vervis.Recipient (makeRecipientSet, LocalStageBy (..), Aud (..), collectAudience, localActorFollowers)
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.RemoteActorStore
import Vervis.Ticket
import Vervis.Web.Collab

-- Meaning: An actor is adding some object to some target
-- Behavior:
--     * Verify that the object is me
--     * Verify the target is some project's components collection URI
--     * Verify the Add is authorized
--     * For all the Stem records I have for this project:
--         * Verify I'm not yet a member of the project
--         * Verify I haven't already Accepted an Add to this project
--         * Verify I haven't already seen an Invite-and-Project-accept for
--           this project
--     * Insert the Add to my inbox
--     * Create a Stem record in DB
--     * Forward the Add activity to my followers
--     * Send an Accept on the Add:
--         * To:
--             * The author of the Add
--             * The project
--         * CC:
--             * Author's followers
--             * Project's followers
--             * My followers
deckAdd
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Add URIMode
    -> ActE (Text, Act (), Next)
deckAdd now deckID (Verse authorIdMsig body) add = do

    -- Check capability
    capability <- do

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the capability URI is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        cap <- nameExceptT "Invite capability" $ parseActivityURI' uCap

        -- Verify the capability is local
        case cap of
            Left (actorByKey, _, outboxItemID) ->
                return (actorByKey, outboxItemID)
            _ -> throwE "Capability is remote i.e. definitely not by me"

    -- Check input
    projectComps <- do
        let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
        (component, projectComps, role) <- parseAdd author add
        unless (component == Left (ComponentDeck deckID)) $
            throwE "Add object isn't me"
        unless (role == AP.RoleAdmin) $
            throwE "Add role isn't admin"
        return projectComps

    -- If project is local, find it in our DB
    -- If project is remote, HTTP GET it and store in our DB (if it's already
    -- there, no need for HTTP)
    --
    -- NOTE: This is a blocking HTTP GET done right here in the handler,
    -- which is NOT a good idea. Ideally, it would be done async, and the
    -- handler result would be sent later in a separate (e.g. Accept) activity.
    -- But for the PoC level, the current situation will hopefully do.
    projectDB <-
        bitraverse
            (withDBExcept . flip getEntityE "Project not found in DB")
            (\ u@(ObjURI h luComps) -> do
                manager <- asksEnv envHttpManager
                collection <-
                    ExceptT $ first T.pack <$>
                        AP.fetchAPID
                            manager
                            (AP.collectionId :: AP.Collection FedURI URIMode -> LocalURI)
                            h
                            luComps
                luProject <- fromMaybeE (AP.collectionContext collection) "Collection has no context"
                project <-
                    ExceptT $ first T.pack <$>
                        AP.fetchAPID manager (AP.actorId . AP.actorLocal . AP.projectActor) h luProject
                unless (AP.projectComponents project == luComps) $
                    throwE "The collection isn't the project's components collection"

                instanceID <-
                    lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                result <-
                    ExceptT $ first (T.pack . displayException) <$>
                        fetchRemoteActor' instanceID h luProject
                case result of
                    Left Nothing -> throwE "Target @id mismatch"
                    Left (Just err) -> throwE $ T.pack $ displayException err
                    Right Nothing -> throwE "Target isn't an actor"
                    Right (Just actor) -> do
                        unless (remoteActorType (entityVal actor) == AP.ActorTypeProject) $
                            throwE "Remote project type isn't Project"
                        return $ entityKey actor
            )
            projectComps

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (deck, actor) <- lift $ do
            d <- getJust deckID
            (d,) <$> getJust (deckActor d)

        -- Find existing Stem records I have for this project
        -- Make sure none are enabled / in Add-Accept mode / in Invite-Accept
        -- mode
        checkExistingStems (ComponentDeck deckID) projectDB

        -- Verify the specified capability gives relevant access
        verifyCapability'
            capability authorIdMsig (LocalActorDeck deckID) AP.RoleAdmin

        -- Insert the Add to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actor) False
        lift $ for mractid $ \ addDB -> do

            -- Create a Stem record in DB
            acceptID <- insertEmptyOutboxItem' (actorOutbox actor) now
            insertStem projectDB addDB acceptID

            -- Prepare forwarding Add to my followers
            sieve <- do
                deckHash <- encodeKeyHashid deckID
                return $ makeRecipientSet [] [LocalStageDeckFollowers deckHash]

            -- Prepare an Accept activity and insert to my outbox
            accept@(actionAccept, _, _, _) <- prepareAccept projectDB
            _luAccept <- updateOutboxItem' (LocalActorDeck deckID) acceptID actionAccept

            return (deckActor deck, sieve, acceptID, accept)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (actorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept)) -> do
            forwardActivity
                authorIdMsig body (LocalActorDeck deckID) actorID sieve
            lift $ sendActivity
                (LocalActorDeck deckID) actorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            done "Recorded and forwarded the Add, sent an Accept"

    where

    insertStem projectDB addDB acceptID = do
        stemID <- insert $ Stem AP.RoleAdmin
        insert_ $ StemIdentDeck stemID deckID
        case projectDB of
            Left (Entity projectID _) ->
                insert_ $ StemProjectLocal stemID projectID
            Right remoteActorID ->
                insert_ $ StemProjectRemote stemID remoteActorID
        insert_ $ StemOriginAdd stemID
        case addDB of
            Left (_, _, addID) ->
                insert_ $ StemComponentGestureLocal stemID addID
            Right (author, _, addID) ->
                insert_ $ StemComponentGestureRemote stemID (remoteAuthorId author) addID
        insert_ $ StemComponentAccept stemID acceptID

    prepareAccept projectDB = do
        encodeRouteHome <- getEncodeRouteHome

        audAdder <- makeAudSenderWithFollowers authorIdMsig
        audProject <-
            case projectDB of
                Left (Entity j _) -> do
                    jh <- encodeKeyHashid j
                    return $
                        AudLocal
                            [LocalActorProject jh]
                            [LocalStageProjectFollowers jh]
                Right remoteActorID -> do
                    ra <- getJust remoteActorID
                    ObjURI h lu <- getRemoteActorURI ra
                    return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
        audComponent <-
            AudLocal [] . pure . LocalStageDeckFollowers <$>
                encodeKeyHashid deckID
        uAdd <- lift $ getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audAdder, audProject, audComponent]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uAdd]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uAdd
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: Someone has created a ticket tracker with my ID URI
-- Behavior:
--     * Verify I'm in a just-been-created state
--     * Verify my creator and the Create sender are the same actor
--     * Create an admin Collab record in DB
--     * Send an admin Grant to the creator
--     * Get out of the just-been-created state
deckCreateMe
    :: UTCTime
    -> DeckId
    -> Verse
    -> ActE (Text, Act (), Next)
deckCreateMe =
    topicCreateMe
        deckActor LocalActorDeck CollabTopicDeckDeck CollabTopicDeck

deckCreate
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Create URIMode
    -> ActE (Text, Act (), Next)
deckCreate now deckID verse (AP.Create obj _muTarget) =
    case obj of

        AP.CreateTicketTracker _ mlocal -> do
            (h, local) <- fromMaybeE mlocal "No tracker id provided"
            let luTracker = AP.actorId local
            uMe <- do
                deckHash <- encodeKeyHashid deckID
                encodeRouteHome <- getEncodeRouteHome
                return $ encodeRouteHome $ DeckR deckHash
            unless (uMe == ObjURI h luTracker) $
                throwE "The created tracker id isn't me"
            deckCreateMe now deckID verse

        _ -> throwE "Unsupported Create object for Deck"

-- Meaning: An actor A is offering a ticket or a ticket dependency
-- Behavior:
--      * Verify I'm the target
--      * Insert the Offer to my inbox
--      * Create the new ticket in my DB
--      * Forward the Offer to my followers
--      * Publish an Accept to:
--          - My followers
--          - Offer sender+followers
deckOffer
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Offer URIMode
    -> ActE (Text, Act (), Next)
deckOffer now deckID (Verse authorIdMsig body) (AP.Offer object uTarget) = do

    -- Check input
    (title, desc, source) <- do
        ticket <-
            case object of
                AP.OfferTicket t -> pure t
                _ -> throwE "Unsupported Offer.object type"
        ObjURI hAuthor _ <- lift $ getActorURI authorIdMsig
        let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
        WorkItemOffer {..} <- checkOfferTicket hAuthor ticket uTarget
        unless (bimap LocalActorPerson id wioAuthor == author) $
            throwE "Offering a Ticket attributed to someone else"
        case wioRest of
            TAM_Task deckID' ->
                if deckID' == deckID
                    then return ()
                    else throwE
                            "Offer target is some other local deck, so I have \
                            \no use for this Offer. Was I supposed to receive \
                            \it?"
            TAM_Merge _ _ ->
                throwE
                    "Offer target is some local loom, so I have no use for \
                    \this Offer. Was I supposed to receive it?"
            TAM_Remote _ _ ->
                throwE
                    "Offer target is some remote tracker, so I have no use \
                    \for this Offer. Was I supposed to receive it?"
        return (wioTitle, wioDesc, wioSource)

    -- Verify the capability URI, if provided, is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCapability <-
        for (AP.activityCapability $ actbActivity body) $ \ uCap ->
            nameExceptT "Offer.capability" $
                first (\ (actor, _, item) -> (actor, item)) <$>
                    parseActivityURI' uCap

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (deckRecip, actorRecip) <- lift $ do
            d <- getJust deckID
            (d,) <$> getJust (deckActor d)

        -- Insert the Offer to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
        for mractid $ \ offerDB -> do

            -- If a capability is provided, check it
            for_ maybeCapability $ \ cap -> do
                lcap <-
                    case cap of
                        Left c -> pure c
                        Right _ -> throwE "Capability is a remote URI, i.e. not authored by me"
                verifyCapability'
                    lcap
                    authorIdMsig
                    (LocalActorDeck deckID)
                    AP.RoleReport

            -- Prepare forwarding the Offer to my followers
            let recipByID = LocalActorDeck deckID
            recipByHash <- hashLocalActor recipByID
            let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

            -- Insert the new ticket to our DB
            acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorRecip) now
            offerDB' <-
                bitraverse
                (traverseOf _1 $ \case
                    LocalActorPerson personID -> pure personID
                    _ -> throwE "Local non-Person ticket authors not allowed"
                )
                pure
                offerDB
            taskID <- lift $ insertTask now title desc source deckID offerDB' acceptID

            -- Prepare an Accept activity and insert to my outbox
            accept@(actionAccept, _, _, _) <- lift $ prepareAccept taskID
            let recipByKey = LocalActorDeck deckID
            _luAccept <- lift $ updateOutboxItem' recipByKey acceptID actionAccept

            return (deckActor deckRecip, sieve, acceptID, accept)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (deckActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept)) -> do
            forwardActivity
                authorIdMsig body (LocalActorDeck deckID) deckActorID sieve
            lift $ sendActivity
                (LocalActorDeck deckID) deckActorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            done "Opened a ticket and forwarded the Offer"

    where

    insertTask now title desc source deckID offerDB acceptID = do
        did <- insert Discussion
        fsid <- insert FollowerSet
        tid <- insert Ticket
            { ticketNumber      = Nothing
            , ticketCreated     = now
            , ticketTitle       = title
            , ticketSource      = source
            , ticketDescription = desc
            , ticketDiscuss     = did
            , ticketFollowers   = fsid
            , ticketAccept      = acceptID
            }
        case offerDB of
            Left (personID, _, offerID) ->
                insert_ TicketAuthorLocal
                    { ticketAuthorLocalTicket = tid
                    , ticketAuthorLocalAuthor = personID
                    , ticketAuthorLocalOpen   = offerID
                    }
            Right (author, _, offerID) ->
                insert_ TicketAuthorRemote
                    { ticketAuthorRemoteTicket = tid
                    , ticketAuthorRemoteAuthor = remoteAuthorId author
                    , ticketAuthorRemoteOpen   = offerID
                    }
        insert $ TicketDeck tid deckID

    prepareAccept taskID = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audSender <- makeAudSenderWithFollowers authorIdMsig
        deckHash <- encodeKeyHashid deckID
        taskHash <- encodeKeyHashid taskID
        let audDeck = AudLocal [] [LocalStageDeckFollowers deckHash]
        uOffer <- lift $ getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audSender, audDeck]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uOffer]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uOffer
                    , AP.acceptResult   =
                        Just $ encodeRouteLocal $ TicketR deckHash taskHash
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor is asking to close a ticket
-- Behavior:
--      * Verify it's my ticket
--      * Verify the Resolve is authorized
--      * Insert the Resolve to my inbox
--      * Close the ticket in my DB
--      * Forward the Resolve to my followers & ticket followers
--      * Publish an Accept to:
--          - My followers
--          - Ticket's followers
--          - Resolve sender+followers
deckResolve
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Resolve URIMode
    -> ActE (Text, Act (), Next)
deckResolve now deckID (Verse authorIdMsig body) (AP.Resolve uObject) = do

    -- Check input
    deckHash <- encodeKeyHashid deckID
    taskHash <- nameExceptT "Resolve.object" $ do
        route <- do
            routeOrRemote <- parseFedURI uObject
            case routeOrRemote of
                Left route -> pure route
                Right _ -> throwE "Remote, so definitely not mine"
        case route of
            TicketR deckHash' taskHash | deckHash' == deckHash ->
                return taskHash
            _ -> throwE "Local route but not a ticket of mine"
    taskID <- decodeKeyHashidE taskHash "Invalid TicketDeck keyhashid"

    -- Verify that a capability is provided
    uCap <- do
        let muCap = AP.activityCapability $ actbActivity body
        fromMaybeE muCap "No capability provided"

    -- Verify the sender is authorized by the tracker to resolve a ticket
    verifyCapability''
        uCap
        authorIdMsig
        (LocalActorDeck deckID)
        AP.RoleTriage

    {-
    -- Check capability
    capability <- do

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the capability URI is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        cap <- nameExceptT "Resolve.capability" $ parseActivityURI' uCap

        -- Verify the capability is local
        case cap of
            Left (actorByKey, _, outboxItemID) ->
                return (actorByKey, outboxItemID)
            _ -> throwE "Capability is remote i.e. definitely not by me"
    -}

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (deckRecip, actorRecip) <- lift $ do
            d <- getJust deckID
            (d,) <$> getJust (deckActor d)

        -- Find ticket in DB, verify it's not resolved
        ticketID <- do
            maybeTicket <- lift $ getTicket deckID taskID
            (_deck, _task, Entity ticketID _, _author, maybeResolve) <-
                fromMaybeE maybeTicket "I don't have such a ticket in DB"
            unless (isNothing maybeResolve) $
                throwE "Ticket is already resolved"
            return ticketID

        -- Insert the Resolve to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
        for mractid $ \ resolveDB -> do

            {-
            -- Verify the sender is authorized by the tracker to resolve a ticket
            verifyCapability'
                capability
                authorIdMsig
                (GrantResourceDeck deckID)
                AP.RoleTriage
            -}

            -- Prepare forwarding the Resolve to my followers & ticket
            -- followers
            let sieve =
                    makeRecipientSet []
                        [ LocalStageDeckFollowers deckHash
                        , LocalStageTicketFollowers deckHash taskHash
                        ]

            -- Mark ticket in DB as resolved by the Resolve
            acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorRecip) now
            lift $ insertResolve ticketID resolveDB acceptID

            -- Prepare an Accept activity and insert to my outbox
            accept@(actionAccept, _, _, _) <- lift $ prepareAccept taskID
            let recipByKey = LocalActorDeck deckID
            _luAccept <- lift $ updateOutboxItem' recipByKey acceptID actionAccept

            return (deckActor deckRecip, sieve, acceptID, accept)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (deckActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept)) -> do
            forwardActivity
                authorIdMsig body (LocalActorDeck deckID) deckActorID sieve
            lift $ sendActivity
                (LocalActorDeck deckID) deckActorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            done "Resolved ticket and forwarded the Resolve"

    where

    insertResolve ticketID resolveDB acceptID = do
        trid <- insert TicketResolve
            { ticketResolveTicket = ticketID
            , ticketResolveAccept = acceptID
            }
        case resolveDB of
            Left (_actorByKey, _, resolveID) ->
                insert_ TicketResolveLocal
                    { ticketResolveLocalTicket   = trid
                    , ticketResolveLocalActivity = resolveID
                    }
            Right (author, _, resolveID) ->
                insert_ TicketResolveRemote
                    { ticketResolveRemoteTicket   = trid
                    , ticketResolveRemoteActivity = resolveID
                    , ticketResolveRemoteActor    = remoteAuthorId author
                    }

    prepareAccept taskID = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audSender <- makeAudSenderWithFollowers authorIdMsig
        deckHash <- encodeKeyHashid deckID
        taskHash <- encodeKeyHashid taskID
        let audDeck =
                AudLocal
                    []
                    [ LocalStageDeckFollowers deckHash
                    , LocalStageTicketFollowers deckHash taskHash
                    ]
        uResolve <- lift $ getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audSender, audDeck]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uResolve]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uResolve
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

------------------------------------------------------------------------------
-- Following
------------------------------------------------------------------------------

-- Meaning: A remote actor is following someone/something
-- Behavior:
--      * Verify the target is me or a ticket of mine
--      * Record the follow in DB
--      * Publish and send an Accept to the sender and its followers
deckFollow
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Follow URIMode
    -> ActE (Text, Act (), Next)
deckFollow now recipDeckID verse follow = do
    recipDeckHash <- encodeKeyHashid recipDeckID
    actorFollow
        (\case
            DeckR d | d == recipDeckHash -> pure Nothing
            TicketR d t | d == recipDeckHash ->
                Just <$> decodeKeyHashidE t "Invalid task keyhashid"
            _ -> throwE "Asking to follow someone else"
        )
        deckActor
        False
        (\ recipDeckActor maybeTaskID ->
            case maybeTaskID of
                Nothing -> pure $ actorFollowers recipDeckActor
                Just taskID -> do
                    maybeTicket <- lift $ getTicket recipDeckID taskID
                    (_deck, _task, Entity _ ticket, _author, _resolve) <-
                        fromMaybeE maybeTicket "I don't have this ticket in DB"
                    return $ ticketFollowers ticket
        )
        (\ _ -> pure $ makeRecipientSet [] [])
        LocalActorDeck
        (\ _ -> pure [])
        now recipDeckID verse follow

------------------------------------------------------------------------------
-- Access
------------------------------------------------------------------------------

-- Meaning: An actor accepted something
-- Behavior:
--     * If it's on an Invite where I'm the resource:
--         * Verify the Accept is by the Invite target
--         * Forward the Accept to my followers
--         * Send a Grant:
--             * To: Accepter (i.e. Invite target)
--             * CC: Invite sender, Accepter's followers, my followers
--     * If it's on a Join where I'm the resource:
--         * Verify the Accept is authorized
--         * Forward the Accept to my followers
--         * Send a Grant:
--             * To: Join sender
--             * CC: Accept sender, Join sender's followers, my followers
--     * If it's an Invite (that I know about) where I'm invited to a project:
--          * If I haven't yet seen the project's approval:
--              * Verify the author is the project
--              * Record the approval in the Stem record in DB
--          * If I saw project's approval, but not my collaborators' approval:
--              * Verify the Accept is authorized
--              * Record the approval in the Stem record in DB
--              * Forward to my followers
--              * Publish and send an Accept:
--                  * To: Inviter, project, Accept author
--                  * CC: Project followers, my followers
--              * Record it in the Stem record in DB as well
--          * If I already saw both approvals, respond with error
--     * If it's an Add (that I know about and already Accepted) where I'm
--       invited to a project:
--          * If I've already seen the project's accept, respond with error
--          * Otherwise, just ignore the Accept
--     * Otherwise respond with error
deckAccept
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Accept URIMode
    -> ActE (Text, Act (), Next)
deckAccept = topicAccept deckActor ComponentDeck

-- Meaning: An actor rejected something
-- Behavior:
--     * If it's on an Invite where I'm the resource:
--         * Verify the Reject is by the Invite target
--         * Remove the relevant Collab record from DB
--         * Forward the Reject to my followers
--         * Send a Reject on the Invite:
--             * To: Rejecter (i.e. Invite target)
--             * CC: Invite sender, Rejecter's followers, my followers
--     * If it's on a Join where I'm the resource:
--         * Verify the Reject is authorized
--         * Remove the relevant Collab record from DB
--         * Forward the Reject to my followers
--         * Send a Reject:
--             * To: Join sender
--             * CC: Reject sender, Join sender's followers, my followers
--     * Otherwise respond with error
deckReject
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Reject URIMode
    -> ActE (Text, Act (), Next)
deckReject = topicReject deckActor LocalActorDeck

-- Meaning: An actor A invited actor B to a resource
-- Behavior:
--      * If resource is my collaborators collection:
--          * Verify A isn't inviting themselves
--          * Verify A is authorized by me to invite actors to me
--          * Verify B doesn't already have an invite/join/grant for me
--          * Remember the invite in DB
--          * Forward the Invite to my followers
--          * Send Accept to A, B, my-followers
--      * If I'm B, i.e. I'm the one being invited:
--          * Verify the resource is some project's components collection URI
--          * For each Stem record I have for this project:
--              * Verify it's not enabled yet, i.e. I'm not already a component
--                of this project
--              * Verify it's not in Invite-Accept state, already got the
--                project's Accept and waiting for my approval
--              * Verify it's not in Add-Accept state, has my approval and
--                waiting for the project's side
--          * Create a Stem record in DB
--          * Insert the Invite to my inbox
--          * Forward the Invite to my followers
deckInvite
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Invite URIMode
    -> ActE (Text, Act (), Next)
deckInvite =
    topicInvite
        deckActor ComponentDeck
        CollabTopicDeckDeck CollabTopicDeckCollab
        CollabTopicDeck StemIdentDeck

-- Meaning: An actor A is removing actor B from a resource
-- Behavior:
--      * Verify the resource is me
--      * Verify A isn't removing themselves
--      * Verify A is authorized by me to remove actors from me
--      * Verify B already has a Grant for me
--      * Remove the whole Collab record from DB
--      * Forward the Remove to my followers
--      * Send a Revoke:
--          * To: Actor B
--          * CC: Actor A, B's followers, my followers
deckRemove
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Remove URIMode
    -> ActE (Text, Act (), Next)
deckRemove =
    topicRemove
        deckActor LocalActorDeck
        CollabTopicDeckDeck CollabTopicDeckCollab

-- Meaning: An actor A asked to join a resource
-- Behavior:
--      * Verify the resource is me
--      * Verify A doesn't already have an invite/join/grant for me
--      * Remember the join in DB
--      * Forward the Join to my followers
deckJoin
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Join URIMode
    -> ActE (Text, Act (), Next)
deckJoin =
    topicJoin
        deckActor LocalActorDeck
        CollabTopicDeckDeck CollabTopicDeckCollab CollabTopicDeck

-- Meaning: An actor is granting access-to-some-resource to another actor
-- Behavior:
--      * If I approved an Add-to-project where I'm the component, and the
--        project is now giving me the delegator-Grant:
--          * Record this in the Stem record in DB
--          * Forward to my followers
--          * Start a delegation chain giving access-to-me, send this new Grant
--            to the project to distribute further, and use the delegator-Grant
--            as the capability
--              * To: Project
--              * CC: My followers, project followers
--      * If I approved an Invite-to-project where I'm the component, and the
--        project is now giving me the delegator-Grant:
--          * Record this in the Stem record in DB
--          * Forward to my followers
--          * Start a delegation chain giving access-to-me, send this new Grant
--            to the project to distribute further, and use the delegator-Grant
--            as the capability
--              * To: Project
--              * CC: My followers, project followers
--      * If the Grant is for an Add/Invite that hasn't had the full approval
--        chain, or I already got the delegator-Grant, raise an error
--      * Otherwise, if I've already seen this Grant or it's simply not related
--        to me, ignore it
deckGrant
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Grant URIMode
    -> ActE (Text, Act (), Next)
deckGrant = componentGrant deckActor ComponentDeck

------------------------------------------------------------------------------
-- Ambiguous: Following/Resolving
------------------------------------------------------------------------------

-- Meaning: An actor is undoing some previous action
-- Behavior:
--      * If they're undoing their Following of me, or a ticket of mine:
--          * Record it in my DB
--          * Publish and send an Accept only to the sender
--      * If they're unresolving a resolved ticket of mine:
--          * Verify they're authorized via a Grant
--          * Record it in my DB
--          * Forward the Undo to my+ticket followers
--          * Send an Accept to sender+followers and to my+ticket followers
--      * Otherwise respond with an error
deckUndo
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Undo URIMode
    -> ActE (Text, Act (), Next)
deckUndo now recipDeckID (Verse authorIdMsig body) (AP.Undo uObject) = do

    -- Check input
    undone <-
        first (\ (actor, _, item) -> (actor, item)) <$>
            parseActivityURI' uObject

    -- Verify the capability URI, if provided, is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCapability <-
        for (AP.activityCapability $ actbActivity body) $ \ uCap ->
            nameExceptT "Undo capability" $
                first (\ (actor, _, item) -> (actor, item)) <$>
                    parseActivityURI' uCap

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (deckRecip, actorRecip) <- lift $ do
            p <- getJust recipDeckID
            (p,) <$> getJust (deckActor p)

        -- Insert the Undo to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
        for mractid $ \ _undoDB -> do

            maybeUndo <- runMaybeT $ do

                -- Find the undone activity in our DB
                undoneDB <- MaybeT $ getActivity undone

                let followers = actorFollowers actorRecip
                asum
                    [ tryUnfollow followers undoneDB authorIdMsig
                    , tryUnresolve maybeCapability undoneDB
                    ]

            (sieve, audience) <-
                fromMaybeE
                    maybeUndo
                    "Undone activity isn't a Follow or Resolve related to me"

            -- Prepare an Accept activity and insert to deck's outbox
            acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorRecip) now
            accept@(actionAccept, _, _, _) <- lift $ lift $ prepareAccept audience
            _luAccept <- lift $ updateOutboxItem' (LocalActorDeck recipDeckID) acceptID actionAccept

            return (deckActor deckRecip, sieve, acceptID, accept)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (actorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept)) -> do
            forwardActivity
                authorIdMsig body (LocalActorDeck recipDeckID) actorID sieve
            lift $ sendActivity
                (LocalActorDeck recipDeckID) actorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            done
                "Undid the Follow/Resolve, forwarded the Undo and published \
                \Accept"

    where

    verifyTargetTicket followerSetID = do
        ticketID <-
            MaybeT $ lift $ getKeyBy $ UniqueTicketFollowers followerSetID
        TicketDeck _ d <-
            MaybeT $ lift $ getValBy $ UniqueTicketDeck ticketID
        guard $ d == recipDeckID

    tryUnfollow deckFollowersID (Left (_actorByKey, _actorE, outboxItemID)) (Left (_, actorID, _)) = do
        Entity followID follow <-
            MaybeT $ lift $ getBy $ UniqueFollowFollow outboxItemID
        let followerID = followActor follow
            followerSetID = followTarget follow
        verifyTargetMe followerSetID <|> verifyTargetTicket followerSetID
        unless (followerID == actorID) $
            lift $ throwE "You're trying to Undo someone else's Follow"
        lift $ lift $ delete followID
        audSenderOnly <- lift $ lift $ lift $ makeAudSenderOnly authorIdMsig
        return (makeRecipientSet [] [], [audSenderOnly])
        where
        verifyTargetMe followerSetID = guard $ followerSetID == deckFollowersID
    tryUnfollow deckFollowersID (Right remoteActivityID) (Right (author, _, _)) = do
        Entity remoteFollowID remoteFollow <-
            MaybeT $ lift $ getBy $ UniqueRemoteFollowFollow remoteActivityID
        let followerID = remoteFollowActor remoteFollow
            followerSetID = remoteFollowTarget remoteFollow
        verifyTargetMe followerSetID <|> verifyTargetTicket followerSetID
        unless (followerID == remoteAuthorId author) $
            lift $ throwE "You're trying to Undo someone else's Follow"
        lift $ lift $ delete remoteFollowID
        audSenderOnly <- lift $ lift $ lift $ makeAudSenderOnly authorIdMsig
        return (makeRecipientSet [] [], [audSenderOnly])
        where
        verifyTargetMe followerSetID = guard $ followerSetID == deckFollowersID
    tryUnfollow _ _ _ = mzero

    tryUnresolve maybeCapability undone = do
        (deleteFromDB, ticketID) <- findTicket undone
        Entity taskID (TicketDeck _ d) <-
            MaybeT $ lift $ getBy $ UniqueTicketDeck ticketID
        guard $ d == recipDeckID

        -- Verify the sender is authorized by the deck to unresolve a ticket
        capability <- lift $ do
            cap <-
                fromMaybeE
                    maybeCapability
                    "Asking to unresolve ticket but no capability provided"
            case cap of
                Left c -> pure c
                Right _ -> throwE "Capability is a remote URI, i.e. not authored by me"
        lift $
            verifyCapability'
                capability
                authorIdMsig
                (LocalActorDeck recipDeckID)
                AP.RoleTriage

        lift $ lift deleteFromDB

        recipDeckHash <- encodeKeyHashid recipDeckID
        taskHash <- encodeKeyHashid taskID
        audSender <- lift $ lift $ makeAudSenderWithFollowers authorIdMsig
        return
            ( makeRecipientSet
                []
                [ LocalStageDeckFollowers recipDeckHash
                , LocalStageTicketFollowers recipDeckHash taskHash
                ]
            , [ AudLocal
                    []
                    [ LocalStageDeckFollowers recipDeckHash
                    , LocalStageTicketFollowers recipDeckHash taskHash
                    ]
              , audSender
              ]
            )
        where
        findTicket (Left (_actorByKey, _actorEntity, itemID)) = do
            Entity resolveLocalID resolveLocal <-
                MaybeT $ lift $ getBy $ UniqueTicketResolveLocalActivity itemID
            let resolveID = ticketResolveLocalTicket resolveLocal
            resolve <- lift $ lift $ getJust resolveID
            let ticketID = ticketResolveTicket resolve
            return
                ( delete resolveLocalID >> delete resolveID
                , ticketID
                )
        findTicket (Right remoteActivityID) = do
            Entity resolveRemoteID resolveRemote <-
                MaybeT $ lift $ getBy $
                    UniqueTicketResolveRemoteActivity remoteActivityID
            let resolveID = ticketResolveRemoteTicket resolveRemote
            resolve <- lift $ lift $ getJust resolveID
            let ticketID = ticketResolveTicket resolve
            return
                ( delete resolveRemoteID >> delete resolveID
                , ticketID
                )

    prepareAccept audience = do
        encodeRouteHome <- getEncodeRouteHome

        uUndo <- getActivityURI authorIdMsig
        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = []
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uUndo
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

------------------------------------------------------------------------------
-- Main behavior function
------------------------------------------------------------------------------

deckBehavior :: UTCTime -> DeckId -> VerseExt -> ActE (Text, Act (), Next)
deckBehavior now deckID (Left verse@(Verse _authorIdMsig body)) =
    case AP.activitySpecific $ actbActivity body of
        AP.AcceptActivity accept -> deckAccept now deckID verse accept
        AP.AddActivity add       -> deckAdd now deckID verse add
        AP.CreateActivity create -> deckCreate now deckID verse create
        AP.FollowActivity follow -> deckFollow now deckID verse follow
        AP.GrantActivity grant   -> deckGrant now deckID verse grant
        AP.InviteActivity invite -> deckInvite now deckID verse invite
        AP.JoinActivity join     -> deckJoin now deckID verse join
        AP.OfferActivity offer   -> deckOffer now deckID verse offer
        AP.RejectActivity reject -> deckReject now deckID verse reject
        AP.RemoveActivity remove -> deckRemove now deckID verse remove
        AP.ResolveActivity resolve -> deckResolve now deckID verse resolve
        AP.UndoActivity undo     -> deckUndo now deckID verse undo
        _ -> throwE "Unsupported activity type for Deck"
deckBehavior _ _ (Right _) = throwE "ClientMsgs aren't supported for Deck"

instance VervisActor Deck where
    actorBehavior = deckBehavior
