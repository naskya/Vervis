{- This file is part of Vervis.
 -
 - Written in 2016, 2019, 2022, 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Handler.Group
    ( getGroupNewR
    , postGroupNewR

    , getGroupR
    , getGroupInboxR
    , postGroupInboxR
    , getGroupOutboxR
    , getGroupOutboxItemR
    , getGroupFollowersR
    , getGroupMessageR

    , getGroupStampR

    , getGroupMembersR
    , getGroupInviteR
    , postGroupInviteR
    , postGroupRemoveR

    , getGroupChildrenR
    , getGroupChildLocalLiveR
    , getGroupChildRemoteLiveR
    , getGroupParentsR





    {-
    , getGroupsR
    , postGroupMembersR
    , getGroupMemberNewR
    , getGroupMemberR
    , deleteGroupMemberR
    , postGroupMemberR
    -}
    )
where

import Control.Applicative
import Control.Monad
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Default.Class
import Data.Foldable
import Data.List
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Network.HTTP.Types.Method
import Optics.Core
import Text.Blaze.Html (Html)
import Yesod.Auth (requireAuth)
import Yesod.Core
import Yesod.Core.Handler (redirect, setMessage, lookupPostParam, notFound)
import Yesod.Form.Functions (runFormPost, runFormGet)
import Yesod.Form.Types (FormResult (..))
import Yesod.Persist.Core (runDB, get404, getBy404)

import qualified Data.ByteString.Lazy as BL
import qualified Database.Esqueleto as E

import Database.Persist.JSON
import Development.PatchMediaType
import Network.FedURI
import Web.ActivityPub hiding (Project (..), Repo (..), Actor (..), ActorDetail (..), ActorLocal (..))
import Yesod.ActivityPub
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Data.Paginate.Local
import Database.Persist.Local
import Yesod.Form.Local
import Yesod.Persist.Local

import Vervis.Access
import Vervis.API
import Vervis.Data.Collab
import Vervis.Federation.Auth
import Vervis.Federation.Discussion
import Vervis.Federation.Offer
import Vervis.Federation.Ticket
import Vervis.FedURI
import Vervis.Form.Ticket
import Vervis.Form.Tracker
import Vervis.Foundation
import Vervis.Model
import Vervis.Paginate
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Recipient
import Vervis.Settings
import Vervis.Ticket
import Vervis.TicketFilter
import Vervis.Time
import Vervis.Web.Actor
import Vervis.Widget
import Vervis.Widget.Person
import Vervis.Widget.Ticket
import Vervis.Widget.Tracker

import qualified Vervis.Client as C

getGroupNewR :: Handler Html
getGroupNewR = do
    ((_result, widget), enctype) <- runFormPost newGroupForm
    defaultLayout $(widgetFile "group/new")

postGroupNewR :: Handler Html
postGroupNewR = do
    NewGroup name desc <- runFormPostRedirect GroupNewR newGroupForm

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    (maybeSummary, audience, detail) <- C.createGroup personHash name desc
    (localRecips, remoteRecips, fwdHosts, action) <-
        C.makeServerInput Nothing maybeSummary audience $ AP.CreateActivity $ AP.Create (AP.CreateTeam detail Nothing) Nothing
    result <-
        runExceptT $
        handleViaActor personID Nothing localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
            redirect GroupNewR
        Right createID -> do
            maybeGroupID <- runDB $ getKeyBy $ UniqueGroupCreate createID
            case maybeGroupID of
                Nothing -> error "Can't find the newly created group"
                Just groupID -> do
                    groupHash <- encodeKeyHashid groupID
                    setMessage "New group created"
                    redirect $ GroupR groupHash

getGroupR :: KeyHashid Group -> Handler TypedContent
getGroupR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    (group, actor, sigKeyIDs) <- runDB $ do
        g <- get404 groupID
        let aid = groupActor g
        a <- getJust aid
        sigKeys <- selectKeysList [SigKeyActor ==. aid] [Asc SigKeyId]
        return (g, a, sigKeys)

    encodeRouteLocal <- getEncodeRouteLocal
    hashSigKey <- getEncodeKeyHashid
    perActor <- asksSite $ appPerActorKeys . appSettings

    let route mk = encodeRouteLocal $ mk groupHash
        actorAP = AP.Actor
            { AP.actorLocal = AP.ActorLocal
                { AP.actorId         = route GroupR
                , AP.actorInbox      = route GroupInboxR
                , AP.actorOutbox     = Just $ route GroupOutboxR
                , AP.actorFollowers  = Just $ route GroupFollowersR
                , AP.actorFollowing  = Nothing
                , AP.actorPublicKeys =
                    map (Left . encodeRouteLocal) $
                    if perActor
                        then map (GroupStampR groupHash . hashSigKey) sigKeyIDs
                        else [ActorKey1R, ActorKey2R]
                , AP.actorSshKeys    = []
                }
            , AP.actorDetail = AP.ActorDetail
                { AP.actorType       = AP.ActorTypeTeam
                , AP.actorUsername   = Nothing
                , AP.actorName       = Just $ actorName actor
                , AP.actorSummary    = Just $ actorDesc actor
                }
            }
        groupAP = AP.Team
            { AP.teamActor    = actorAP
            , AP.teamChildren = encodeRouteLocal $ GroupChildrenR groupHash
            , AP.teamParents  = encodeRouteLocal $ GroupParentsR groupHash
            , AP.teamMembers  = encodeRouteLocal $ GroupMembersR groupHash
            }

    provideHtmlAndAP groupAP $(widgetFile "group/one")

getGroupInboxR :: KeyHashid Group -> Handler TypedContent
getGroupInboxR = getInbox GroupInboxR groupActor

postGroupInboxR :: KeyHashid Group -> Handler ()
postGroupInboxR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    postInbox $ LocalActorGroup groupID

getGroupOutboxR :: KeyHashid Group -> Handler TypedContent
getGroupOutboxR = getOutbox GroupOutboxR GroupOutboxItemR groupActor

getGroupOutboxItemR
    :: KeyHashid Group -> KeyHashid OutboxItem -> Handler TypedContent
getGroupOutboxItemR = getOutboxItem GroupOutboxItemR groupActor

getGroupFollowersR :: KeyHashid Group -> Handler TypedContent
getGroupFollowersR = getActorFollowersCollection GroupFollowersR groupActor

getGroupMessageR
    :: KeyHashid Group -> KeyHashid LocalMessage -> Handler TypedContent
getGroupMessageR _ _ = notFound

getGroupStampR :: KeyHashid Group -> KeyHashid SigKey -> Handler TypedContent
getGroupStampR = servePerActorKey groupActor LocalActorGroup

getGroupMembersR :: KeyHashid Group -> Handler TypedContent
getGroupMembersR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    members <- runDB $ do
        _group <- get404 groupID
        grants <-
            getTopicGrants CollabTopicGroupCollab CollabTopicGroupGroup groupID
        for grants $ \ (role, actor, _ct, time) ->
            (role,time,) <$> bitraverse pure (getRemoteActorURI <=< getJust) actor
    h <- asksSite siteInstanceHost
    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    hashPerson <- getEncodeKeyHashid
    let makeItem (role, time, i) = AP.Relationship
            { AP.relationshipId           = Nothing
            , AP.relationshipExtraTypes   = []
            , AP.relationshipSubject      = encodeRouteHome $ GroupR groupHash
            , AP.relationshipProperty     = Left AP.RelHasMember
            , AP.relationshipObject       =
                case i of
                    Left personID -> encodeRouteHome $ PersonR $ hashPerson personID
                    Right u -> u
            , AP.relationshipAttributedTo = encodeRouteLocal $ GroupR groupHash
            , AP.relationshipPublished    = Just time
            , AP.relationshipUpdated      = Nothing
            , AP.relationshipInstrument   = Just role
            }
        membersAP = AP.Collection
            { AP.collectionId         = encodeRouteLocal $ GroupMembersR groupHash
            , AP.collectionType       = CollectionTypeUnordered
            , AP.collectionTotalItems = Just $ length members
            , AP.collectionCurrent    = Nothing
            , AP.collectionFirst      = Nothing
            , AP.collectionLast       = Nothing
            , AP.collectionItems      = map (Doc h . makeItem) members
            , AP.collectionContext    =
                Just $ encodeRouteLocal $ GroupR groupHash
            }
    provideHtmlAndAP membersAP $ getHtml groupID
    where
    getHtml groupID = do
        (group, actor, members, invites, joins) <- handlerToWidget $ runDB $ do
            group <- get404 groupID
            actor <- getJust $ groupActor group
            members <- do
                grants <-
                    getTopicGrants CollabTopicGroupCollab CollabTopicGroupGroup groupID
                for grants $ \ (role, actor, ct, time) ->
                    (,role,ct,time) <$> getPersonWidgetInfo actor
            invites <- do
                invites' <-
                    getTopicInvites CollabTopicGroupCollab CollabTopicGroupGroup groupID
                for invites' $ \ (inviter, recip, time, role) -> (,,,)
                    <$> (getPersonWidgetInfo =<< bitraverse grabPerson pure inviter)
                    <*> getPersonWidgetInfo recip
                    <*> pure time
                    <*> pure role
            joins <- do
                joins' <-
                    getTopicJoins CollabTopicGroupCollab CollabTopicGroupGroup groupID
                for joins' $ \ (recip, time, role) ->
                    (,time,role) <$> getPersonWidgetInfo recip
            return (group, actor, members, invites, joins)
        $(widgetFile "group/members")
        where
        grabPerson actorID = do
            actorByKey <- getLocalActor actorID
            case actorByKey of
                LocalActorPerson personID -> return personID
                _ -> error "Surprise, local inviter actor isn't a Person"

getGroupInviteR :: KeyHashid Group -> Handler Html
getGroupInviteR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    ((_result, widget), enctype) <- runFormPost $ groupInviteForm groupID
    defaultLayout $(widgetFile "group/member/new")

postGroupInviteR :: KeyHashid Group -> Handler Html
postGroupInviteR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    GroupInvite recipPersonID role <-
        runFormPostRedirect (GroupInviteR groupHash) $ groupInviteForm groupID

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    recipPersonHash <- encodeKeyHashid recipPersonID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        (maybeSummary, audience, invite) <- do
            let uRecipient = encodeRouteHome $ PersonR recipPersonHash
                uResourceCollabs = encodeRouteHome $ GroupMembersR groupHash
            C.invite personID uRecipient uResourceCollabs role
        grantID <- do
            maybeItem <- lift $ runDB $ getGrant CollabTopicGroupCollab CollabTopicGroupGroup groupID personID
            fromMaybeE maybeItem "You need to be a collaborator in the Group to invite people"
        grantHash <- encodeKeyHashid grantID
        let uCap = encodeRouteHome $ GroupOutboxItemR groupHash grantHash
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.InviteActivity invite
        let cap =
                Left (LocalActorGroup groupID, LocalActorGroup groupHash, grantID)
        handleViaActor
            personID (Just cap) localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
            redirect $ GroupInviteR groupHash
        Right inviteID -> do
            setMessage "Invite sent"
            redirect $ GroupMembersR groupHash

postGroupRemoveR :: KeyHashid Group -> CollabTopicGroupId -> Handler Html
postGroupRemoveR groupHash ctID = do
    groupID <- decodeKeyHashid404 groupHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            CollabTopicGroup collabID groupID' <- MaybeT $ get ctID
            guard $ groupID' == groupID
            _ <- MaybeT $ getBy $ UniqueCollabEnable collabID
            member <-
                Left <$> MaybeT (getValBy $ UniqueCollabRecipLocal collabID) <|>
                Right <$> MaybeT (getValBy $ UniqueCollabRecipRemote collabID)
            lift $
                bitraverse
                    (pure . collabRecipLocalPerson)
                    (getRemoteActorURI <=< getJust . collabRecipRemoteActor)
                    member
        pidOrU <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uRecipient <-
                case pidOrU of
                    Left pid -> encodeRouteHome . PersonR <$> encodeKeyHashid pid
                    Right u -> pure u
            let uResourceCollabs = encodeRouteHome $ GroupMembersR groupHash
            C.remove personID uRecipient uResourceCollabs
        grantID <- do
            maybeItem <- lift $ runDB $ getGrant CollabTopicGroupCollab CollabTopicGroupGroup groupID personID
            fromMaybeE maybeItem "You need to be a collaborator in the Group to remove people"
        grantHash <- encodeKeyHashid grantID
        let uCap = encodeRouteHome $ GroupOutboxItemR groupHash grantHash
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap =
                Left (LocalActorGroup groupID, LocalActorGroup groupHash, grantID)
        handleViaActor
            personID (Just cap) localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ GroupMembersR groupHash

getGroupChildrenR :: KeyHashid Group -> Handler TypedContent
getGroupChildrenR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    (actor, group, children) <- runDB $ do
        group <- get404 groupID
        actor <- getJust $ groupActor group
        children <- getChildren groupID
        return (actor, group, children)
    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    hashGroup <- getEncodeKeyHashid
    h <- asksSite siteInstanceHost
    let makeId (Left (childID, _)) =
            encodeRouteHome $ GroupR $ hashGroup childID
        makeId (Right (i, ro, _)) =
            ObjURI (instanceHost i) (remoteObjectIdent ro)
        makeItem (role, time, i) = AP.Relationship
            { AP.relationshipId           = Nothing
            , AP.relationshipExtraTypes   = []
            , AP.relationshipSubject      = encodeRouteHome $ GroupR groupHash
            , AP.relationshipProperty     = Left AP.RelHasChild
            , AP.relationshipObject       = makeId i
            , AP.relationshipAttributedTo = encodeRouteLocal $ GroupR groupHash
            , AP.relationshipPublished    = Just time
            , AP.relationshipUpdated      = Nothing
            , AP.relationshipInstrument   = Just role
            }
        childrenAP = Collection
            { collectionId         = encodeRouteLocal $ GroupChildrenR groupHash
            , collectionType       = CollectionTypeUnordered
            , collectionTotalItems = Just $ length children
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      = map (Doc h . makeItem) children
            , collectionContext    =
                Just $ encodeRouteLocal $ GroupR groupHash
            }
    provideHtmlAndAP childrenAP $ getHtml groupID group actor children

    where

    getChildren groupID = fmap (sortOn $ view _2) $ liftA2 (++)
        (map (\ (E.Value role, E.Value time, E.Value child, Entity _ actor) ->
                (role, time, Left (child, actor))
             )
            <$> getLocals groupID
        )
        (map (\ (E.Value role, E.Value time, Entity _ i, Entity _ ro, Entity _ ra) ->
                (role, time, Right (i, ro, ra))
             )
            <$> getRemotes groupID
        )

    getLocals groupID =
        E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` group `E.InnerJoin` actor `E.InnerJoin` accept `E.InnerJoin` deleg `E.InnerJoin` grant) -> do
            E.on $ deleg E.^. DestThemSendDelegatorLocalGrant E.==. grant E.^. OutboxItemId
            E.on $ accept E.^. DestUsAcceptId E.==. deleg E.^. DestThemSendDelegatorLocalDest
            E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
            E.on $ group E.^. GroupActor E.==. actor E.^. ActorId
            E.on $ topic E.^. DestTopicGroupChild E.==. group E.^. GroupId
            E.on $ holder E.^. DestHolderGroupId E.==. topic E.^. DestTopicGroupHolder
            E.on $ dest E.^. DestId E.==. holder E.^. DestHolderGroupDest
            E.where_ $ holder E.^. DestHolderGroupGroup E.==. E.val groupID
            E.orderBy [E.asc $ grant E.^. OutboxItemPublished]
            return
                ( dest E.^. DestRole
                , grant E.^. OutboxItemPublished
                , topic E.^. DestTopicGroupChild
                , actor
                )

    getRemotes groupID =
        E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` deleg `E.InnerJoin` grant `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ topic E.^. DestTopicRemoteTopic E.==. ra E.^. RemoteActorId
            E.on $ deleg E.^. DestThemSendDelegatorRemoteGrant E.==. grant E.^. RemoteActivityId
            E.on $ accept E.^. DestUsAcceptId E.==. deleg E.^. DestThemSendDelegatorRemoteDest
            E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
            E.on $ dest E.^. DestId E.==. topic E.^. DestTopicRemoteDest
            E.on $ dest E.^. DestId E.==. holder E.^. DestHolderGroupDest
            E.where_ $ holder E.^. DestHolderGroupGroup E.==. E.val groupID
            E.orderBy [E.asc $ grant E.^. RemoteActivityReceived]
            return
                ( dest E.^. DestRole
                , grant E.^. RemoteActivityReceived
                , i
                , ro
                , ra
                )

    getHtml groupID group actor children = do
        $(widgetFile "group/children")

getGroupChildLocalLiveR :: KeyHashid Group -> KeyHashid DestThemSendDelegatorLocal -> Handler ()
getGroupChildLocalLiveR groupHash delegHash = do
    groupID <- decodeKeyHashid404 groupHash
    delegID <- decodeKeyHashid404 delegHash
    runDB $ do
        _ <- get404 groupID
        DestThemSendDelegatorLocal _ localID _ <- get404 delegID
        DestTopicLocal destID <- getJust localID
        Entity _ (DestHolderGroup _ g) <-
            getBy404 $ UniqueDestHolderGroup destID
        unless (g == groupID) notFound

getGroupChildRemoteLiveR :: KeyHashid Group -> KeyHashid DestThemSendDelegatorRemote -> Handler ()
getGroupChildRemoteLiveR groupHash delegHash = do
    groupID <- decodeKeyHashid404 groupHash
    delegID <- decodeKeyHashid404 delegHash
    runDB $ do
        _ <- get404 groupID
        DestThemSendDelegatorRemote _ remoteID _ <- get404 delegID
        DestTopicRemote destID _ <- getJust remoteID
        Entity _ (DestHolderGroup _ g) <-
            getBy404 $ UniqueDestHolderGroup destID
        unless (g == groupID) notFound

getGroupParentsR :: KeyHashid Group -> Handler TypedContent
getGroupParentsR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    (actor, group, parents) <- runDB $ do
        group <- get404 groupID
        actor <- getJust $ groupActor group
        parents <- getParents groupID
        return (actor, group, parents)
    encodeRouteHome <- getEncodeRouteHome
    encodeRouteLocal <- getEncodeRouteLocal
    hashGroup <- getEncodeKeyHashid
    h <- asksSite siteInstanceHost
    let makeId (Left (parentID, _)) =
            encodeRouteHome $ GroupR $ hashGroup parentID
        makeId (Right (i, ro, _)) =
            ObjURI (instanceHost i) (remoteObjectIdent ro)
        makeItem (role, time, i) = AP.Relationship
            { AP.relationshipId           = Nothing
            , AP.relationshipExtraTypes   = []
            , AP.relationshipSubject      = encodeRouteHome $ GroupR groupHash
            , AP.relationshipProperty     = Left AP.RelHasParent
            , AP.relationshipObject       = makeId i
            , AP.relationshipAttributedTo = encodeRouteLocal $ GroupR groupHash
            , AP.relationshipPublished    = Just time
            , AP.relationshipUpdated      = Nothing
            , AP.relationshipInstrument   = Just role
            }
        parentsAP = Collection
            { collectionId         = encodeRouteLocal $ GroupParentsR groupHash
            , collectionType       = CollectionTypeUnordered
            , collectionTotalItems = Just $ length parents
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      = map (Doc h . makeItem) parents
            , collectionContext    =
                Just $ encodeRouteLocal $ GroupR groupHash
            }
    provideHtmlAndAP parentsAP $ getHtml groupID group actor parents

    where

    getParents groupID = fmap (sortOn $ view _2) $ liftA2 (++)
        (map (\ (E.Value role, E.Value time, E.Value parent, Entity _ actor) ->
                (role, time, Left (parent, actor))
             )
            <$> getLocals groupID
        )
        (map (\ (E.Value role, E.Value time, Entity _ i, Entity _ ro, Entity _ ra) ->
                (role, time, Right (i, ro, ra))
             )
            <$> getRemotes groupID
        )

    getLocals groupID =
        E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` group `E.InnerJoin` actor `E.InnerJoin` deleg `E.InnerJoin` grant) -> do
            E.on $ deleg E.^. SourceUsSendDelegatorGrant E.==. grant E.^. OutboxItemId
            E.on $ source E.^. SourceId E.==. deleg E.^. SourceUsSendDelegatorSource
            E.on $ group E.^. GroupActor E.==. actor E.^. ActorId
            E.on $ topic E.^. SourceTopicGroupParent E.==. group E.^. GroupId
            E.on $ holder E.^. SourceHolderGroupId E.==. topic E.^. SourceTopicGroupHolder
            E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderGroupSource
            E.where_ $ holder E.^. SourceHolderGroupGroup E.==. E.val groupID
            E.orderBy [E.asc $ deleg E.^. SourceUsSendDelegatorId]
            return
                ( source E.^. SourceRole
                , grant E.^. OutboxItemPublished
                , topic E.^. SourceTopicGroupParent
                , actor
                )

    getRemotes groupID =
        E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` grant `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ topic E.^. SourceTopicRemoteTopic E.==. ra E.^. RemoteActorId
            E.on $ deleg E.^. SourceUsSendDelegatorGrant E.==. grant E.^. OutboxItemId
            E.on $ source E.^. SourceId E.==. deleg E.^. SourceUsSendDelegatorSource
            E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicRemoteSource
            E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderGroupSource
            E.where_ $ holder E.^. SourceHolderGroupGroup E.==. E.val groupID
            E.orderBy [E.asc $ deleg E.^. SourceUsSendDelegatorId]
            return
                ( source E.^. SourceRole
                , grant E.^. OutboxItemPublished
                , i
                , ro
                , ra
                )

    getHtml groupID group actor parents = do
        $(widgetFile "group/parents")

















{-
getGroupsR :: Handler Html
getGroupsR = do
    groups <- runDB $ select $ from $ \ (sharer, group) -> do
        where_ $ sharer ^. SharerId E.==. group ^. GroupIdent
        orderBy [asc $ sharer ^. SharerIdent]
        return sharer
    defaultLayout $(widgetFile "group/list")

getgid :: ShrIdent -> AppDB GroupId
getgid shar = do
    Entity s _ <- getBy404 $ UniqueSharer shar
    Entity g _ <- getBy404 $ UniqueGroup s
    return g

postGroupMembersR :: ShrIdent -> Handler Html
postGroupMembersR shar = do
    ((result, widget), enctype) <-
        runFormPost $ newGroupMemberForm $ getgid shar
    case result of
        FormSuccess ngm -> do
            now <- liftIO getCurrentTime
            runDB $ do
                gid <- getgid shar
                pid <- do
                    Entity s _ <- getBy404 $ UniqueSharer $ ngmIdent ngm
                    Entity p _ <- getBy404 $ UniquePersonIdent s
                    return p
                let member = GroupMember
                        { groupMemberPerson = pid
                        , groupMemberGroup  = gid
                        , groupMemberRole   = ngmRole ngm
                        , groupMemberJoined = now
                        }
                insert_ member
            redirect $ GroupMemberR shar $ ngmIdent ngm
        FormMissing -> do
            setMessage "Field(s) missing"
            defaultLayout $(widgetFile "group/member/new")
        FormFailure _l -> do
            setMessage "Member insertion failed, see errors below"
            defaultLayout $(widgetFile "group/member/new")

getGroupMemberNewR :: ShrIdent -> Handler Html
getGroupMemberNewR shar = do
    ((_result, widget), enctype) <-
        runFormPost $ newGroupMemberForm $ getgid shar
    defaultLayout $(widgetFile "group/member/new")

getGroupMemberR :: ShrIdent -> ShrIdent -> Handler Html
getGroupMemberR grp memb = do
    member <- runDB $ do
        gid <- do
            Entity s _ <- getBy404 $ UniqueSharer grp
            Entity g _ <- getBy404 $ UniqueGroup s
            return g
        pid <- do
            Entity s _ <- getBy404 $ UniqueSharer memb
            Entity p _ <- getBy404 $ UniquePersonIdent s
            return p
        Entity _mid m <- getBy404 $ UniqueGroupMember pid gid
        return m
    defaultLayout $(widgetFile "group/member/one")

deleteGroupMemberR :: ShrIdent -> ShrIdent -> Handler Html
deleteGroupMemberR grp memb = do
    succ <- runDB $ do
        gid <- do
            Entity s _ <- getBy404 $ UniqueSharer grp
            Entity g _ <- getBy404 $ UniqueGroup s
            return g
        pid <- do
            Entity s _ <- getBy404 $ UniqueSharer memb
            Entity p _ <- getBy404 $ UniquePersonIdent s
            return p
        mm <-
            selectFirst
                [ GroupMemberGroup  ==. gid
                , GroupMemberPerson !=. pid
                , GroupMemberRole   ==. GRAdmin
                ]
                []
        case mm of
            Nothing -> return False
            Just _  -> do
                Entity mid _m <- getBy404 $ UniqueGroupMember pid gid
                delete mid
                return True
    setMessage $
        if succ
            then "Group member removed."
            else "Can’t leave a group without an admin."
    redirect $ GroupMembersR grp

postGroupMemberR :: ShrIdent -> ShrIdent -> Handler Html
postGroupMemberR grp memb = do
    mmethod <- lookupPostParam "_method"
    case mmethod of
        Just "DELETE" -> deleteGroupMemberR grp memb
        _             -> notFound
-}
