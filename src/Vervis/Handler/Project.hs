{- This file is part of Vervis.
 -
 - Written in 2016, 2019, 2022, 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Handler.Project
    ( getProjectR
    , getProjectInboxR
    , postProjectInboxR
    , getProjectOutboxR
    , getProjectOutboxItemR
    , getProjectFollowersR

    , getProjectMessageR

    , getProjectNewR
    , postProjectNewR

    , getProjectStampR

    , getProjectCollabsR
    , getProjectInviteR
    , postProjectInviteR
    , postProjectRemoveR

    , getProjectComponentsR
    , getProjectCollabLiveR

    , getProjectInviteCompR
    , postProjectInviteCompR

    , getProjectChildrenR
    , getProjectParentsR
    , getProjectParentLocalLiveR
    , getProjectParentRemoteLiveR
    )
where

import Control.Applicative
import Control.Monad
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Default.Class
import Data.Foldable
import Data.List
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Network.HTTP.Types.Method
import Optics.Core
import Text.Blaze.Html (Html)
import Yesod.Auth (requireAuth)
import Yesod.Core
import Yesod.Core.Handler (redirect, setMessage, lookupPostParam, notFound)
import Yesod.Form.Functions (runFormPost, runFormGet)
import Yesod.Form.Types (FormResult (..))
import Yesod.Persist.Core (runDB, get404, getBy404)

import qualified Data.ByteString.Lazy as BL
import qualified Database.Esqueleto as E

import Database.Persist.JSON
import Development.PatchMediaType
import Network.FedURI
import Web.ActivityPub hiding (Project (..), Repo (..), Actor (..), ActorDetail (..), ActorLocal (..))
import Yesod.ActivityPub
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Data.Paginate.Local
import Database.Persist.Local
import Yesod.Form.Local
import Yesod.Persist.Local

import Vervis.Access
import Vervis.API
import Vervis.Data.Collab
import Vervis.Federation.Auth
import Vervis.Federation.Discussion
import Vervis.Federation.Offer
import Vervis.Federation.Ticket
import Vervis.FedURI
import Vervis.Form.Ticket
import Vervis.Form.Tracker
import Vervis.Foundation
import Vervis.Model
import Vervis.Paginate
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Recipient
import Vervis.Settings
import Vervis.Ticket
import Vervis.TicketFilter
import Vervis.Time
import Vervis.Web.Actor
import Vervis.Widget
import Vervis.Widget.Person
import Vervis.Widget.Ticket
import Vervis.Widget.Tracker

import qualified Vervis.Client as C

getProjectR :: KeyHashid Project -> Handler TypedContent
getProjectR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    (project, actor, sigKeyIDs) <- runDB $ do
        d <- get404 projectID
        let aid = projectActor d
        a <- getJust aid
        sigKeys <- selectKeysList [SigKeyActor ==. aid] [Asc SigKeyId]
        return (d, a, sigKeys)

    encodeRouteLocal <- getEncodeRouteLocal
    hashSigKey <- getEncodeKeyHashid
    perActor <- asksSite $ appPerActorKeys . appSettings
    let projectAP = AP.Project
            { AP.projectActor = AP.Actor
                { AP.actorLocal = AP.ActorLocal
                    { AP.actorId         = encodeRouteLocal $ ProjectR projectHash
                    , AP.actorInbox      = encodeRouteLocal $ ProjectInboxR projectHash
                    , AP.actorOutbox     =
                        Just $ encodeRouteLocal $ ProjectOutboxR projectHash
                    , AP.actorFollowers  =
                        Just $ encodeRouteLocal $ ProjectFollowersR projectHash
                    , AP.actorFollowing  = Nothing
                    , AP.actorPublicKeys =
                        map (Left . encodeRouteLocal) $
                        if perActor
                            then map (ProjectStampR projectHash . hashSigKey) sigKeyIDs
                            else [ActorKey1R, ActorKey2R]
                    , AP.actorSshKeys    = []
                    }
                , AP.actorDetail = AP.ActorDetail
                    { AP.actorType       = AP.ActorTypeProject
                    , AP.actorUsername   = Nothing
                    , AP.actorName       = Just $ actorName actor
                    , AP.actorSummary    = Just $ actorDesc actor
                    }
                }
            , AP.projectTracker    = Nothing
            , AP.projectChildren   = encodeRouteLocal $ ProjectChildrenR projectHash
            , AP.projectParents    = encodeRouteLocal $ ProjectParentsR projectHash
            , AP.projectComponents =
                encodeRouteLocal $ ProjectComponentsR projectHash
            , AP.projectCollaborators =
                encodeRouteLocal $ ProjectCollabsR projectHash
            }
    provideHtmlAndAP projectAP $(widgetFile "project/one")
    where
    here = ProjectR projectHash

getProjectInboxR :: KeyHashid Project -> Handler TypedContent
getProjectInboxR = getInbox ProjectInboxR projectActor

postProjectInboxR :: KeyHashid Project -> Handler ()
postProjectInboxR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    postInbox $ LocalActorProject projectID

getProjectOutboxR :: KeyHashid Project -> Handler TypedContent
getProjectOutboxR = getOutbox ProjectOutboxR ProjectOutboxItemR projectActor

getProjectOutboxItemR
    :: KeyHashid Project -> KeyHashid OutboxItem -> Handler TypedContent
getProjectOutboxItemR = getOutboxItem ProjectOutboxItemR projectActor

getProjectFollowersR :: KeyHashid Project -> Handler TypedContent
getProjectFollowersR = getActorFollowersCollection ProjectFollowersR projectActor

getProjectMessageR :: KeyHashid Project -> KeyHashid LocalMessage -> Handler Html
getProjectMessageR _ _ = notFound

getProjectNewR :: Handler Html
getProjectNewR = do
    ((_result, widget), enctype) <- runFormPost newProjectForm
    defaultLayout $(widgetFile "project/new")

postProjectNewR :: Handler Html
postProjectNewR = do
    NewProject name desc <- runFormPostRedirect ProjectNewR newProjectForm

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    (maybeSummary, audience, detail) <- C.createProject personHash name desc
    (localRecips, remoteRecips, fwdHosts, action) <-
        C.makeServerInput Nothing maybeSummary audience $ AP.CreateActivity $ AP.Create (AP.CreateProject detail Nothing) Nothing
    result <-
        runExceptT $
        handleViaActor personID Nothing localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
            redirect ProjectNewR
        Right createID -> do
            maybeProjectID <- runDB $ getKeyBy $ UniqueProjectCreate createID
            case maybeProjectID of
                Nothing -> error "Can't find the newly created project"
                Just projectID -> do
                    projectHash <- encodeKeyHashid projectID
                    setMessage "New project created"
                    redirect $ ProjectR projectHash

getProjectStampR :: KeyHashid Project -> KeyHashid SigKey -> Handler TypedContent
getProjectStampR = servePerActorKey projectActor LocalActorProject

getProjectCollabsR :: KeyHashid Project -> Handler TypedContent
getProjectCollabsR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    collabs <- runDB $ do
        _project <- get404 projectID
        grants <- getTopicGrants CollabTopicProjectCollab CollabTopicProjectProject projectID
        for grants $ \ (role, actor, _ct, time) ->
            (role,time,) <$> bitraverse pure (getRemoteActorURI <=< getJust) actor
    h <- asksSite siteInstanceHost
    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    hashPerson <- getEncodeKeyHashid
    let makeItem (role, time, i) = AP.Relationship
            { AP.relationshipId           = Nothing
            , AP.relationshipExtraTypes   = []
            , AP.relationshipSubject      = encodeRouteHome $ ProjectR projectHash
            , AP.relationshipProperty     = Left AP.RelHasCollab
            , AP.relationshipObject       =
                case i of
                    Left personID -> encodeRouteHome $ PersonR $ hashPerson personID
                    Right u -> u
            , AP.relationshipAttributedTo = encodeRouteLocal $ ProjectR projectHash
            , AP.relationshipPublished    = Just time
            , AP.relationshipUpdated      = Nothing
            , AP.relationshipInstrument   = Just role
            }
        collabsAP = AP.Collection
            { AP.collectionId         = encodeRouteLocal $ ProjectCollabsR projectHash
            , AP.collectionType       = CollectionTypeUnordered
            , AP.collectionTotalItems = Just $ length collabs
            , AP.collectionCurrent    = Nothing
            , AP.collectionFirst      = Nothing
            , AP.collectionLast       = Nothing
            , AP.collectionItems      = map (Doc h . makeItem) collabs
            , AP.collectionContext    =
                Just $ encodeRouteLocal $ ProjectR projectHash
            }
    provideHtmlAndAP collabsAP $ getHtml projectID
    where
    getHtml projectID = do
        (project, actor, collabs, invites, joins) <- handlerToWidget $ runDB $ do
            project <- get404 projectID
            actor <- getJust $ projectActor project
            collabs <- do
                grants <-
                    getTopicGrants CollabTopicProjectCollab CollabTopicProjectProject projectID
                for grants $ \ (role, actor, ct, time) ->
                    (,role,ct,time) <$> getPersonWidgetInfo actor
            invites <- do
                invites' <-
                    getTopicInvites CollabTopicProjectCollab CollabTopicProjectProject projectID
                for invites' $ \ (inviter, recip, time, role) -> (,,,)
                    <$> (getPersonWidgetInfo =<< bitraverse grabPerson pure inviter)
                    <*> getPersonWidgetInfo recip
                    <*> pure time
                    <*> pure role
            joins <- do
                joins' <-
                    getTopicJoins CollabTopicProjectCollab CollabTopicProjectProject projectID
                for joins' $ \ (recip, time, role) ->
                    (,time,role) <$> getPersonWidgetInfo recip
            return (project, actor, collabs, invites, joins)
        $(widgetFile "project/collab/list")
        where
        grabPerson actorID = do
            actorByKey <- getLocalActor actorID
            case actorByKey of
                LocalActorPerson personID -> return personID
                _ -> error "Surprise, local inviter actor isn't a Person"

getProjectInviteR :: KeyHashid Project -> Handler Html
getProjectInviteR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    ((_result, widget), enctype) <- runFormPost $ projectInviteForm projectID
    defaultLayout $(widgetFile "project/collab/new")

postProjectInviteR :: KeyHashid Project -> Handler Html
postProjectInviteR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    ProjectInvite recipPersonID role <-
        runFormPostRedirect (ProjectInviteR projectHash) $ projectInviteForm projectID

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    recipPersonHash <- encodeKeyHashid recipPersonID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        (maybeSummary, audience, invite) <- do
            let uRecipient = encodeRouteHome $ PersonR recipPersonHash
                uResourceCollabs = encodeRouteHome $ ProjectCollabsR projectHash
            C.invite personID uRecipient uResourceCollabs role
        grantID <- do
            maybeItem <- lift $ runDB $ getGrant CollabTopicProjectCollab CollabTopicProjectProject projectID personID
            fromMaybeE maybeItem "You need to be a collaborator in the Project to invite people"
        grantHash <- encodeKeyHashid grantID
        let uCap = encodeRouteHome $ ProjectOutboxItemR projectHash grantHash
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.InviteActivity invite
        let cap =
                Left (LocalActorProject projectID, LocalActorProject projectHash, grantID)
        handleViaActor
            personID (Just cap) localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
            redirect $ ProjectInviteR projectHash
        Right inviteID -> do
            setMessage "Invite sent"
            redirect $ ProjectCollabsR projectHash

postProjectRemoveR :: KeyHashid Project -> CollabTopicProjectId -> Handler Html
postProjectRemoveR projectHash ctID = do
    projectID <- decodeKeyHashid404 projectHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            CollabTopicProject collabID projectID' <- MaybeT $ get ctID
            guard $ projectID' == projectID
            _ <- MaybeT $ getBy $ UniqueCollabEnable collabID
            member <-
                Left <$> MaybeT (getValBy $ UniqueCollabRecipLocal collabID) <|>
                Right <$> MaybeT (getValBy $ UniqueCollabRecipRemote collabID)
            lift $
                bitraverse
                    (pure . collabRecipLocalPerson)
                    (getRemoteActorURI <=< getJust . collabRecipRemoteActor)
                    member
        pidOrU <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uRecipient <-
                case pidOrU of
                    Left pid -> encodeRouteHome . PersonR <$> encodeKeyHashid pid
                    Right u -> pure u
            let uResourceCollabs = encodeRouteHome $ ProjectCollabsR projectHash
            C.remove personID uRecipient uResourceCollabs
        grantID <- do
            maybeItem <- lift $ runDB $ getGrant CollabTopicProjectCollab CollabTopicProjectProject projectID personID
            fromMaybeE maybeItem "You need to be a collaborator in the Project to remove people"
        grantHash <- encodeKeyHashid grantID
        let uCap = encodeRouteHome $ ProjectOutboxItemR projectHash grantHash
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap =
                Left (LocalActorProject projectID, LocalActorProject projectHash, grantID)
        handleViaActor
            personID (Just cap) localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ ProjectCollabsR projectHash

getProjectComponentsR :: KeyHashid Project -> Handler TypedContent
getProjectComponentsR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    components <- runDB $ concat <$> sequence
        [ map (Left . ComponentRepo) <$> getRepos projectID
        , map (Left . ComponentDeck) <$> getDecks projectID
        , map (Left . ComponentLoom) <$> getLooms projectID
        , map Right <$> getRemotes projectID
        ]
    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    hashActor <- getHashLocalActor
    let componentsAP = Collection
            { collectionId         = encodeRouteLocal $ ProjectComponentsR projectHash
            , collectionType       = CollectionTypeUnordered
            , collectionTotalItems = Just $ length components
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      =
                map (bimap
                        ( encodeRouteHome
                        . renderLocalActor
                        . hashActor
                        . componentActor
                        )
                        id
                    )
                    components
            , collectionContext    =
                Just $ encodeRouteLocal $ ProjectR projectHash
            }
    provideHtmlAndAP componentsAP $ getHtml projectID

    where

    getRepos projectID =
        fmap (map E.unValue) $
        E.select $ E.from $ \ (comp `E.InnerJoin` enable `E.InnerJoin` local `E.InnerJoin` repo) -> do
            E.on $ local E.^. ComponentLocalId E.==. repo E.^. ComponentLocalRepoComponent
            E.on $ comp E.^. ComponentId E.==. local E.^. ComponentLocalComponent
            E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
            E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
            return $ repo E.^. ComponentLocalRepoRepo

    getDecks projectID =
        fmap (map E.unValue) $
        E.select $ E.from $ \ (comp `E.InnerJoin` enable `E.InnerJoin` local `E.InnerJoin` deck) -> do
            E.on $ local E.^. ComponentLocalId E.==. deck E.^. ComponentLocalDeckComponent
            E.on $ comp E.^. ComponentId E.==. local E.^. ComponentLocalComponent
            E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
            E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
            return $ deck E.^. ComponentLocalDeckDeck

    getLooms projectID =
        fmap (map E.unValue) $
        E.select $ E.from $ \ (comp `E.InnerJoin` enable `E.InnerJoin` local `E.InnerJoin` loom) -> do
            E.on $ local E.^. ComponentLocalId E.==. loom E.^. ComponentLocalLoomComponent
            E.on $ comp E.^. ComponentId E.==. local E.^. ComponentLocalComponent
            E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
            E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
            return $ loom E.^. ComponentLocalLoomLoom

    getRemotes projectID =
        fmap (map $ uncurry ObjURI . bimap E.unValue E.unValue) $
        E.select $ E.from $ \ (comp `E.InnerJoin` enable `E.InnerJoin` remote `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ remote E.^. ComponentRemoteActor E.==. ra E.^. RemoteActorId
            E.on $ comp E.^. ComponentId E.==. remote E.^. ComponentRemoteComponent
            E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
            E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
            return (i E.^. InstanceHost, ro E.^. RemoteObjectIdent)

    getHtml projectID = do
        (project, actor, comps, drafts) <- handlerToWidget $ runDB $ do
            project <- get404 projectID
            actor <- getJust $ projectActor project
            cs <-
                E.select $ E.from $ \ (comp `E.InnerJoin` enable `E.InnerJoin` grant) -> do
                    E.on $ enable E.^. ComponentEnableGrant E.==. grant E.^. OutboxItemId
                    E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                    E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
                    return (comp, grant)
            cs' <- for cs $ \ (Entity cid c, Entity _ i) -> do
                byKeyOrRaid <- bimap snd snd <$> getComponentIdent cid
                identView <-
                    bitraverse
                        (\ byKey -> do
                            actorID <-
                                case byKey of
                                    ComponentRepo k -> repoActor <$> getJust k
                                    ComponentDeck k -> deckActor <$> getJust k
                                    ComponentLoom k -> loomActor <$> getJust k
                            actor <- getJust actorID
                            return (byKey, actor)
                        )
                        (\ remoteActorID -> do
                            remoteActor <- getJust remoteActorID
                            remoteObject <- getJust $ remoteActorIdent remoteActor
                            inztance <- getJust $ remoteObjectInstance remoteObject
                            return (inztance, remoteObject, remoteActor)
                        )
                        byKeyOrRaid
                return (identView, componentRole c, outboxItemPublished i)
            ds <-
                E.select $ E.from $ \ (comp `E.LeftOuterJoin` enable) -> do
                    E.on $ E.just (comp E.^. ComponentId) E.==. enable E.?. ComponentEnableComponent
                    E.where_ $
                        comp E.^. ComponentProject E.==. E.val projectID E.&&.
                        E.isNothing (enable E.?. ComponentEnableId)
                    return comp
            ds' <- for ds $ \ (Entity cid c) -> do
                byKeyOrRaid <- bimap snd snd <$> getComponentIdent cid
                identView <-
                    bitraverse
                        (\ byKey -> do
                            actorID <-
                                case byKey of
                                    ComponentRepo k -> repoActor <$> getJust k
                                    ComponentDeck k -> deckActor <$> getJust k
                                    ComponentLoom k -> loomActor <$> getJust k
                            actor <- getJust actorID
                            return (byKey, actor)
                        )
                        (\ remoteActorID -> do
                            remoteActor <- getJust remoteActorID
                            remoteObject <- getJust $ remoteActorIdent remoteActor
                            inztance <- getJust $ remoteObjectInstance remoteObject
                            return (inztance, remoteObject, remoteActor)
                        )
                        byKeyOrRaid
                return (identView, componentRole c)
            return (project, actor, cs', ds')
        $(widgetFile "project/components")

getProjectCollabLiveR
    :: KeyHashid Project -> KeyHashid CollabEnable -> Handler ()
getProjectCollabLiveR projectHash enableHash = do
    projectID <- decodeKeyHashid404 projectHash
    enableID <- decodeKeyHashid404 enableHash
    runDB $ do
        CollabEnable collabID _ <- get404 enableID
        CollabTopicProject _ j <-
            getValBy404 $ UniqueCollabTopicProject collabID
        unless (j == projectID) notFound

getProjectInviteCompR :: KeyHashid Project -> Handler Html
getProjectInviteCompR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    ((_result, widget), enctype) <- runFormPost projectInviteCompForm
    defaultLayout $(widgetFile "project/component-new")

postProjectInviteCompR :: KeyHashid Project -> Handler Html
postProjectInviteCompR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    uComp <-
        runFormPostRedirect (ProjectInviteCompR projectHash) projectInviteCompForm

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        (maybeSummary, audience, invite) <-
            C.inviteComponent personID projectID uComp
        grantID <- do
            maybeItem <- lift $ runDB $ getGrant CollabTopicProjectCollab CollabTopicProjectProject projectID personID
            fromMaybeE maybeItem "You need to be a collaborator in the Project to invite people"
        grantHash <- encodeKeyHashid grantID
        let uCap = encodeRouteHome $ ProjectOutboxItemR projectHash grantHash
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.InviteActivity invite
        let cap =
                Left (LocalActorProject projectID, LocalActorProject projectHash, grantID)
        handleViaActor
            personID (Just cap) localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
            redirect $ ProjectInviteCompR projectHash
        Right inviteID -> do
            setMessage "Invite sent"
            redirect $ ProjectComponentsR projectHash

getProjectChildrenR :: KeyHashid Project -> Handler TypedContent
getProjectChildrenR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    (actor, project, children) <- runDB $ do
        project <- get404 projectID
        actor <- getJust $ projectActor project
        children <- getChildren projectID
        return (actor, project, children)
    encodeRouteHome <- getEncodeRouteHome
    encodeRouteLocal <- getEncodeRouteLocal
    hashProject <- getEncodeKeyHashid
    h <- asksSite siteInstanceHost
    let makeId (Left (childID, _)) =
            encodeRouteHome $ ProjectR $ hashProject childID
        makeId (Right (i, ro, _)) =
            ObjURI (instanceHost i) (remoteObjectIdent ro)
        makeItem (role, time, i) = AP.Relationship
            { AP.relationshipId           = Nothing
            , AP.relationshipExtraTypes   = []
            , AP.relationshipSubject      = encodeRouteHome $ ProjectR projectHash
            , AP.relationshipProperty     = Left AP.RelHasChild
            , AP.relationshipObject       = makeId i
            , AP.relationshipAttributedTo = encodeRouteLocal $ ProjectR projectHash
            , AP.relationshipPublished    = Just time
            , AP.relationshipUpdated      = Nothing
            , AP.relationshipInstrument   = Just role
            }
        childrenAP = Collection
            { collectionId         = encodeRouteLocal $ ProjectChildrenR projectHash
            , collectionType       = CollectionTypeUnordered
            , collectionTotalItems = Just $ length children
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      = map (Doc h . makeItem) children
            , collectionContext    =
                Just $ encodeRouteLocal $ ProjectR projectHash
            }
    provideHtmlAndAP childrenAP $ getHtml projectID project actor children

    where

    getChildren projectID = fmap (sortOn $ view _2) $ liftA2 (++)
        (map (\ (E.Value role, E.Value time, E.Value child, Entity _ actor) ->
                (role, time, Left (child, actor))
             )
            <$> getLocals projectID
        )
        (map (\ (E.Value role, E.Value time, Entity _ i, Entity _ ro, Entity _ ra) ->
                (role, time, Right (i, ro, ra))
             )
            <$> getRemotes projectID
        )

    getLocals projectID =
        E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` project `E.InnerJoin` actor `E.InnerJoin` deleg `E.InnerJoin` grant) -> do
            E.on $ deleg E.^. SourceUsSendDelegatorGrant E.==. grant E.^. OutboxItemId
            E.on $ source E.^. SourceId E.==. deleg E.^. SourceUsSendDelegatorSource
            E.on $ project E.^. ProjectActor E.==. actor E.^. ActorId
            E.on $ topic E.^. SourceTopicProjectChild E.==. project E.^. ProjectId
            E.on $ holder E.^. SourceHolderProjectId E.==. topic E.^. SourceTopicProjectHolder
            E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
            E.where_ $ holder E.^. SourceHolderProjectProject E.==. E.val projectID
            E.orderBy [E.asc $ deleg E.^. SourceUsSendDelegatorId]
            return
                ( source E.^. SourceRole
                , grant E.^. OutboxItemPublished
                , topic E.^. SourceTopicProjectChild
                , actor
                )

    getRemotes projectID =
        E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` grant `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ topic E.^. SourceTopicRemoteTopic E.==. ra E.^. RemoteActorId
            E.on $ deleg E.^. SourceUsSendDelegatorGrant E.==. grant E.^. OutboxItemId
            E.on $ source E.^. SourceId E.==. deleg E.^. SourceUsSendDelegatorSource
            E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicRemoteSource
            E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
            E.where_ $ holder E.^. SourceHolderProjectProject E.==. E.val projectID
            E.orderBy [E.asc $ deleg E.^. SourceUsSendDelegatorId]
            return
                ( source E.^. SourceRole
                , grant E.^. OutboxItemPublished
                , i
                , ro
                , ra
                )

    getHtml projectID project actor children = do
        $(widgetFile "project/children")

getProjectParentsR :: KeyHashid Project -> Handler TypedContent
getProjectParentsR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    (actor, project, parents) <- runDB $ do
        project <- get404 projectID
        actor <- getJust $ projectActor project
        parents <- getParents projectID
        return (actor, project, parents)
    encodeRouteHome <- getEncodeRouteHome
    encodeRouteLocal <- getEncodeRouteLocal
    hashProject <- getEncodeKeyHashid
    h <- asksSite siteInstanceHost
    let makeId (Left (parentID, _)) =
            encodeRouteHome $ ProjectR $ hashProject parentID
        makeId (Right (i, ro, _)) =
            ObjURI (instanceHost i) (remoteObjectIdent ro)
        makeItem (role, time, i) = AP.Relationship
            { AP.relationshipId           = Nothing
            , AP.relationshipExtraTypes   = []
            , AP.relationshipSubject      = encodeRouteHome $ ProjectR projectHash
            , AP.relationshipProperty     = Left AP.RelHasParent
            , AP.relationshipObject       = makeId i
            , AP.relationshipAttributedTo = encodeRouteLocal $ ProjectR projectHash
            , AP.relationshipPublished    = Just time
            , AP.relationshipUpdated      = Nothing
            , AP.relationshipInstrument   = Just role
            }
        parentsAP = Collection
            { collectionId         = encodeRouteLocal $ ProjectParentsR projectHash
            , collectionType       = CollectionTypeUnordered
            , collectionTotalItems = Just $ length parents
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      = map (Doc h . makeItem) parents
            , collectionContext    =
                Just $ encodeRouteLocal $ ProjectR projectHash
            }
    provideHtmlAndAP parentsAP $ getHtml projectID project actor parents

    where

    getParents projectID = fmap (sortOn $ view _2) $ liftA2 (++)
        (map (\ (E.Value role, E.Value time, E.Value parent, Entity _ actor) ->
                (role, time, Left (parent, actor))
             )
            <$> getLocals projectID
        )
        (map (\ (E.Value role, E.Value time, Entity _ i, Entity _ ro, Entity _ ra) ->
                (role, time, Right (i, ro, ra))
             )
            <$> getRemotes projectID
        )

    getLocals projectID =
        E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` project `E.InnerJoin` actor `E.InnerJoin` accept `E.InnerJoin` deleg `E.InnerJoin` grant) -> do
            E.on $ deleg E.^. DestThemSendDelegatorLocalGrant E.==. grant E.^. OutboxItemId
            E.on $ accept E.^. DestUsAcceptId E.==. deleg E.^. DestThemSendDelegatorLocalDest
            E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
            E.on $ project E.^. ProjectActor E.==. actor E.^. ActorId
            E.on $ topic E.^. DestTopicProjectParent E.==. project E.^. ProjectId
            E.on $ holder E.^. DestHolderProjectId E.==. topic E.^. DestTopicProjectHolder
            E.on $ dest E.^. DestId E.==. holder E.^. DestHolderProjectDest
            E.where_ $ holder E.^. DestHolderProjectProject E.==. E.val projectID
            E.orderBy [E.asc $ grant E.^. OutboxItemPublished]
            return
                ( dest E.^. DestRole
                , grant E.^. OutboxItemPublished
                , topic E.^. DestTopicProjectParent
                , actor
                )

    getRemotes projectID =
        E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` deleg `E.InnerJoin` grant `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ topic E.^. DestTopicRemoteTopic E.==. ra E.^. RemoteActorId
            E.on $ deleg E.^. DestThemSendDelegatorRemoteGrant E.==. grant E.^. RemoteActivityId
            E.on $ accept E.^. DestUsAcceptId E.==. deleg E.^. DestThemSendDelegatorRemoteDest
            E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
            E.on $ dest E.^. DestId E.==. topic E.^. DestTopicRemoteDest
            E.on $ dest E.^. DestId E.==. holder E.^. DestHolderProjectDest
            E.where_ $ holder E.^. DestHolderProjectProject E.==. E.val projectID
            E.orderBy [E.asc $ grant E.^. RemoteActivityReceived]
            return
                ( dest E.^. DestRole
                , grant E.^. RemoteActivityReceived
                , i
                , ro
                , ra
                )

    getHtml projectID project actor parents = do
        $(widgetFile "project/parents")

getProjectParentLocalLiveR :: KeyHashid Project -> KeyHashid DestThemSendDelegatorLocal -> Handler ()
getProjectParentLocalLiveR projectHash delegHash = do
    projectID <- decodeKeyHashid404 projectHash
    delegID <- decodeKeyHashid404 delegHash
    runDB $ do
        _ <- get404 projectID
        DestThemSendDelegatorLocal _ localID _ <- get404 delegID
        DestTopicLocal destID <- getJust localID
        Entity _ (DestHolderProject _ j) <-
            getBy404 $ UniqueDestHolderProject destID
        unless (j == projectID) notFound

getProjectParentRemoteLiveR :: KeyHashid Project -> KeyHashid DestThemSendDelegatorRemote -> Handler ()
getProjectParentRemoteLiveR projectHash delegHash = do
    projectID <- decodeKeyHashid404 projectHash
    delegID <- decodeKeyHashid404 delegHash
    runDB $ do
        _ <- get404 projectID
        DestThemSendDelegatorRemote _ remoteID _ <- get404 delegID
        DestTopicRemote destID _ <- getJust remoteID
        Entity _ (DestHolderProject _ j) <-
            getBy404 $ UniqueDestHolderProject destID
        unless (j == projectID) notFound
