{- This file is part of Vervis.
 -
 - Written in 2022, 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}

module Vervis.Data.Collab
    ( GrantRecipBy (..)
    , hashGrantRecip

    , parseInvite
    , parseJoin
    , parseGrant
    , parseGrant'
    , parseAccept
    , parseReject
    , parseRemove
    , parseAdd

    , grantResourceActorID

    , ComponentBy (..)
    , parseComponent
    , hashComponent
    , unhashComponentE
    , componentActor
    , actorToComponent
    )
where

import Control.Applicative
import Control.Monad
import Control.Monad.Trans.Except
import Data.Barbie
import Data.Bifunctor
import Data.Bitraversable
import Data.Functor.Identity
import Data.Text (Text)
import Data.Traversable
import Database.Persist
import Database.Persist.Types
import GHC.Generics
import Yesod.Core

import Control.Concurrent.Actor
import Data.Time.Clock
import Network.FedURI
import Web.Actor
import Yesod.ActivityPub
import Yesod.Actor
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite (asksSite)

import qualified Web.ActivityPub as AP
import qualified Web.Actor.Persist as WAP

import Control.Monad.Trans.Except.Local
import Database.Persist.Local

import Vervis.Actor
--import Vervis.Actor2
import Vervis.Data.Actor
import Vervis.FedURI
import Vervis.Foundation
import Vervis.Model

parseGrantResourceCollabs (RepoCollabsR r) = Just $ LocalActorRepo r
parseGrantResourceCollabs (DeckCollabsR d) = Just $ LocalActorDeck d
parseGrantResourceCollabs (LoomCollabsR l) = Just $ LocalActorLoom l
parseGrantResourceCollabs (ProjectCollabsR l) = Just $ LocalActorProject l
parseGrantResourceCollabs (GroupMembersR l) = Just $ LocalActorGroup l
parseGrantResourceCollabs _         = Nothing

data GrantRecipBy f = GrantRecipPerson (f Person)
    deriving (Generic, FunctorB, TraversableB, ConstraintsB)

deriving instance AllBF Eq f GrantRecipBy => Eq (GrantRecipBy f)

parseGrantRecip (PersonR p) = Just $ GrantRecipPerson p
parseGrantRecip _           = Nothing

hashGrantRecip (GrantRecipPerson k) =
    GrantRecipPerson <$> WAP.encodeKeyHashid k

unhashGrantRecipPure ctx = f
    where
    f (GrantRecipPerson p) =
        GrantRecipPerson <$> decodeKeyHashidPure ctx p

unhashGrantRecipOld resource = do
    ctx <- asksSite siteHashidsContext
    return $ unhashGrantRecipPure ctx resource

unhashGrantRecip resource = do
    ctx <- asksEnv WAP.stageHashidsContext
    return $ unhashGrantRecipPure ctx resource

unhashGrantRecipEOld resource e =
    ExceptT $ maybe (Left e) Right <$> unhashGrantRecipOld resource

unhashGrantRecipE resource e =
    ExceptT $ maybe (Left e) Right <$> unhashGrantRecip resource

verifyRole = pure

parseTopic
    :: StageRoute Env ~ Route App
    => FedURI -> ActE (Either (LocalActorBy Key) FedURI)
parseTopic u = do
    t <- parseTopic' u
    bitraverse
        (\case
            Left r -> pure r
            Right _ -> throwE "Local topic is a components route"
        )
        pure
        t

parseTopic'
    :: StageRoute Env ~ Route App
    => FedURI
    -> ActE (Either (Either (LocalActorBy Key) ProjectId) FedURI)
parseTopic' u = do
    routeOrRemote <- parseFedURI u
    bitraverse
        (\case
            ProjectComponentsR j ->
                Right <$> WAP.decodeKeyHashidE j "Not a project components route"
            route -> Left <$> do
                resourceHash <-
                    fromMaybeE
                        (parseGrantResourceCollabs route)
                        "Not a shared resource collabs route"
                unhashLocalActorE
                    resourceHash
                    "Contains invalid hashid"
        )
        pure
        routeOrRemote

parseRecipient sender u = do
    routeOrRemote <- parseFedURI u
    bitraverse
        (\ route -> do
            recipHash <-
                fromMaybeE
                    (parseGrantRecip route)
                    "Not a grant recipient route"
            recipKey <-
                unhashGrantRecipE
                    recipHash
                    "Contains invalid hashid"
            case recipKey of
                GrantRecipPerson p | Left (LocalActorPerson p) == sender ->
                    throwE "Invite local sender and recipient are the same Person"
                _ -> return recipKey
        )
        (\ u -> do
            when (Right u == sender) $
                throwE "Invite remote sender and recipient are the same actor"
            return u
        )
        routeOrRemote

parseRecipient' sender u = do
    routeOrRemote <- parseFedURI u
    bitraverse
        (\ route -> do
            recipOrComp <-
                Left <$>
                fromMaybeE
                    (parseGrantRecip route)
                    "Not a grant recipient route"
                <|>
                Right <$>
                fromMaybeE
                    (parseComponent route)
                    "Not a component route"
            bitraverse
                (\ recipHash -> do
                    recipKey <-
                        unhashGrantRecipE
                            recipHash
                            "Contains invalid hashid"
                    case recipKey of
                        GrantRecipPerson p | Left (LocalActorPerson p) == sender ->
                            throwE "Invite local sender and recipient are the same Person"
                        _ -> return recipKey
                )
                (flip unhashComponentE "Contains invalid keyhashid")
                recipOrComp
        )
        (\ u -> do
            when (Right u == sender) $
                throwE "Invite remote sender and recipient are the same actor"
            return u
        )
        routeOrRemote

parseInvite
    :: StageRoute Env ~ Route App
    => Either (LocalActorBy Key) FedURI
    -> AP.Invite URIMode
    -> ActE
        ( AP.Role
        , Either (Either (LocalActorBy Key) ProjectId) FedURI
        , Either (Either (GrantRecipBy Key) (ComponentBy Key)) FedURI
        )
parseInvite sender (AP.Invite instrument object target) =
    (,,)
        <$> verifyRole instrument
        <*> nameExceptT "Invite target" (parseTopic' target)
        <*> nameExceptT "Invite object" (parseRecipient' sender object)

parseJoin
    :: StageRoute Env ~ Route App
    => AP.Join URIMode
    -> ActE (AP.Role, Either (LocalActorBy Key) FedURI)
parseJoin (AP.Join instrument object) =
    (,) <$> verifyRole instrument
        <*> nameExceptT "Join object" (parseTopic object)

parseGrant
    :: Host
    -> AP.Grant URIMode
    -> ActE
        ( AP.RoleExt
        , Either (LocalActorBy Key) LocalURI
        , Either (GrantRecipBy Key) FedURI
        , Maybe (LocalURI, Maybe Int)
        , Maybe UTCTime
        , Maybe UTCTime
        )
parseGrant h (AP.Grant object context target mresult mstart mend allows deleg) = do
    case allows of
        AP.Invoke -> pure ()
        _ -> throwE "Grant.allows isn't invoke"
    case deleg of
        Nothing -> pure ()
        Just _ -> throwE "Grant.delegates is specified"
    (,,,,,)
        <$> verifyRole object
        <*> parseContext context
        <*> parseTarget target
        <*> pure
                (fmap
                    (\ (lu, md) -> (lu, (\ (AP.Duration i) -> i) <$> md))
                    mresult
                )
        <*> pure mstart
        <*> pure mend
    where
    parseContext (ObjURI h' lu) = do
        unless (h == h') $ throwE "Context and author aren't of same host"
        hl <- hostIsLocal h
        if hl
            then Left <$> do
                route <-
                    fromMaybeE
                        (decodeRouteLocal lu)
                        "Grant context isn't a valid route"
                parseLocalActorE' route
            else pure $ Right lu
    parseTarget u@(ObjURI h lu) = do
        hl <- hostIsLocal h
        if hl
            then Left <$> do
                route <-
                    fromMaybeE
                        (decodeRouteLocal lu)
                        "Grant target isn't a valid route"
                recipHash <-
                    fromMaybeE
                        (parseGrantRecip route)
                        "Grant target isn't a grant recipient route"
                unhashGrantRecipE
                    recipHash
                    "Grant target contains invalid hashid"
            else pure $ Right u

parseGrant'
    :: AP.Grant URIMode
    -> ActE
        ( AP.RoleExt
        , Either (LocalActorBy Key) FedURI
        , Either (LocalActorBy Key) FedURI
        , Maybe (LocalURI, Maybe Int)
        , Maybe UTCTime
        , Maybe UTCTime
        , AP.Usage
        , Maybe (Either (LocalActorBy Key, OutboxItemId) FedURI)
        )
parseGrant' (AP.Grant object context target mresult mstart mend allows deleg) =
    (,,,,,,,)
        <$> verifyRole object
        <*> parseContext context
        <*> parseTarget target
        <*> pure
                (fmap
                    (\ (lu, md) -> (lu, (\ (AP.Duration i) -> i) <$> md))
                    mresult
                )
        <*> pure mstart
        <*> pure mend
        <*> pure allows
        <*> for deleg (fmap (first (\ (actor, _, item) -> (actor, item))) . parseActivityURI')
    where
    parseContext u@(ObjURI h lu) = do
        hl <- hostIsLocal h
        if hl
            then Left <$> do
                route <-
                    fromMaybeE
                        (decodeRouteLocal lu)
                        "Grant context isn't a valid route"
                parseLocalActorE' route
            else pure $ Right u
    parseTarget u@(ObjURI h lu) = nameExceptT "Grant target" $ do
        hl <- hostIsLocal h
        if hl
            then Left <$> do
                route <-
                    fromMaybeE
                        (decodeRouteLocal lu)
                        "Grant target isn't a valid route"
                parseLocalActorE' route
            else pure $ Right u

parseAccept (AP.Accept object mresult) = do
    --verifyNothingE mresult "Accept must not contain 'result'"
    first (\ (actor, _, item) -> (actor, item)) <$>
        nameExceptT "Accept object" (parseActivityURI' object)

parseReject (AP.Reject object) =
    first (\ (actor, _, item) -> (actor, item)) <$>
        nameExceptT "Reject object" (parseActivityURI' object)

parseRemove
    :: StageRoute Env ~ Route App
    => Either (LocalActorBy Key) FedURI
    -> AP.Remove URIMode
    -> ActE
        ( Either (Either (LocalActorBy Key) ProjectId) FedURI
        , Either (Either (GrantRecipBy Key) (ComponentBy Key)) FedURI
        )
parseRemove sender (AP.Remove object origin) =
    (,) <$> nameExceptT "Remove origin" (parseTopic' origin)
        <*> nameExceptT "Remove object" (parseRecipient' sender object)

parseAdd
    :: StageRoute Env ~ Route App
    => Either (LocalActorBy Key) FedURI
    -> AP.Add URIMode
    -> ActE
        ( Either (ComponentBy Key) FedURI
        , Either ProjectId FedURI
        , AP.Role
        )
parseAdd sender (AP.Add object target role) = do
    result@(component, collection) <-
        (,) <$> nameExceptT "Add.object" (parseComponent' object)
            <*> nameExceptT "Add.target" (parseProjectComps target)
    case result of
        (Right u, Right v) | u == v -> throwE "Object and target are the same"
        _ -> pure ()
    when (sender == first componentActor component) $
        throwE "Sender and component are the same"
    case collection of
        Left projectID | sender == Left (LocalActorProject projectID) ->
            throwE "Sender and project are the same"
        _ -> pure ()
    return (component, collection, role)
    where
    parseComponent' (Right _) = throwE "Not a component URI"
    parseComponent' (Left u) = do
        routeOrRemote <- parseFedURI u
        bitraverse
            (\ route -> do
                componentHash <-
                    fromMaybeE
                        (parseComponent route)
                        "Not a component route"
                unhashComponentE
                    componentHash
                    "Contains invalid hashid"
            )
            pure
            routeOrRemote
    parseProjectComps u = do
        routeOrRemote <- parseFedURI u
        bitraverse
            (\case
                ProjectComponentsR j -> WAP.decodeKeyHashidE j "Inavlid hashid"
                _ -> throwE "Not a project components collection route"
            )
            pure
            routeOrRemote

grantResourceActorID :: LocalActorBy Identity -> ActorId
grantResourceActorID (LocalActorPerson (Identity p)) = personActor p
grantResourceActorID (LocalActorRepo (Identity r)) = repoActor r
grantResourceActorID (LocalActorDeck (Identity d)) = deckActor d
grantResourceActorID (LocalActorLoom (Identity l)) = loomActor l
grantResourceActorID (LocalActorProject (Identity j)) = projectActor j
grantResourceActorID (LocalActorGroup (Identity g)) = groupActor g

data ComponentBy f
    = ComponentRepo (f Repo)
    | ComponentDeck (f Deck)
    | ComponentLoom (f Loom)
    deriving (Generic, FunctorB, TraversableB, ConstraintsB)

deriving instance AllBF Eq f ComponentBy => Eq (ComponentBy f)

parseComponent (RepoR r) = Just $ ComponentRepo r
parseComponent (DeckR d) = Just $ ComponentDeck d
parseComponent (LoomR l) = Just $ ComponentLoom l
parseComponent _         = Nothing

hashComponent (ComponentRepo k) = ComponentRepo <$> WAP.encodeKeyHashid k
hashComponent (ComponentDeck k) = ComponentDeck <$> WAP.encodeKeyHashid k
hashComponent (ComponentLoom k) = ComponentLoom <$> WAP.encodeKeyHashid k

unhashComponentPure ctx = f
    where
    f (ComponentRepo r) =
        ComponentRepo <$> decodeKeyHashidPure ctx r
    f (ComponentDeck d) =
        ComponentDeck <$> decodeKeyHashidPure ctx d
    f (ComponentLoom l) =
        ComponentLoom <$> decodeKeyHashidPure ctx l

unhashComponent c = do
    ctx <- asksEnv WAP.stageHashidsContext
    return $ unhashComponentPure ctx c

unhashComponentE c e = ExceptT $ maybe (Left e) Right <$> unhashComponent c

componentActor (ComponentRepo r) = LocalActorRepo r
componentActor (ComponentDeck d) = LocalActorDeck d
componentActor (ComponentLoom l) = LocalActorLoom l

actorToComponent = \case
    LocalActorPerson _ -> Nothing
    LocalActorRepo k -> Just $ ComponentRepo k
    LocalActorDeck k -> Just $ ComponentDeck k
    LocalActorLoom k -> Just $ ComponentLoom k
    LocalActorProject _ -> Nothing
    LocalActorGroup _ -> Nothing
