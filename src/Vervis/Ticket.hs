{- This file is part of Vervis.
 -
 - Written in 2016, 2018, 2019, 2020, 2021, 2022
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Ticket
    (
      getTicketSummaries
    , getClothSummaries
    --, getTicketDepEdges

    , WorkflowFieldFilter (..)
    , WorkflowFieldSummary (..)
    , TicketTextParamValue (..)
    , TicketTextParam (..)
    , getTicketTextParams
    , WorkflowEnumSummary (..)
    , TicketEnumParamValue (..)
    , TicketEnumParam (..)
    , getTicketEnumParams
    , TicketClassParam (..)
    , getTicketClasses


    , getTicket
    , getTicket404

    --, getDependencyCollection
    --, getReverseDependencyCollection

    --, getWorkItem

    --, checkDepAndTarget
    )
where

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Bitraversable
import Data.Either
import Data.Foldable (for_)
import Data.Maybe
import Data.Text (Text)
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Yesod.Core (notFound)
import Yesod.Core.Content
import Yesod.Persist.Core

import qualified Database.Esqueleto as E

import Network.FedURI
import Web.ActivityPub hiding (Ticket, Project)
import Yesod.ActivityPub
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Data.Paginate.Local
import Database.Persist.Local
import Yesod.Persist.Local

import Vervis.Data.Ticket
import Vervis.FedURI
import Vervis.Foundation
import Vervis.Model
import Vervis.Model.Ident
import Vervis.Model.Workflow
import Vervis.Paginate
import Vervis.Persist.Actor
import Vervis.Recipient
import Vervis.Widget.Ticket

-- | Get summaries of all the tickets in the given project.
getTicketSummaries
    :: Maybe (E.SqlExpr (Maybe (Entity TicketResolve)) -> E.SqlExpr (E.Value Bool))
    -> Maybe (E.SqlExpr (Entity Ticket) -> [E.SqlExpr E.OrderBy])
    -> Maybe (Int, Int)
    -> DeckId
    -> AppDB [TicketSummary]
getTicketSummaries mfilt morder offlim deckID = do
    tickets <- E.select $ E.from $
        \ ( t
            `E.InnerJoin` td
            `E.LeftOuterJoin` (tal `E.InnerJoin` p `E.InnerJoin` a)
            `E.LeftOuterJoin` (tar `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i)
            `E.LeftOuterJoin` tr
            `E.InnerJoin` d
            `E.LeftOuterJoin` m
          ) -> do
            E.on $ E.just (d E.^. DiscussionId) E.==. m E.?. MessageRoot
            E.on $ t E.^. TicketDiscuss E.==. d E.^. DiscussionId

            E.on $ E.just (t E.^. TicketId) E.==. tr E.?. TicketResolveTicket

            E.on $ ro E.?. RemoteObjectInstance E.==. i E.?. InstanceId
            E.on $ ra E.?. RemoteActorIdent E.==. ro E.?. RemoteObjectId
            E.on $ tar E.?. TicketAuthorRemoteAuthor E.==. ra E.?. RemoteActorId
            E.on $ E.just (t E.^. TicketId) E.==. tar E.?. TicketAuthorRemoteTicket

            E.on $ p E.?. PersonActor E.==. a E.?. ActorId
            E.on $ tal E.?. TicketAuthorLocalAuthor E.==. p E.?. PersonId
            E.on $ E.just (t E.^. TicketId) E.==. tal E.?. TicketAuthorLocalTicket

            E.on $ t E.^. TicketId E.==. td E.^. TicketDeckTicket

            E.where_ $ td E.^. TicketDeckDeck E.==. E.val deckID
            E.groupBy
                ( (t E.^. TicketId, td E.^. TicketDeckId)
                , tr E.?. TicketResolveId
                , tal E.?. TicketAuthorLocalId, p E.?. PersonId, a E.?. ActorId
                , ra E.?. RemoteActorId, ro E.?. RemoteObjectId, i E.?. InstanceId
                )
            for_ mfilt $ \ filt -> E.where_ $ filt tr
            for_ morder $ \ order -> E.orderBy $ order t
            for_ offlim $ \ (off, lim) -> do
                E.offset $ fromIntegral off
                E.limit $ fromIntegral lim

            return
                ( t E.^. TicketId
                , td E.^. TicketDeckId
                , p, a
                , i, ro, ra
                , t E.^. TicketCreated
                , t E.^. TicketTitle
                , tr E.?. TicketResolveId
                , E.count $ m E.?. MessageId
                )

    for tickets $
        \ (E.Value tid, E.Value tdid, mp, ma, mi, mro, mra, E.Value c, E.Value t, E.Value mc, E.Value r) -> do
            labels <- E.select $ E.from $ \ (tpc `E.InnerJoin` wf) -> do
                E.on $ tpc E.^. TicketParamClassField E.==. wf E.^. WorkflowFieldId
                E.where_ $ tpc E.^. TicketParamClassTicket E.==. E.val tid
                return wf
            return TicketSummary
                { tsId        = tdid
                , tsCreatedBy =
                    case (mp, ma, mi, mro, mra) of
                        (Just p, Just a, Nothing, Nothing, Nothing) ->
                            Left (p, entityVal a)
                        (Nothing, Nothing, Just i, Just ro, Just ra) ->
                            Right (entityVal i, entityVal ro, entityVal ra)
                        _ -> error "Ticket author DB invalid state"
                , tsCreatedAt = c
                , tsTitle     = t
                , tsLabels    = map entityVal labels
                , tsClosed    = isJust mc
                , tsComments  = r
                }

getClothSummaries
    :: Maybe (E.SqlExpr (Maybe (Entity TicketResolve)) -> E.SqlExpr (E.Value Bool))
    -> Maybe (E.SqlExpr (Entity Ticket) -> [E.SqlExpr E.OrderBy])
    -> Maybe (Int, Int)
    -> LoomId
    -> AppDB [ClothSummary]
getClothSummaries mfilt morder offlim loomID = do
    tickets <- E.select $ E.from $
        \ ( t
            `E.InnerJoin` tl
            `E.LeftOuterJoin` (tal `E.InnerJoin` p `E.InnerJoin` a)
            `E.LeftOuterJoin` (tar `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i)
            `E.LeftOuterJoin` tr
            `E.InnerJoin` d
            `E.LeftOuterJoin` m
          ) -> do
            E.on $ E.just (d E.^. DiscussionId) E.==. m E.?. MessageRoot
            E.on $ t E.^. TicketDiscuss E.==. d E.^. DiscussionId

            E.on $ E.just (t E.^. TicketId) E.==. tr E.?. TicketResolveTicket

            E.on $ ro E.?. RemoteObjectInstance E.==. i E.?. InstanceId
            E.on $ ra E.?. RemoteActorIdent E.==. ro E.?. RemoteObjectId
            E.on $ tar E.?. TicketAuthorRemoteAuthor E.==. ra E.?. RemoteActorId
            E.on $ E.just (t E.^. TicketId) E.==. tar E.?. TicketAuthorRemoteTicket

            E.on $ p E.?. PersonActor E.==. a E.?. ActorId
            E.on $ tal E.?. TicketAuthorLocalAuthor E.==. p E.?. PersonId
            E.on $ E.just (t E.^. TicketId) E.==. tal E.?. TicketAuthorLocalTicket

            E.on $ t E.^. TicketId E.==. tl E.^. TicketLoomTicket

            E.where_ $ tl E.^. TicketLoomLoom E.==. E.val loomID
            E.groupBy
                ( (t E.^. TicketId, tl E.^. TicketLoomId)
                , tr E.?. TicketResolveId
                , tal E.?. TicketAuthorLocalId, p E.?. PersonId, a E.?. ActorId
                , ra E.?. RemoteActorId, ro E.?. RemoteObjectId, i E.?. InstanceId
                )
            for_ mfilt $ \ filt -> E.where_ $ filt tr
            for_ morder $ \ order -> E.orderBy $ order t
            for_ offlim $ \ (off, lim) -> do
                E.offset $ fromIntegral off
                E.limit $ fromIntegral lim

            return
                ( t E.^. TicketId
                , tl E.^. TicketLoomId
                , p, a
                , i, ro, ra
                , t E.^. TicketCreated
                , t E.^. TicketTitle
                , tr E.?. TicketResolveId
                , E.count $ m E.?. MessageId
                )

    for tickets $
        \ (E.Value tid, E.Value tlid, mp, ma, mi, mro, mra, E.Value c, E.Value t, E.Value mc, E.Value r) -> do
            labels <- E.select $ E.from $ \ (tpc `E.InnerJoin` wf) -> do
                E.on $ tpc E.^. TicketParamClassField E.==. wf E.^. WorkflowFieldId
                E.where_ $ tpc E.^. TicketParamClassTicket E.==. E.val tid
                return wf
            return ClothSummary
                { csId        = tlid
                , csCreatedBy =
                    case (mp, ma, mi, mro, mra) of
                        (Just p, Just a, Nothing, Nothing, Nothing) ->
                            Left (p, entityVal a)
                        (Nothing, Nothing, Just i, Just ro, Just ra) ->
                            Right (entityVal i, entityVal ro, entityVal ra)
                        _ -> error "Ticket author DB invalid state"
                , csCreatedAt = c
                , csTitle     = t
                , csLabels    = map entityVal labels
                , csClosed    = isJust mc
                , csComments  = r
                }

-- | Get the child-parent ticket number pairs of all the ticket dependencies
-- in the given project, in ascending order by child, and then ascending order
-- by parent.
{-
getTicketDepEdges :: ProjectId -> AppDB [(Int64, Int64)]
getTicketDepEdges jid =
    fmap (map $ fromSqlKey . unValue *** fromSqlKey . unValue) $
    select $ from $
        \ (t1 `InnerJoin` tcl1 `InnerJoin` tpl1 `InnerJoin`
           td `InnerJoin`
           t2 `InnerJoin` tcl2 `InnerJoin` tpl2
          ) -> do
            on $ tcl2 ^. TicketContextLocalId ==. tpl2 ^. TicketProjectLocalContext
            on $ t2 ^. TicketId ==. tcl2 ^. TicketContextLocalTicket
            on $ t2 ^. TicketId ==. td ^. TicketDependencyParent
            on $ t1 ^. TicketId ==. td ^. TicketDependencyChild
            on $ tcl1 ^. TicketContextLocalId ==. tpl1 ^. TicketProjectLocalContext
            on $ t1 ^. TicketId ==. tcl1 ^. TicketContextLocalTicket
            where_ $
                tpl1 ^. TicketProjectLocalProject ==. val jid &&.
                tpl2 ^. TicketProjectLocalProject ==. val jid
            orderBy [asc $ t1 ^. TicketId, asc $ t2 ^. TicketId]
            return (t1 ^. TicketId, t2 ^. TicketId)
-}

data WorkflowFieldFilter = WorkflowFieldFilter
    { wffNew    :: Bool
    , wffTodo   :: Bool
    , wffClosed :: Bool
    }

data WorkflowFieldSummary = WorkflowFieldSummary
    { wfsId       :: WorkflowFieldId
    , wfsIdent    :: FldIdent
    , wfsName     :: Text
    , wfsRequired :: Bool
    , wfsConstant :: Bool
    , wfsFilter   :: WorkflowFieldFilter
    }

data TicketTextParamValue = TicketTextParamValue
    { ttpvId  :: TicketParamTextId
    , ttpvVal :: Text
    }

data TicketTextParam = TicketTextParam
    { ttpField :: WorkflowFieldSummary
    , ttpValue :: Maybe TicketTextParamValue
    }

toTParam
    :: ( E.Value WorkflowFieldId
       , E.Value FldIdent
       , E.Value Text
       , E.Value Bool
       , E.Value Bool
       , E.Value Bool
       , E.Value Bool
       , E.Value Bool
       , E.Value (Maybe TicketParamTextId)
       , E.Value (Maybe Text)
       )
    -> TicketTextParam
toTParam
    ( E.Value fid
    , E.Value fld
    , E.Value name
    , E.Value req
    , E.Value con
    , E.Value new
    , E.Value todo
    , E.Value closed
    , E.Value mp
    , E.Value mt
    ) =
        TicketTextParam
            { ttpField = WorkflowFieldSummary
                { wfsId       = fid
                , wfsIdent    = fld
                , wfsName     = name
                , wfsRequired = req
                , wfsConstant = con
                , wfsFilter   = WorkflowFieldFilter
                    { wffNew    = new
                    , wffTodo   = todo
                    , wffClosed = closed
                    }
                }
            , ttpValue =
                case (mp, mt) of
                    (Just p,  Just t)  ->
                        Just TicketTextParamValue
                            { ttpvId  = p
                            , ttpvVal = t
                            }
                    (Nothing, Nothing) -> Nothing
                    _                  -> error "Impossible"
            }

getTicketTextParams :: TicketId {--> WorkflowId-} -> AppDB [TicketTextParam]
getTicketTextParams tid {-wid-} = fmap (map toTParam) $
    E.select $ E.from $ \ (p `E.RightOuterJoin` f) -> do
        E.on $
            p E.?. TicketParamTextField  E.==. E.just (f E.^. WorkflowFieldId) E.&&.
            p E.?. TicketParamTextTicket E.==. E.just (E.val tid)
        E.where_ $
            --f E.^. WorkflowFieldWorkflow E.==. E.val wid     E.&&.
            f E.^. WorkflowFieldType     E.==. E.val WFTText E.&&.
            E.isNothing (f E.^. WorkflowFieldEnm)
        return
            ( f E.^. WorkflowFieldId
            , f E.^. WorkflowFieldIdent
            , f E.^. WorkflowFieldName
            , f E.^. WorkflowFieldRequired
            , f E.^. WorkflowFieldConstant
            , f E.^. WorkflowFieldFilterNew
            , f E.^. WorkflowFieldFilterTodo
            , f E.^. WorkflowFieldFilterClosed
            , p E.?. TicketParamTextId
            , p E.?. TicketParamTextValue
            )

data WorkflowEnumSummary = WorkflowEnumSummary
    { wesId    :: WorkflowEnumId
    , wesIdent :: EnmIdent
    }

data TicketEnumParamValue = TicketEnumParamValue
    { tepvId   :: TicketParamEnumId
    , tepvVal  :: WorkflowEnumCtorId
    , tepvName :: Text
    }

data TicketEnumParam = TicketEnumParam
    { tepField :: WorkflowFieldSummary
    , tepEnum  :: WorkflowEnumSummary
    , tepValue :: Maybe TicketEnumParamValue
    }

toEParam
    :: ( E.Value WorkflowFieldId
       , E.Value FldIdent
       , E.Value Text
       , E.Value Bool
       , E.Value Bool
       , E.Value Bool
       , E.Value Bool
       , E.Value Bool
       , E.Value WorkflowEnumId
       , E.Value EnmIdent
       , E.Value (Maybe TicketParamEnumId)
       , E.Value (Maybe WorkflowEnumCtorId)
       , E.Value (Maybe Text)
       )
    -> TicketEnumParam
toEParam
    ( E.Value fid
    , E.Value fld
    , E.Value name
    , E.Value req
    , E.Value con
    , E.Value new
    , E.Value todo
    , E.Value closed
    , E.Value i
    , E.Value e
    , E.Value mp
    , E.Value mc
    , E.Value mt
    ) =
        TicketEnumParam
            { tepField = WorkflowFieldSummary
                { wfsId       = fid
                , wfsIdent    = fld
                , wfsName     = name
                , wfsRequired = req
                , wfsConstant = con
                , wfsFilter   = WorkflowFieldFilter
                    { wffNew    = new
                    , wffTodo   = todo
                    , wffClosed = closed
                    }
                }
            , tepEnum = WorkflowEnumSummary
                { wesId    = i
                , wesIdent = e
                }
            , tepValue =
                case (mp, mc, mt) of
                    (Just p,  Just c,  Just t)  ->
                        Just TicketEnumParamValue
                            { tepvId   = p
                            , tepvVal  = c
                            , tepvName = t
                            }
                    (Nothing, Nothing, Nothing) -> Nothing
                    _                           -> error "Impossible"
            }

getTicketEnumParams :: TicketId {--> WorkflowId-} -> AppDB [TicketEnumParam]
getTicketEnumParams tid {-wid-} = fmap (map toEParam) $
    E.select $ E.from $ \ (p `E.InnerJoin` c `E.RightOuterJoin` f `E.InnerJoin` e) -> do
        E.on $
            --e E.^. WorkflowEnumWorkflow E.==. E.val wid E.&&.
            f E.^. WorkflowFieldEnm     E.==. E.just (e E.^. WorkflowEnumId)
        E.on $
            --f E.^. WorkflowFieldWorkflow E.==. E.val wid                       E.&&.
            f E.^. WorkflowFieldType     E.==. E.val WFTEnum                   E.&&.
            p E.?. TicketParamEnumField  E.==. E.just (f E.^. WorkflowFieldId) E.&&.
            c E.?. WorkflowEnumCtorEnum  E.==. f E.^. WorkflowFieldEnm
        E.on $
            p E.?. TicketParamEnumTicket E.==. E.just (E.val tid) E.&&.
            p E.?. TicketParamEnumValue  E.==. c E.?. WorkflowEnumCtorId
        return
            ( f E.^. WorkflowFieldId
            , f E.^. WorkflowFieldIdent
            , f E.^. WorkflowFieldName
            , f E.^. WorkflowFieldRequired
            , f E.^. WorkflowFieldConstant
            , f E.^. WorkflowFieldFilterNew
            , f E.^. WorkflowFieldFilterTodo
            , f E.^. WorkflowFieldFilterClosed
            , e E.^. WorkflowEnumId
            , e E.^. WorkflowEnumIdent
            , p E.?. TicketParamEnumId
            , c E.?. WorkflowEnumCtorId
            , c E.?. WorkflowEnumCtorName
            )

data TicketClassParam = TicketClassParam
    { tcpField :: WorkflowFieldSummary
    , tcpValue :: Maybe TicketParamClassId
    }

toCParam
    :: ( E.Value WorkflowFieldId
       , E.Value FldIdent
       , E.Value Text
       , E.Value Bool
       , E.Value Bool
       , E.Value Bool
       , E.Value Bool
       , E.Value Bool
       , E.Value (Maybe TicketParamClassId)
       )
    -> TicketClassParam
toCParam
    ( E.Value fid
    , E.Value fld
    , E.Value name
    , E.Value req
    , E.Value con
    , E.Value new
    , E.Value todo
    , E.Value closed
    , E.Value mp
    ) = TicketClassParam
            { tcpField = WorkflowFieldSummary
                { wfsId       = fid
                , wfsIdent    = fld
                , wfsName     = name
                , wfsRequired = req
                , wfsConstant = con
                , wfsFilter   = WorkflowFieldFilter
                    { wffNew    = new
                    , wffTodo   = todo
                    , wffClosed = closed
                    }
                }
            , tcpValue = mp
            }

getTicketClasses :: TicketId {--> WorkflowId-} -> AppDB [TicketClassParam]
getTicketClasses tid {-wid-} = fmap (map toCParam) $
    E.select $ E.from $ \ (p `E.RightOuterJoin` f) -> do
        E.on $
            p E.?. TicketParamClassField  E.==. E.just (f E.^. WorkflowFieldId) E.&&.
            p E.?. TicketParamClassTicket E.==. E.just (E.val tid)
        E.where_ $
            --f E.^. WorkflowFieldWorkflow E.==. E.val wid      E.&&.
            f E.^. WorkflowFieldType     E.==. E.val WFTClass E.&&.
            E.isNothing (f E.^. WorkflowFieldEnm)
        return
            ( f E.^. WorkflowFieldId
            , f E.^. WorkflowFieldIdent
            , f E.^. WorkflowFieldName
            , f E.^. WorkflowFieldRequired
            , f E.^. WorkflowFieldConstant
            , f E.^. WorkflowFieldFilterNew
            , f E.^. WorkflowFieldFilterTodo
            , f E.^. WorkflowFieldFilterClosed
            , p E.?. TicketParamClassId
            )

getTicket
    :: MonadIO m
    => DeckId
    -> TicketDeckId
    -> ReaderT SqlBackend m
        ( Maybe
            ( Entity Deck
            , Entity TicketDeck
            , Entity Ticket
            , Either (Entity TicketAuthorLocal) (Entity TicketAuthorRemote)
            , Maybe
                ( Entity TicketResolve
                , Either
                    (Entity TicketResolveLocal)
                    (Entity TicketResolveRemote)
                )
            )
        )
getTicket did tdid = runMaybeT $ do
    d <- MaybeT $ get did
    td <- MaybeT $ get tdid
    guard $ ticketDeckDeck td == did

    let tid = ticketDeckTicket td
    t <- lift $ getJust tid

    author <-
        lift $
            requireEitherAlt
                (getBy $ UniqueTicketAuthorLocal tid)
                (getBy $ UniqueTicketAuthorRemote tid)
                "Ticket doesn't have author"
                "Ticket has both local and remote author"

    mresolved <- lift $ getResolved tid

    return (Entity did d, Entity tdid td, Entity tid t, author, mresolved)

    where

    getResolved
        :: MonadIO m
        => TicketId
        -> ReaderT SqlBackend m
            (Maybe
                ( Entity TicketResolve
                , Either
                    (Entity TicketResolveLocal)
                    (Entity TicketResolveRemote)
                )
            )
    getResolved tid = do
        metr <- getBy $ UniqueTicketResolve tid
        for metr $ \ etr@(Entity trid _) ->
            (etr,) <$>
                requireEitherAlt
                    (getBy $ UniqueTicketResolveLocal trid)
                    (getBy $ UniqueTicketResolveRemote trid)
                    "No TRX"
                    "Both TRL and TRR"

getTicket404
    :: KeyHashid Deck
    -> KeyHashid TicketDeck
    -> AppDB
        ( Entity Deck
        , Entity TicketDeck
        , Entity Ticket
        , Either (Entity TicketAuthorLocal) (Entity TicketAuthorRemote)
        , Maybe
            ( Entity TicketResolve
            , Either
                (Entity TicketResolveLocal)
                (Entity TicketResolveRemote)
            )
        )
getTicket404 dkhid tdkhid = do
    did <- decodeKeyHashid404 dkhid
    tdid <- decodeKeyHashid404 tdkhid
    mticket <- getTicket did tdid
    case mticket of
        Nothing -> notFound
        Just ticket -> return ticket

{-
getDependencyCollection
    :: Route App
    -> (KeyHashid LocalTicket -> Route App)
    -> AppDB LocalTicketId
    -> Handler TypedContent
getDependencyCollection here depRoute getLocalTicketId404 = do
    tdids <- runDB $ do
        ltid <- getLocalTicketId404
        selectKeysList
            [LocalTicketDependencyParent ==. ltid]
            [Desc LocalTicketDependencyId]
    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    encodeHid <- getEncodeKeyHashid
    let deps = Collection
            { collectionId         = encodeRouteLocal here
            , collectionType       = CollectionTypeOrdered
            , collectionTotalItems = Just $ length tdids
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      =
                map (encodeRouteHome . depRoute . encodeHid) tdids
            , collectionContext    = Nothing
            }
    provideHtmlAndAP deps $ redirectToPrettyJSON here

getReverseDependencyCollection
    :: Route App -> AppDB LocalTicketId -> Handler TypedContent
getReverseDependencyCollection here getLocalTicketId404 = do
    (locals, remotes) <- runDB $ do
        ltid <- getLocalTicketId404
        (,) <$> getLocals ltid <*> getRemotes ltid
    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    encodeHid <- getEncodeKeyHashid
    let deps = Collection
            { collectionId         = encodeRouteLocal here
            , collectionType       = CollectionTypeUnordered
            , collectionTotalItems = Just $ length locals + length remotes
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      =
                map (encodeRouteHome . TicketDepR . encodeHid) locals ++
                map (\ (E.Value h, E.Value lu) -> ObjURI h lu) remotes
            , collectionContext    = Nothing
            }
    provideHtmlAndAP deps $ redirectToPrettyJSON here
    where
    getLocals ltid =
        map (ticketDependencyChildLocalDep . entityVal) <$>
            selectList [TicketDependencyChildLocalChild ==. ltid] []
    getRemotes ltid =
        E.select $ E.from $ \ (rtd `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ rtd E.^. RemoteTicketDependencyIdent E.==. ro E.^. RemoteObjectId
            E.where_ $ rtd E.^. RemoteTicketDependencyChild E.==. E.val ltid
            return (i E.^. InstanceHost, ro E.^. RemoteObjectIdent)
-}

{-
checkDepAndTarget
    :: (MonadSite m, SiteEnv m ~ App)
    => TicketDependency URIMode
    -> FedURI
    -> ExceptT Text m (Either WorkItem FedURI, Either WorkItem FedURI)
checkDepAndTarget
    (TicketDependency id_ uParent uChild _attrib published updated) uTarget = do
        verifyNothingE id_ "Dep with 'id'"
        parent <- parseWorkItem "Dep parent" uParent
        child <- parseWorkItem "Dep child" uChild
        when (parent == child) $
            throwE "Parent and child are the same work item"
        verifyNothingE published "Dep with 'published'"
        verifyNothingE updated "Dep with 'updated'"
        target <- parseTarget uTarget
        checkParentAndTarget parent target
        return (parent, child)
    where
    parseTarget u@(ObjURI h lu) = do
        hl <- hostIsLocal h
        if hl
            then Left <$> do
                route <-
                    fromMaybeE
                    (decodeRouteLocal lu)
                    "Offer local target isn't a valid route"
                fromMaybeE
                    (parseLocalActor route)
                    "Offer local target isn't an actor route"
            else return $ Right u
    checkParentAndTarget (Left wi) (Left la) = do
        la' <-
            case wi of
                WorkItemTicket did _ -> LocalActorDeck <$> encodeKeyHashid did
                WorkItemCloth lid _ -> LocalActorLoom <$> encodeKeyHashid lid
        unless (la' == la) $
            throwE "Parent and target mismatch"
    checkParentAndTarget (Left _) (Right _) = throwE "Local parent but remote target"
    checkParentAndTarget (Right _) (Left _) = throwE "Local target but remote parent"
    checkParentAndTarget (Right _) (Right _) = return ()
-}
