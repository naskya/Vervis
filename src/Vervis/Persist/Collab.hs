{- This file is part of Vervis.
 -
 - Written in 2022, 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Persist.Collab
    ( getCollabTopic
    , getCollabTopic'
    , getCollabRecip
    , getPermitTopicLocal
    , getPermitTopic
    , getStemIdent
    , getStemProject
    , getGrantRecip
    , getComponentE
    , getTopicGrants
    , getTopicInvites
    , getTopicJoins

    , verifyCapability
    , verifyCapability'

    , getGrant

    , getComponentIdent

    , checkExistingStems
    , checkExistingPermits
    )
where

import Control.Applicative
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Foldable
import Data.List (sortOn)
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist.Sql
import Optics.Core

import qualified Database.Esqueleto as E

import Network.FedURI

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Data.Maybe.Local
import Database.Persist.Local

import Vervis.Actor
import Vervis.Data.Collab
import Vervis.Model
import Vervis.Persist.Actor

getCollabTopic
    :: MonadIO m => CollabId -> ReaderT SqlBackend m (LocalActorBy Key)
getCollabTopic = fmap snd . getCollabTopic'

getCollabTopic'
    :: MonadIO m => CollabId -> ReaderT SqlBackend m (ReaderT SqlBackend m (), LocalActorBy Key)
getCollabTopic' collabID = do
    maybeRepo <- getBy $ UniqueCollabTopicRepo collabID
    maybeDeck <- getBy $ UniqueCollabTopicDeck collabID
    maybeLoom <- getBy $ UniqueCollabTopicLoom collabID
    maybeProject <- getBy $ UniqueCollabTopicProject collabID
    maybeGroup <- getBy $ UniqueCollabTopicGroup collabID
    return $
        case (maybeRepo, maybeDeck, maybeLoom, maybeProject, maybeGroup) of
            (Nothing, Nothing, Nothing, Nothing, Nothing) -> error "Found Collab without topic"
            (Just (Entity k r), Nothing, Nothing, Nothing, Nothing) ->
                (delete k, LocalActorRepo $ collabTopicRepoRepo r)
            (Nothing, Just (Entity k d), Nothing, Nothing, Nothing) ->
                (delete k, LocalActorDeck $ collabTopicDeckDeck d)
            (Nothing, Nothing, Just (Entity k l), Nothing, Nothing) ->
                (delete k, LocalActorLoom $ collabTopicLoomLoom l)
            (Nothing, Nothing, Nothing, Just (Entity k l), Nothing) ->
                (delete k, LocalActorProject $ collabTopicProjectProject l)
            (Nothing, Nothing, Nothing, Nothing, Just (Entity k l)) ->
                (delete k, LocalActorGroup $ collabTopicGroupGroup l)
            _ -> error "Found Collab with multiple topics"

getCollabRecip
    :: MonadIO m
    => CollabId
    -> ReaderT SqlBackend m
        (Either (Entity CollabRecipLocal) (Entity CollabRecipRemote))
getCollabRecip collabID =
    requireEitherAlt
        (getBy $ UniqueCollabRecipLocal collabID)
        (getBy $ UniqueCollabRecipRemote collabID)
        "Collab without recip"
        "Collab with both local and remote recip"

getPermitTopicLocal
    :: MonadIO m
    => PermitTopicLocalId
    -> ReaderT SqlBackend m (LocalActorBy Key)
getPermitTopicLocal localID = do
    options <-
        sequence
            [ fmap (LocalActorRepo . permitTopicRepoRepo) <$>
                getValBy (UniquePermitTopicRepo localID)
            , fmap (LocalActorDeck . permitTopicDeckDeck) <$>
                getValBy (UniquePermitTopicDeck localID)
            , fmap (LocalActorLoom . permitTopicLoomLoom) <$>
                getValBy (UniquePermitTopicLoom localID)
            , fmap (LocalActorProject . permitTopicProjectProject) <$>
                getValBy (UniquePermitTopicProject localID)
            , fmap (LocalActorGroup . permitTopicGroupGroup) <$>
                getValBy (UniquePermitTopicGroup localID)
            ]
    exactlyOneJust
        options
        "Found Permit without topic"
        "Found Permit with multiple topics"

getPermitTopic
    :: MonadIO m
    => PermitId
    -> ReaderT SqlBackend m
        (Either
            (PermitTopicLocalId, LocalActorBy Key)
            (PermitTopicRemoteId, RemoteActorId)
        )
getPermitTopic permitID = do
    topic <-
        requireEitherAlt
            (getKeyBy $ UniquePermitTopicLocal permitID)
            (getBy $ UniquePermitTopicRemote permitID)
            "Permit without topic"
            "Permit with both local and remote topic"
    bitraverse
        (\ localID -> (localID,) <$> getPermitTopicLocal localID)
        (\ (Entity topicID (PermitTopicRemote _ actorID)) ->
            return (topicID, actorID)
        )
        topic

getStemIdent :: MonadIO m => StemId -> ReaderT SqlBackend m (ComponentBy Key)
getStemIdent stemID = do
    maybeRepo <- getValBy $ UniqueStemIdentRepo stemID
    maybeDeck <- getValBy $ UniqueStemIdentDeck stemID
    maybeLoom <- getValBy $ UniqueStemIdentLoom stemID
    return $
        case (maybeRepo, maybeDeck, maybeLoom) of
            (Nothing, Nothing, Nothing) -> error "Found Stem without ident"
            (Just r, Nothing, Nothing) -> ComponentRepo $ stemIdentRepoRepo r
            (Nothing, Just d, Nothing) -> ComponentDeck $ stemIdentDeckDeck d
            (Nothing, Nothing, Just l) -> ComponentLoom $ stemIdentLoomLoom l
            _ -> error "Found Stem with multiple idents"

getStemProject
    :: MonadIO m
    => StemId
    -> ReaderT SqlBackend m (Either ProjectId RemoteActorId)
getStemProject stemID =
    requireEitherAlt
        (fmap stemProjectLocalProject <$> getValBy (UniqueStemProjectLocal stemID))
        (fmap stemProjectRemoteProject <$> getValBy (UniqueStemProjectRemote stemID))
        "Found Stem without project"
        "Found Stem with multiple projects"

getGrantRecip (GrantRecipPerson k) e = GrantRecipPerson <$> getEntityE k e

getComponentE (ComponentRepo k) e = ComponentRepo <$> getEntityE k e
getComponentE (ComponentDeck k) e = ComponentDeck <$> getEntityE k e
getComponentE (ComponentLoom k) e = ComponentLoom <$> getEntityE k e

getTopicGrants
    :: ( MonadIO m
       , PersistRecordBackend topic SqlBackend
       , PersistRecordBackend resource SqlBackend
       )
    => EntityField topic CollabId
    -> EntityField topic (Key resource)
    -> Key resource
    -> ReaderT SqlBackend m [(AP.Role, Either PersonId RemoteActorId, Key topic, UTCTime)]
getTopicGrants topicCollabField topicActorField resourceID =
    fmap (reverse . sortOn (view _1) . map adapt) $
    E.select $ E.from $ \ (topic `E.InnerJoin` collab `E.InnerJoin` enable `E.InnerJoin` grant `E.LeftOuterJoin` recipL `E.LeftOuterJoin` recipR) -> do
        E.on $ E.just (enable E.^. CollabEnableCollab) E.==. recipR E.?. CollabRecipRemoteCollab
        E.on $ E.just (enable E.^. CollabEnableCollab) E.==. recipL E.?. CollabRecipLocalCollab
        E.on $ enable E.^. CollabEnableGrant E.==. grant E.^. OutboxItemId
        E.on $ topic E.^. topicCollabField E.==. enable E.^. CollabEnableCollab
        E.on $ topic E.^. topicCollabField E.==. collab E.^. CollabId
        E.where_ $ topic E.^. topicActorField E.==. E.val resourceID
        E.orderBy [E.desc $ enable E.^. CollabEnableId]
        return
            ( collab E.^. CollabRole
            , recipL E.?. CollabRecipLocalPerson
            , recipR E.?. CollabRecipRemoteActor
            , topic E.^. persistIdField
            , grant E.^. OutboxItemPublished
            )
    where
    adapt (E.Value role, E.Value maybePersonID, E.Value maybeRemoteActorID, E.Value ctID, E.Value time) =
        ( role
        , case (maybePersonID, maybeRemoteActorID) of
            (Nothing, Nothing) -> error "No recip"
            (Just personID, Nothing) -> Left personID
            (Nothing, Just remoteActorID) -> Right remoteActorID
            (Just _, Just _) -> error "Multi recip"
        , ctID
        , time
        )

getTopicInvites
    :: ( MonadIO m
       , PersistRecordBackend topic SqlBackend
       , PersistRecordBackend resource SqlBackend
       )
    => EntityField topic CollabId
    -> EntityField topic (Key resource)
    -> Key resource
    -> ReaderT SqlBackend m [(Either ActorId RemoteActorId, Either PersonId RemoteActorId, UTCTime, AP.Role)]
getTopicInvites topicCollabField topicActorField resourceID =
    fmap (map adapt) $
    E.select $ E.from $
    \ (topic `E.InnerJoin` collab `E.LeftOuterJoin` enable `E.InnerJoin` fulfills
      `E.LeftOuterJoin` recipL `E.LeftOuterJoin` recipR
      `E.LeftOuterJoin` (inviterL `E.InnerJoin` item `E.InnerJoin` actor)
      `E.LeftOuterJoin` (inviterR `E.InnerJoin` activity)
      ) -> do
        E.on $ inviterR E.?. CollabInviterRemoteInvite E.==. activity E.?. RemoteActivityId
        E.on $ E.just (fulfills E.^. CollabFulfillsInviteId) E.==. inviterR E.?. CollabInviterRemoteCollab
        E.on $ item E.?. OutboxItemOutbox E.==. actor E.?. ActorOutbox
        E.on $ inviterL E.?. CollabInviterLocalInvite E.==. item E.?. OutboxItemId
        E.on $ E.just (fulfills E.^. CollabFulfillsInviteId) E.==. inviterL E.?. CollabInviterLocalCollab
        E.on $ E.just (fulfills E.^. CollabFulfillsInviteCollab) E.==. recipR E.?. CollabRecipRemoteCollab
        E.on $ E.just (fulfills E.^. CollabFulfillsInviteCollab) E.==. recipL E.?. CollabRecipLocalCollab
        E.on $ topic E.^. topicCollabField E.==. fulfills E.^. CollabFulfillsInviteCollab
        E.on $ E.just (topic E.^. topicCollabField) E.==. enable E.?. CollabEnableCollab
        E.on $ topic E.^. topicCollabField E.==. collab E.^. CollabId
        E.where_ $
            topic E.^. topicActorField E.==. E.val resourceID E.&&.
            E.isNothing (enable E.?. CollabEnableId)
        E.orderBy [E.asc $ fulfills E.^. CollabFulfillsInviteId]
        return
            ( actor E.?. ActorId
            , item E.?. OutboxItemPublished
            , inviterR E.?. CollabInviterRemoteActor
            , activity E.?. RemoteActivityReceived
            , recipL E.?. CollabRecipLocalPerson
            , recipR E.?. CollabRecipRemoteActor
            , collab E.^. CollabRole
            )
    where
    adapt (E.Value inviterL, E.Value timeL, E.Value inviterR, E.Value timeR, E.Value recipL, E.Value recipR, E.Value role) =
        let l = case (inviterL, timeL) of
                    (Nothing, Nothing) -> Nothing
                    (Just i, Just t) -> Just (i, t)
                    _ -> error "Impossible"
            r = case (inviterR, timeR) of
                    (Nothing, Nothing) -> Nothing
                    (Just i, Just t) -> Just (i, t)
                    _ -> error "Impossible"
            (inviter, time) =
                case (l, r) of
                    (Nothing, Nothing) -> error "No inviter"
                    (Just (actorID, time), Nothing) ->
                        (Left actorID, time)
                    (Nothing, Just (remoteActorID, time)) ->
                        (Right remoteActorID, time)
                    (Just _, Just _) -> error "Multi inviter"
        in  ( inviter
            , case (recipL, recipR) of
                (Nothing, Nothing) -> error "No recip"
                (Just personID, Nothing) -> Left personID
                (Nothing, Just remoteActorID) -> Right remoteActorID
                (Just _, Just _) -> error "Multi recip"
            , time
            , role
            )

getTopicJoins
    :: ( MonadIO m
       , PersistRecordBackend topic SqlBackend
       , PersistRecordBackend resource SqlBackend
       )
    => EntityField topic CollabId
    -> EntityField topic (Key resource)
    -> Key resource
    -> ReaderT SqlBackend m [(Either PersonId RemoteActorId, UTCTime, AP.Role)]
getTopicJoins topicCollabField topicActorField resourceID =
    fmap (map adapt) $
    E.select $ E.from $
    \ (topic `E.InnerJoin` collab `E.LeftOuterJoin` enable `E.InnerJoin` fulfills
      `E.LeftOuterJoin` (joinL `E.InnerJoin` recipL `E.InnerJoin` item)
      `E.LeftOuterJoin` (joinR `E.InnerJoin` recipR `E.InnerJoin` activity)
      ) -> do
        E.on $ joinR E.?. CollabRecipRemoteJoinJoin E.==. activity E.?. RemoteActivityId
        E.on $ joinR E.?. CollabRecipRemoteJoinCollab E.==. recipR E.?. CollabRecipRemoteId
        E.on $ E.just (fulfills E.^. CollabFulfillsJoinId) E.==. joinR E.?. CollabRecipRemoteJoinFulfills
        E.on $ joinL E.?. CollabRecipLocalJoinJoin E.==. item E.?. OutboxItemId
        E.on $ joinL E.?. CollabRecipLocalJoinCollab E.==. recipL E.?. CollabRecipLocalId
        E.on $ E.just (fulfills E.^. CollabFulfillsJoinId) E.==. joinL E.?. CollabRecipLocalJoinFulfills
        E.on $ topic E.^. topicCollabField E.==. fulfills E.^. CollabFulfillsJoinCollab
        E.on $ E.just (topic E.^. topicCollabField) E.==. enable E.?. CollabEnableCollab
        E.on $ topic E.^. topicCollabField E.==. collab E.^. CollabId
        E.where_ $
            topic E.^. topicActorField E.==. E.val resourceID E.&&.
            E.isNothing (enable E.?. CollabEnableId)
        E.orderBy [E.asc $ fulfills E.^. CollabFulfillsJoinId]
        return
            ( recipL E.?. CollabRecipLocalPerson
            , item E.?. OutboxItemPublished
            , recipR E.?. CollabRecipRemoteActor
            , activity E.?. RemoteActivityReceived
            , collab E.^. CollabRole
            )
    where
    adapt (E.Value recipL, E.Value timeL, E.Value recipR, E.Value timeR, E.Value role) =
        let l = case (recipL, timeL) of
                    (Nothing, Nothing) -> Nothing
                    (Just r, Just t) -> Just (r, t)
                    _ -> error "Impossible"
            r = case (recipR, timeR) of
                    (Nothing, Nothing) -> Nothing
                    (Just r, Just t) -> Just (r, t)
                    _ -> error "Impossible"
        in  case (l, r) of
                (Nothing, Nothing) -> error "No recip"
                (Just (personID, time), Nothing) -> (Left personID, time, role)
                (Nothing, Just (remoteActorID, time)) -> (Right remoteActorID, time, role)
                (Just _, Just _) -> error "Multi recip"

verifyCapability
    :: MonadIO m
    => (LocalActorBy Key, OutboxItemId)
    -> Either PersonId RemoteActorId
    -> LocalActorBy Key
    -> AP.Role
    -> ExceptT Text (ReaderT SqlBackend m) ()
verifyCapability (capActor, capItem) actor resource requiredRole = do

    -- Find the activity itself by URI in the DB
    nameExceptT "Capability activity not found" $
        verifyLocalActivityExistsInDB capActor capItem

    -- Find the Collab record for that activity
    collabID <- do
        maybeEnable <- lift $ getValBy $ UniqueCollabEnableGrant capItem
        collabEnableCollab <$>
            fromMaybeE maybeEnable "No CollabEnable for this activity"

    -- Find the recipient of that Collab
    recipID <-
        lift $ bimap collabRecipLocalPerson collabRecipRemoteActor <$>
            requireEitherAlt
                (getValBy $ UniqueCollabRecipLocal collabID)
                (getValBy $ UniqueCollabRecipRemote collabID)
                "No collab recip"
                "Both local and remote recips for collab"

    -- Verify the recipient is the expected one
    unless (recipID == actor) $
        throwE "Collab recipient is someone else"

    -- Find the local topic, on which this Collab gives access
    topic <- lift $ getCollabTopic collabID

    -- Verify that topic is indeed the sender of the Grant
    unless (topic == capActor) $
        error "Grant sender isn't the topic"

    -- Verify the topic matches the resource specified
    unless (topic == resource) $
        throwE "Capability topic is some other local resource"

    -- Verify that the granted role is equal or greater than the required role
    Collab givenRole <- lift $ getJust collabID
    unless (givenRole >= requiredRole) $
        throwE "The granted role doesn't allow the requested operation"

verifyCapability'
    :: MonadIO m
    => (LocalActorBy Key, OutboxItemId)
    -> Either
        (LocalActorBy Key, ActorId, OutboxItemId)
        (RemoteAuthor, LocalURI, Maybe ByteString)
    -> LocalActorBy Key
    -> AP.Role
    -> ExceptT Text (ReaderT SqlBackend m) ()
verifyCapability' cap actor resource role = do
    actorP <- processActor actor
    verifyCapability cap actorP resource role
    where
    processActor = bitraverse processLocal processRemote
        where
        processLocal (actorByKey, _, _) =
            case actorByKey of
                LocalActorPerson personID -> return personID
                _ -> throwE "Non-person local actors can't get Grants at the moment"
        processRemote (author, _, _) = pure $ remoteAuthorId author

getGrant
    :: ( MonadIO m
       , PersistRecordBackend topic SqlBackend
       , PersistRecordBackend resource SqlBackend
       , Show (Key resource)
       )
    => EntityField topic CollabId
    -> EntityField topic (Key resource)
    -> Key resource
    -> PersonId
    -> ReaderT SqlBackend m (Maybe OutboxItemId)
getGrant topicCollabField topicActorField resourceID personID = do
    items <-
        E.select $ E.from $ \ (topic `E.InnerJoin` enable `E.InnerJoin` grant `E.InnerJoin` recipL) -> do
            E.on $ enable E.^. CollabEnableCollab E.==. recipL E.^. CollabRecipLocalCollab
            E.on $ enable E.^. CollabEnableGrant E.==. grant E.^. OutboxItemId
            E.on $ topic E.^. topicCollabField E.==. enable E.^. CollabEnableCollab
            E.where_ $
                topic E.^. topicActorField E.==. E.val resourceID E.&&.
                recipL E.^. CollabRecipLocalPerson E.==. E.val personID
            return $ grant E.^. OutboxItemId
    case items of
        [] -> return Nothing
        [E.Value i] -> return $ Just i
        _ -> error $ "Multiple grants for a Person in resource#" ++ show resourceID

getComponentIdent
    :: MonadIO m
    => ComponentId
    -> ReaderT SqlBackend m
        (Either
            (ComponentLocalId, ComponentBy Key)
            (ComponentRemoteId, RemoteActorId)
        )
getComponentIdent componentID = do
    ident <-
        requireEitherAlt
            (getKeyBy $ UniqueComponentLocal componentID)
            (getBy $ UniqueComponentRemote componentID)
            "Found Component without ident"
            "Found Component with both local and remote ident"
    bitraverse
        (\ localID -> do
            maybeRepo <- getValBy $ UniqueComponentLocalRepo localID
            maybeDeck <- getValBy $ UniqueComponentLocalDeck localID
            maybeLoom <- getValBy $ UniqueComponentLocalLoom localID
            fmap (localID,) $ return $
                case (maybeRepo, maybeDeck, maybeLoom) of
                    (Nothing, Nothing, Nothing) ->
                        error "Found ComponentLocal without ident"
                    (Just r, Nothing, Nothing) ->
                        ComponentRepo $ componentLocalRepoRepo r
                    (Nothing, Just d, Nothing) ->
                        ComponentDeck $ componentLocalDeckDeck d
                    (Nothing, Nothing, Just l) ->
                        ComponentLoom $ componentLocalLoomLoom l
                    _ -> error "Found ComponentLocal with multiple idents"
        )
        (\ (Entity k v) -> pure (k, componentRemoteActor v))
        ident

checkExistingStems
    :: ComponentBy Key -> Either (Entity Project) RemoteActorId -> ActDBE ()
checkExistingStems componentByID projectDB = do

    -- Find existing Stem records I have for this project
    stemIDs <- lift $ getExistingStems componentByID

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    byEnabled <-
        lift $ for stemIDs $ \ (_, stem) ->
            isJust <$> runMaybeT (tryStemEnabled stem)
    case length $ filter id byEnabled of
        0 -> return ()
        1 -> throwE "I already have a StemProjectGrant* for this project"
        _ -> error "Multiple StemProjectGrant* for a project"

    -- Verify none of the Stem records are already in
    -- Add-waiting-for-project or Invite-waiting-for-my-collaborator state
    anyStarted <-
        lift $ runMaybeT $ asum $
            map (\ (stemID, project) ->
                    tryStemAddAccept stemID <|>
                    tryStemInviteAccept stemID project
                )
                stemIDs
    unless (isNothing anyStarted) $
        throwE
            "One of the Stem records is already in Add-Accept or \
            \Invite-Accept state"

    where

    getExistingStems' compID stemField compField (Left (Entity projectID _)) =
        fmap (map $ bimap E.unValue (Left . E.unValue)) $
        E.select $ E.from $ \ (project `E.InnerJoin` ident) -> do
            E.on $ project E.^. StemProjectLocalStem E.==. ident E.^. stemField
            E.where_ $
                project E.^. StemProjectLocalProject E.==. E.val projectID E.&&.
                ident E.^. compField E.==. E.val compID
            return
                ( project E.^. StemProjectLocalStem
                , project E.^. StemProjectLocalId
                )
    getExistingStems' compID stemField compField (Right remoteActorID) =
        fmap (map $ bimap E.unValue (Right . E.unValue)) $
        E.select $ E.from $ \ (project `E.InnerJoin` ident) -> do
            E.on $ project E.^. StemProjectRemoteStem E.==. ident E.^. stemField
            E.where_ $
                project E.^. StemProjectRemoteProject E.==. E.val remoteActorID E.&&.
                ident E.^. compField E.==. E.val compID
            return
                ( project E.^. StemProjectRemoteStem
                , project E.^. StemProjectRemoteId
                )

    getExistingStems (ComponentRepo repoID) =
        getExistingStems' repoID StemIdentRepoStem StemIdentRepoRepo projectDB
    getExistingStems (ComponentDeck deckID) =
        getExistingStems' deckID StemIdentDeckStem StemIdentDeckDeck projectDB
    getExistingStems (ComponentLoom loomID) =
        getExistingStems' loomID StemIdentLoomStem StemIdentLoomLoom projectDB

    tryStemEnabled (Left localID) =
        const () <$> MaybeT (getBy $ UniqueStemProjectGrantLocalProject localID)
    tryStemEnabled (Right remoteID) =
        const () <$> MaybeT (getBy $ UniqueStemProjectGrantRemoteProject remoteID)

    tryStemAddAccept stemID = do
        _ <- MaybeT $ getBy $ UniqueStemOriginAdd stemID
        _ <- MaybeT $ getBy $ UniqueStemComponentAccept stemID
        pure ()

    tryStemInviteAccept stemID project = do
        originID <- MaybeT $ getKeyBy $ UniqueStemOriginInvite stemID
        case project of
            Left localID ->
                const () <$> MaybeT (getBy $ UniqueStemProjectAcceptLocalProject localID)
            Right remoteID ->
                const () <$> MaybeT (getBy $ UniqueStemProjectAcceptRemoteProject remoteID)

checkExistingPermits
    :: PersonId -> Either (LocalActorBy Key) RemoteActorId -> ActDBE ()
checkExistingPermits personID topicDB = do

    -- Find existing Permit records I have for this topic
    permitIDs <- lift $ getExistingPermits topicDB

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    byEnabled <-
        lift $ for permitIDs $ \ (_, permit) ->
            isJust <$> runMaybeT (tryPermitEnabled permit)
    case length $ filter id byEnabled of
        0 -> return ()
        1 -> throwE "I already have a PermitTopicEnable* for this topic"
        _ -> error "Multiple PermitTopicEnable* for a topic"

    -- Verify none of the Permit records are already in Join or
    -- Invite-and-Accept state
    anyStarted <-
        lift $ runMaybeT $ asum $
            map (\ (permitID, topic) ->
                    tryPermitJoin permitID <|>
                    tryPermitInviteAccept permitID topic
                )
                permitIDs
    unless (isNothing anyStarted) $
        throwE
            "One of the Permit records is already in Join or Invite-Accept \
            \state"

    where

    getExistingPermits (Left (LocalActorPerson _)) = pure []
    getExistingPermits (Left (LocalActorRepo repoID)) =
        fmap (map $ bimap E.unValue (Left . E.unValue)) $
        E.select $ E.from $ \ (permit `E.InnerJoin` local `E.InnerJoin` topic) -> do
            E.on $ local E.^. PermitTopicLocalId E.==. topic E.^. PermitTopicRepoPermit
            E.on $ permit E.^. PermitId E.==. local E.^. PermitTopicLocalPermit
            E.where_ $
                permit E.^. PermitPerson E.==. E.val personID E.&&.
                topic E.^. PermitTopicRepoRepo E.==. E.val repoID
            return
                ( permit E.^. PermitId
                , local E.^. PermitTopicLocalId
                )
    getExistingPermits (Left (LocalActorDeck deckID)) =
        fmap (map $ bimap E.unValue (Left . E.unValue)) $
        E.select $ E.from $ \ (permit `E.InnerJoin` local `E.InnerJoin` topic) -> do
            E.on $ local E.^. PermitTopicLocalId E.==. topic E.^. PermitTopicDeckPermit
            E.on $ permit E.^. PermitId E.==. local E.^. PermitTopicLocalPermit
            E.where_ $
                permit E.^. PermitPerson E.==. E.val personID E.&&.
                topic E.^. PermitTopicDeckDeck E.==. E.val deckID
            return
                ( permit E.^. PermitId
                , local E.^. PermitTopicLocalId
                )
    getExistingPermits (Left (LocalActorLoom loomID)) =
        fmap (map $ bimap E.unValue (Left . E.unValue)) $
        E.select $ E.from $ \ (permit `E.InnerJoin` local `E.InnerJoin` topic) -> do
            E.on $ local E.^. PermitTopicLocalId E.==. topic E.^. PermitTopicLoomPermit
            E.on $ permit E.^. PermitId E.==. local E.^. PermitTopicLocalPermit
            E.where_ $
                permit E.^. PermitPerson E.==. E.val personID E.&&.
                topic E.^. PermitTopicLoomLoom E.==. E.val loomID
            return
                ( permit E.^. PermitId
                , local E.^. PermitTopicLocalId
                )
    getExistingPermits (Left (LocalActorProject projectID)) =
        fmap (map $ bimap E.unValue (Left . E.unValue)) $
        E.select $ E.from $ \ (permit `E.InnerJoin` local `E.InnerJoin` topic) -> do
            E.on $ local E.^. PermitTopicLocalId E.==. topic E.^. PermitTopicProjectPermit
            E.on $ permit E.^. PermitId E.==. local E.^. PermitTopicLocalPermit
            E.where_ $
                permit E.^. PermitPerson E.==. E.val personID E.&&.
                topic E.^. PermitTopicProjectProject E.==. E.val projectID
            return
                ( permit E.^. PermitId
                , local E.^. PermitTopicLocalId
                )
    getExistingPermits (Left (LocalActorGroup groupID)) =
        fmap (map $ bimap E.unValue (Left . E.unValue)) $
        E.select $ E.from $ \ (permit `E.InnerJoin` local `E.InnerJoin` topic) -> do
            E.on $ local E.^. PermitTopicLocalId E.==. topic E.^. PermitTopicGroupPermit
            E.on $ permit E.^. PermitId E.==. local E.^. PermitTopicLocalPermit
            E.where_ $
                permit E.^. PermitPerson E.==. E.val personID E.&&.
                topic E.^. PermitTopicGroupGroup E.==. E.val groupID
            return
                ( permit E.^. PermitId
                , local E.^. PermitTopicLocalId
                )
    getExistingPermits (Right remoteActorID) =
        fmap (map $ bimap E.unValue (Right . E.unValue)) $
        E.select $ E.from $ \ (permit `E.InnerJoin` remote) -> do
            E.on $ permit E.^. PermitId E.==. remote E.^. PermitTopicRemotePermit
            E.where_ $
                permit E.^. PermitPerson E.==. E.val personID E.&&.
                remote E.^. PermitTopicRemoteActor E.==. E.val remoteActorID
            return
                ( permit E.^. PermitId
                , remote E.^. PermitTopicRemoteId
                )

    tryPermitEnabled (Left localID) =
        const () <$> MaybeT (getBy $ UniquePermitTopicEnableLocalTopic localID)
    tryPermitEnabled (Right remoteID) =
        const () <$> MaybeT (getBy $ UniquePermitTopicEnableRemoteTopic remoteID)

    tryPermitJoin permitID = do
        _ <- MaybeT $ getBy $ UniquePermitFulfillsJoin permitID
        pure ()

    tryPermitInviteAccept permitID topic = do
        _fulfillsID <- MaybeT $ getKeyBy $ UniquePermitFulfillsInvite permitID
        case topic of
            Left localID ->
                const () <$> MaybeT (getBy $ UniquePermitTopicAcceptLocalTopic localID)
            Right remoteID ->
                const () <$> MaybeT (getBy $ UniquePermitTopicAcceptRemoteTopic remoteID)
