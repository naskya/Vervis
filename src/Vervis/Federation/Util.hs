{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Federation.Util
    ( insertToInbox
    )
where

import Control.Monad.IO.Class
import Control.Monad.Trans.Reader
import Data.ByteString (ByteString)
import Data.Either
import Data.Time.Clock
import Database.Persist
import Database.Persist.Sql

import Database.Persist.JSON
import Network.FedURI

import Database.Persist.Local

import Vervis.Actor
import Vervis.Federation.Auth
import Vervis.Foundation
import Vervis.Model

-- | Insert an activity delivered to us into our inbox. Return its
-- database ID if the activity wasn't already in our inbox.
insertToInbox
    :: UTCTime
    -> Either
        (LocalActorBy Key, ActorId, OutboxItemId)
        (RemoteAuthor, LocalURI, Maybe ByteString)
    -> ActivityBody
    -> InboxId
    -> Bool
    -> ActDB
        (Maybe
            (Either
                (LocalActorBy Key, ActorId, OutboxItemId)
                (RemoteAuthor, LocalURI, RemoteActivityId)
            )
        )
insertToInbox now (Left a@(_, _, outboxItemID)) _body inboxID unread = do
    inboxItemID <- insert $ InboxItem unread now
    maybeItem <- insertUnique $ InboxItemLocal inboxID outboxItemID inboxItemID
    case maybeItem of
        Nothing -> do
            delete inboxItemID
            return Nothing
        Just _ -> return $ Just $ Left a
insertToInbox now (Right (author, luAct, _)) body inboxID unread = do
    let iidAuthor = remoteAuthorInstance author
    roid <-
        either entityKey id <$> insertBy' (RemoteObject iidAuthor luAct)
    ractid <- either entityKey id <$> insertBy' RemoteActivity
        { remoteActivityIdent    = roid
        , remoteActivityContent  = persistJSONFromBL $ actbBL body
        , remoteActivityReceived = now
        }
    ibiid <- insert $ InboxItem unread now
    mibrid <- insertUnique $ InboxItemRemote inboxID ractid ibiid
    case mibrid of
        Nothing -> do
            delete ibiid
            return Nothing
        Just _ -> return $ Just $ Right (author, luAct, ractid)
