{- This file is part of Vervis.
 -
 - Written in 2019, 2022, 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Widget.Tracker
    ( deckNavW
    , loomNavW
    , projectNavW
    , componentLinkFedW
    , projectLinkFedW
    , groupLinkFedW
    , actorLinkFedW
    , groupNavW
    )
where

import Data.Bifunctor
import Database.Persist
import Database.Persist.Types
import Yesod.Core.Widget
import Yesod.Persist.Core

import Network.FedURI
import Yesod.Hashids

import qualified Web.ActivityPub as AP

import Vervis.Actor
import Vervis.Data.Collab
import Vervis.Foundation
import Vervis.Model
import Vervis.Model.Ident
import Vervis.Settings

deckNavW :: Entity Deck -> Actor -> Widget
deckNavW (Entity deckID deck) actor = do
    deckHash <- encodeKeyHashid deckID
    hashRepo <- getEncodeKeyHashid
    $(widgetFile "deck/widget/nav")

loomNavW :: Entity Loom -> Actor -> Widget
loomNavW (Entity loomID loom) actor = do
    loomHash <- encodeKeyHashid loomID
    hashRepo <- getEncodeKeyHashid
    $(widgetFile "loom/widget/nav")

projectNavW :: Entity Project -> Actor -> Widget
projectNavW (Entity projectID project) actor = do
    projectHash <- encodeKeyHashid projectID
    $(widgetFile "project/widget/nav")

groupNavW :: Entity Group -> Actor -> Widget
groupNavW (Entity groupID group) actor = do
    groupHash <- encodeKeyHashid groupID
    $(widgetFile "group/nav")

componentLinkW :: ComponentBy Key -> Actor -> Widget
componentLinkW (ComponentRepo k) actor = do
    h <- encodeKeyHashid k
    [whamlet|
        <a href=@{RepoR h}>
          ^#{keyHashidText h} #{actorName actor}
    |]
componentLinkW (ComponentDeck k) actor = do
    h <- encodeKeyHashid k
    [whamlet|
        <a href=@{DeckR h}>
          =#{keyHashidText h} #{actorName actor}
    |]
componentLinkW (ComponentLoom k) actor = do
    h <- encodeKeyHashid k
    [whamlet|
        <a href=@{LoomR h}>
          +#{keyHashidText h} #{actorName actor}
    |]

componentLinkFedW
    :: Either (ComponentBy Key, Actor) (Instance, RemoteObject, RemoteActor)
    -> Widget
componentLinkFedW (Left (c, a)) = componentLinkW c a
componentLinkFedW (Right (inztance, object, actor)) =
    [whamlet|
        <a href="#{renderObjURI uActor}">
          $maybe name <- remoteActorName actor
            #{name}
          $nothing
            #{renderAuthority $ instanceHost inztance}#{localUriPath $ remoteObjectIdent object}
    |]
    where
    uActor = ObjURI (instanceHost inztance) (remoteObjectIdent object)

projectLinkFedW
    :: Either (ProjectId, Actor) (Instance, RemoteObject, RemoteActor)
    -> Widget
projectLinkFedW = actorLinkFedW . bimap (first LocalActorProject) id

groupLinkFedW
    :: Either (GroupId, Actor) (Instance, RemoteObject, RemoteActor)
    -> Widget
groupLinkFedW = actorLinkFedW . bimap (first LocalActorGroup) id

actorLinkW :: LocalActorBy Key -> Actor -> Widget
actorLinkW (LocalActorPerson k) actor = do
    p <- handlerToWidget $ runDB $ getJust k
    h <- encodeKeyHashid k
    [whamlet|
        <a href=@{PersonR h}>
          ~#{username2text $ personUsername p} #{actorName actor}
    |]
actorLinkW (LocalActorRepo k) actor = do
    h <- encodeKeyHashid k
    [whamlet|
        <a href=@{RepoR h}>
          ^#{keyHashidText h} #{actorName actor}
    |]
actorLinkW (LocalActorDeck k) actor = do
    h <- encodeKeyHashid k
    [whamlet|
        <a href=@{DeckR h}>
          =#{keyHashidText h} #{actorName actor}
    |]
actorLinkW (LocalActorLoom k) actor = do
    h <- encodeKeyHashid k
    [whamlet|
        <a href=@{LoomR h}>
          +#{keyHashidText h} #{actorName actor}
    |]
actorLinkW (LocalActorProject k) actor = do
    h <- encodeKeyHashid k
    [whamlet|
        <a href=@{ProjectR h}>
          \$#{keyHashidText h} #{actorName actor}
    |]
actorLinkW (LocalActorGroup k) actor = do
    h <- encodeKeyHashid k
    [whamlet|
        <a href=@{GroupR h}>
          &#{keyHashidText h} #{actorName actor}
    |]

actorLinkFedW
    :: Either (LocalActorBy Key, Actor) (Instance, RemoteObject, RemoteActor)
    -> Widget
actorLinkFedW (Left (c, a)) = actorLinkW c a
actorLinkFedW (Right (inztance, object, actor)) =
    [whamlet|
        <a href="#{renderObjURI uActor}">
          #{marker $ remoteActorType actor} #
          $maybe name <- remoteActorName actor
            #{name} @ #{renderAuthority $ instanceHost inztance}
          $nothing
            #{renderAuthority $ instanceHost inztance}#{localUriPath $ remoteObjectIdent object}
    |]
    where
    uActor = ObjURI (instanceHost inztance) (remoteObjectIdent object)
    marker = \case
        AP.ActorTypePerson -> '~'
        AP.ActorTypeRepo -> '^'
        AP.ActorTypeTicketTracker -> '='
        AP.ActorTypePatchTracker -> '+'
        AP.ActorTypeProject -> '$'
        AP.ActorTypeTeam -> '&'
        AP.ActorTypeOther _ -> '?'
