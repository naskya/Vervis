{- This file is part of Vervis.
 -
 - Written in 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

-- | Save and load the read-only environment of application components.
--
-- Meh, that's the best title I can come up with right now. I'm tempted not to
-- make it actor-specific, hence also no "Actor" in the module name. But here's
-- an attempt with actors:
--
-- Disk-persistent actor read-only identity/environments
--
-- Or:
--
-- Store and load the read-only environments that identity/define/accompany
-- your actor/microservice threads throughout their lifetimes.
--
-- I'm thinking of 3 basic ways that the slab system can be implemented:
--
-- * Using one file per actor, naming the file using either UUID or a hash of
--   some unique property of the actor if it already has one
-- * Same but using an SQLite database for each slab, for atomicity
-- * A single database, say SQLite, for all the slabs
--
-- This module provides just the slab system, without a specific backend. I'll
-- write a file backend in another module, which can live in the same package.
-- The SQLite ones need their own package(s) because they depend on a DB.
--
-- Let's see what comes up.
module Data.Slab
    ( Engrave ()
    , EngraveShow ()
    , EngraveJSON ()
    , EngraveSerialize ()
    , Slab (..)
    , Workshop (..)
    )
where

import Data.Slab.Backend
